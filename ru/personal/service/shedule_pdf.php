<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
  CModule::IncludeModule('iblock');

  $times = CTimesMatrix::getTimeslots();

  /*---------------------------------------------------*/
  //           ��������� ����� ��� �������             //
  /*---------------------------------------------------*/
	  $rsUser = CUser::GetByID($USER->GetID());
	  $thisUser = $rsUser->Fetch();
	  $arResult["USER"]["NAME"] = $thisUser["NAME"]." ".$thisUser["LAST_NAME"];
	  $arResult["USER"]["COMPANY"] = $thisUser["WORK_COMPANY"];
      $arAnswer = CFormResult::GetDataByID($thisUser["UF_ANKETA"], array(), $arTmpResult, $arAnswer2);
	  $arResult["USER"]["CITY"] = $arAnswer2["city"]["133"]["USER_TEXT"];
	  $arParams["APP_COUNT"] = CTimesMatrix::getTimeslotsCount();
	  $arParams["GROUP_RECIVER_ID"] = 4;

	  $myShedule = array();
	  $myFreeMeet = array();
	  $myBeasyMeet = array();
	  $myFreeCount = 0;
	  //��������� ������ ��� ������� ������
	  for($i=1; $i<$arParams["APP_COUNT"]+1; $i++){
		  $myShedule[$i]["ID"] = $thisUser["UF_SHEDULE_".$i];
		  $myShedule[$i]["TITLE"] = $times[$i-1];
		  $myShedule[$i]["STATUS"] = '';
		  $myShedule[$i]["NOTES"] = 'FREE';
		  $myShedule[$i]["PARTNER_ID"] = '';
		  $myShedule[$i]["REP"] = '';
		  $myShedule[$i]["COMPANY"] = '';
		  $myShedule[$i]["LIST"]["COUNT"] = 0;
		  $myShedule[$i]["LIST"]["COMPANYS"] = array();
		  if($thisUser["UF_SHEDULE_".$i] == ''){
			$myFreeMeet[] = $i;
			$myFreeCount++;
		  }
		  else{
			$myShedule[$i]["NOTES"] = 'ACT';
			$myBeasyMeet[] = $thisUser["UF_SHEDULE_".$i];
		  }
	  }
	  //������ ����������� ������
	  if($myBeasyMeet){
		  $arFilterM = Array(
			 "IBLOCK_ID" => $arParams["APP_ID"],
			 "ID" => $meeting_list
			 );
		  $arSelect = Array("DATE_CREATE", "ID", "NAME", "ACTIVE", "PROPERTY_SENDER_ID", "PROPERTY_RECIVER_ID", "PROPERTY_STATUS", "PROPERTY_TIME");
		  $resMeet = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilterM, false, false, $arSelect);
		  while($ar_meet = $resMeet->GetNext()){
			for($i=1; $i<$arParams["APP_COUNT"]+1; $i++){
				if($thisUser["UF_SHEDULE_".$i] == $ar_meet["ID"]){
				  if($ar_meet["ACTIVE"] == 'N'){
					$myShedule[$i]["NOTES"] = 'N';
				  }
				  if($ar_meet['PROPERTY_SENDER_ID_VALUE'] == $thisUser['ID']){
					$myShedule[$i]["STATUS"] = 'MY';
					$myShedule[$i]["PARTNER_ID"] = $ar_meet['PROPERTY_RECIVER_ID_VALUE'];
				  }
				  else{
					$myShedule[$i]["STATUS"] = 'PEP';
					$myShedule[$i]["PARTNER_ID"] = $ar_meet['PROPERTY_SENDER_ID_VALUE'];
				  }
				  if($ar_meet['PROPERTY_STATUS_VALUE'] == 'ADM'){
					$myShedule[$i]["STATUS"] = 'ADM';
				  }
				}
			}
		  }
	  }
	  //������ �������������
	  $filter = Array(
		  "GROUPS_ID"  => Array($arParams["GROUP_RECIVER_ID"])
	  );
	  $rsUsers = CUser::GetList(($by="WORK_COMPANY"), ($order="asc"), $filter, array("SELECT"=>array("UF_*"))); // �������� �������������
	  $myWishIn = array();
	  $myWishOut = array();
	  $notFreeTimes = array();
	  while($arUsersTemp=$rsUsers->Fetch()){
		  for($i=1; $i<$arParams["APP_COUNT"]+1; $i++){
			  if($myShedule[$i]["ID"] != ''){
				  if($myShedule[$i]["PARTNER_ID"] == $arUsersTemp["ID"]){
					$myShedule[$i]["REP"] = $arUsersTemp["NAME"]." ".$arUsersTemp["LAST_NAME"];
					$myShedule[$i]["COMPANY"] = $arUsersTemp["WORK_COMPANY"];
					$arAnswer = CFormResult::GetDataByID($arUsersTemp["UF_ANKETA"], array(), $arTmpResult, $arAnswer2);
					$userHall = '';
					if(isset($arAnswer2["user_hall"])){
					  foreach($arAnswer2["user_hall"] as $value){
						  $userHall = $value["MESSAGE"];
					  }						
					}					
					if(!isset($arAnswer2["user_hall"]) || $userHall == 'None'){
						$myShedule[$i]["HALL"] = '';
						$myShedule[$i]["TABLE"] = '';
					}
					else{
						$myShedule[$i]["HALL"] = $userHall;
						$myShedule[$i]["TABLE"] = $arAnswer2["user_table"]["394"]["USER_TEXT"];
					}
				  }
			  }
		  }
	  }
	  $arResult["SHEDULE"] = $myShedule;
	  $arResult["APP_COUNT"] = $arParams["APP_COUNT"];

	require('pdf/tcpdf.php');
	$pdf = new TCPDF('P', 'mm', 'A4', false, 'UTF-8', false);
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);
	$pdf->AddFont('times','I','timesi.php');
	$pdf->AddPage();
	$pdf->ImageSVG($file='images/logo.svg', $x=30, $y=5, $w='150', $h='', $link='', $align='', $palign='', $border=0, $fitonpage=false);

	$pdf->setXY(0,25);
	$pdf->SetFont('times','B',17);
	$pdf->multiCell(210, 6, "���������� ������ �� ������� ������\n������� ������� 2018", 0, C);
	$pdf->SetFont('times','',15);
	$pdf->setXY(30,44);
	$pdf->multiCell(210, 5, $arResult["USER"]["COMPANY"].", ". $arResult["USER"]["CITY"], 0, L);
	$pdf->setXY(30,52);
	$pdf->multiCell(210, 5,$arResult["USER"]['NAME']." ".$arResult["USER"]["LAST_NAME"], 0, L);
	$pdf->SetFont('times','',11);
	$pdf->setXY(0,60);
	$pdf->multiCell(210, 5, "���� ����������", 0, C);

	$pdf->setY(70);
	$pdf->SetX(20);

	/* ��������� ������� */
        $tbl = '<table cellspacing="0" cellpadding="5" border="1">
    <tr nobr="true">
        <td align="center" width="65">�����</td>
        <td align="center" width="240">���������</td>
        <td align="center" width="90"> </td>
        <td align="center" width="100">���, ����</td>
    </tr>';
	$pdf->SetFont('times','',9);
	$counter = 0;
	for($i=1; $i<$arParams["APP_COUNT"]+1; $i++){
		if($arResult["SHEDULE"][$i]['NOTES'] == 'FREE'){
			$tbl .= '<tr nobr="true">
                        <td>'.$arResult["SHEDULE"][$i]['TITLE'].'</td>
                        <td colspan="3" align="center">��������</td>
                    </tr>';
			$counter++;
		}
		elseif($arResult["SHEDULE"][$i]['NOTES'] == 'ACT'){
			$tbl .= '<tr nobr="true">
                        <td>'.$arResult["SHEDULE"][$i]['TITLE'].'</td>
                        <td>��������: '.$arResult["SHEDULE"][$i]['COMPANY'].'<br />�������������: '.$arResult["SHEDULE"][$i]["REP"].'</td>
                        <td align="center">������������</td>';
			if($arResult["SHEDULE"][$i]['HALL']){
				 $tbl .= '<td>'.$arResult["SHEDULE"][$i]['HALL'].', '.$arResult["SHEDULE"][$i]['TABLE'].'</td>
                        </tr>';
			}
			else{
				$tbl .= '<td> </td>
                        </tr>';
			}
			$counter++;
		}
		else{
			if($arResult["SHEDULE"][$i]['STATUS'] == 'MY'){
				$tbl .= '<tr nobr="true">
                                <td>'.$arResult["SHEDULE"][$i]['TITLE'].'</td>
                                <td>��������: '.$arResult["SHEDULE"][$i]['COMPANY'].'<br />�������������: '.$arResult["SHEDULE"][$i]["REP"].'</td>
                                <td align="center">�� ����</td>';
				if($arResult["SHEDULE"][$i]['HALL']){
					 $tbl .= '<td>'.$arResult["SHEDULE"][$i]['HALL'].', '.$arResult["SHEDULE"][$i]['TABLE'].'</td>
                                </tr>';
				}
				else{
					$tbl .= '<td> </td>
                                </tr>';
				}
			}
			elseif($arResult["SHEDULE"][$i]['STATUS'] == 'ADM'){
				$tbl .= '<tr nobr="true">
                                <td>'.$arResult["SHEDULE"][$i]['TITLE'].'</td>
                                <td>��������: '.$arResult["SHEDULE"][$i]['COMPANY'].'<br />�������������: '.$arResult["SHEDULE"][$i]["REP"].'</td>
                                <td align="center">��������� ���������������</td>';
				if($arResult["SHEDULE"][$i]['HALL']){
					 $tbl .= '<td>'.$arResult["SHEDULE"][$i]['HALL'].', '.$arResult["SHEDULE"][$i]['TABLE'].'</td>
                                </tr>';
				}
				else{
					$tbl .= '<td> </td>
                                </tr>';
				}
			}
			else{
				 $tbl .= '<tr nobr="true">
                                <td>'.$arResult["SHEDULE"][$i]['TITLE'].'</td>
                                <td>��������: '.$arResult["SHEDULE"][$i]['COMPANY'].'<br />�������������: '.$arResult["SHEDULE"][$i]["REP"].'</td>
                                <td align="center">���</td>';
                                if($arResult["SHEDULE"][$i]['HALL']){
                                    $tbl .= '<td>'.$arResult["SHEDULE"][$i]['HALL'].', '.$arResult["SHEDULE"][$i]['TABLE'].'</td>
                                </tr>';
                                }
                                else{
                                    $tbl .= '<td> </td>
                                </tr>';
                                }
			}
			$counter++;
		}
	}
	/*$tbl .= '<tr>
				<td>'.CTimesMatrix::getCoffeeTime().'</td>
				<td colspan="3" align="center">������� �� ����</td>
			</tr>';
	$tbl .= '</table>';
	$pdf->writeHTML($tbl, true, false, false, false, '');

	$pdf->setXY(0,$pdf->getY() + 1);
	$pdf->multiCell(210, 5, "����������� �� ��������� ��������", 0, C);

	$pdf->AddPage();
	$tbl = '<table cellspacing="0" cellpadding="5" border="1">
		<tr>
			<td align="center" width="65">�����</td>
			<td align="center" width="240">���������</td>
			<td align="center" width="90"> </td>
			<td align="center" width="100">���, ����</td>
		</tr>';
	$pdf->SetFont('times','',9);
	$pdf->SetX(20);
	for($i=CTimesMatrix::getCoffeeCount()+1; $i<$arParams["APP_COUNT"]+1; $i++){
		if($arResult["SHEDULE"][$i]['NOTES'] == 'FREE'){
			$tbl .= '<tr>
                        <td>'.$arResult["SHEDULE"][$i]['TITLE'].'</td>
                        <td colspan="3" align="center">��������</td>
                    </tr>';
			$counter++;
		}
		elseif($arResult["SHEDULE"][$i]['NOTES'] == 'ACT'){
			$tbl .= '<tr>
                        <td>'.$arResult["SHEDULE"][$i]['TITLE'].'</td>
                        <td>��������: '.$arResult["SHEDULE"][$i]['COMPANY'].'<br />�������������: '.$arResult["SHEDULE"][$i]["REP"].'</td>
                        <td align="center">������������</td>';
			if($arResult["SHEDULE"][$i]['HALL']){
				 $tbl .= '<td>'.$arResult["SHEDULE"][$i]['HALL'].', '.$arResult["SHEDULE"][$i]['TABLE'].'</td>
                        </tr>';
			}
			else{
				$tbl .= '<td> </td>
                        </tr>';
			}
			$counter++;
		}
		else{
			if($arResult["SHEDULE"][$i]['STATUS'] == 'MY'){
				$tbl .= '<tr>
                                <td>'.$arResult["SHEDULE"][$i]['TITLE'].'</td>
                                <td>��������: '.$arResult["SHEDULE"][$i]['COMPANY'].'<br />�������������: '.$arResult["SHEDULE"][$i]["REP"].'</td>
                                <td align="center">�� ����</td>';
				if($arResult["SHEDULE"][$i]['HALL']){
					 $tbl .= '<td>'.$arResult["SHEDULE"][$i]['HALL'].', '.$arResult["SHEDULE"][$i]['TABLE'].'</td>
                                </tr>';
				}
				else{
					$tbl .= '<td> </td>
                                </tr>';
				}
			}
			elseif($arResult["SHEDULE"][$i]['STATUS'] == 'ADM'){
				$tbl .= '<tr>
                                <td>'.$arResult["SHEDULE"][$i]['TITLE'].'</td>
                                <td>��������: '.$arResult["SHEDULE"][$i]['COMPANY'].'<br />�������������: '.$arResult["SHEDULE"][$i]["REP"].'</td>
                                <td align="center">��������� ���������������</td>';
				if($arResult["SHEDULE"][$i]['HALL']){
					 $tbl .= '<td>'.$arResult["SHEDULE"][$i]['HALL'].', '.$arResult["SHEDULE"][$i]['TABLE'].'</td>
                                </tr>';
				}
				else{
					$tbl .= '<td> </td>
                                </tr>';
				}
			}
			else{
				 $tbl .= '<tr>
                                <td>'.$arResult["SHEDULE"][$i]['TITLE'].'</td>
                                <td>��������: '.$arResult["SHEDULE"][$i]['COMPANY'].'<br />�������������: '.$arResult["SHEDULE"][$i]["REP"].'</td>
                                <td align="center">���</td>';
                                if($arResult["SHEDULE"][$i]['HALL']){
                                    $tbl .= '<td>'.$arResult["SHEDULE"][$i]['HALL'].', '.$arResult["SHEDULE"][$i]['TABLE'].'</td>
                                </tr>';
                                }
                                else{
                                    $tbl .= '<td> </td>
                                </tr>';
                                }
			}
			$counter++;
		}
	}*/
	$tbl .= '</table>';
	$pdf->writeHTML($tbl, true, false, false, false, '');
        	
	$pdf->setXY(20,$pdf->getY() + 2);
	$y = $pdf->getY();
$pdf->SetFont('times','',8);
	$html = '<b>����� ����������:</b> ����� St Regis, ��� ���������� (����������, 12)';
	$pdf->writeHTMLCell('', '', 20, $y, $html, $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true);

	$pdf->setY($pdf->getY() + 5);
	$y = $pdf->getY();
	$html = '
<p><b>����������� ������</b> ���������� � <b>10:30.</b> ��� ����� � ��� ����� ����������� ������ �����������, �� ������� �� �������
�������� ���� �����. �������� ���� ��������, ��� �� ������� ������ ����������� ������ ��������, ������� ��������� �����������.</p>';
	$pdf->writeHTMLCell('', '', 20, $y, $html, $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true);

	$pdf->Output("print.pdf", I);
?>