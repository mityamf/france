<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("LTM Kiev 2012 in details");
?> 
<h1>LTM Kiev 2012 details:</h1>
 
<ul> 
  <li>All spaces were <b>sold out within 3 weeks</b> from the opening of registration</li>
 
  <li><b>124 exhibitors</b> (maximum capacity)</li>
 
  <li><b>209 guests</b> at the morning session</li>
 
  <li><b>516 guests</b> at the evening session</li>
 
  <li><b>58 hosted buyers</b> from across Ukraine (Dnepropetrovsk, Zaporizhie, Kharkiv, Odessa, Lviv, Chernovtsi)</li>
 
  <li><b>1488 appointments</b> were held during the morning session</li>
 </ul>
 
<p><b>A few comments from exhibitors:</b></p>
 
<p><b style="color: rgb(102, 204, 255);">Kirsten Simon, The Ritz-Carlton Hotel Company:</b> Very professional organization, well-done! The event which must to attend.</p>
 
<p><b style="color: rgb(102, 204, 255);">Eugene Feklistov, Banyan Tree Resorts Maldives:</b> Organization of the event was perfect, as always with you!!!
  <br />
 Thanks a lot and we are looking to have great business for 2013 from Ukraine following our fruitful meetings we had at LTM.</p>
 
<p><b style="color: rgb(102, 204, 255);">Vadim Ovcharov, Mandarin Oriental Hotel Group:</b> From the very beginning I had no doubt that this would be a successful and well-organized event &ndash; the exhibition in Moscow in March and the Kiev event in September proved yet again that the experience and reputation of the organizers were priceless assets in organizing such a large-scale event. From the point of view of Mandarin Oriental, the greatest benefit of this event in my opinion was the large number of &laquo;new faces&raquo; among the visitors; and their &laquo;quality&raquo; , if I may use such a word, surpassed the highest of expectations. I would like to make special mention of an additional new service &ndash; the organization of transport , help with visa applications and booking of accomodation in Kiev. This is very important for hoteliers that are not so familair with the Ukrainian market as it helps us save one of our most important assets &ndash; our time. Definitely see you there next year!</p>
 
<p><b style="color: rgb(102, 204, 255);">Julie Zelditch, Hotel Byblos St. Tropez:</b> An enormous thank you for your super-professionalism and for your attentive approach to business and to people. You are the best and unique. What you achieved in Kiev cannoty be praised highly enough.Julie Zelditch, Hotel Byblos St. Tropez: An enormous thank you for your super-professionalism and for your attentive approach to business and to people. You are the best and unique. What you achieved in Kiev cannoty be praised highly enough.</p>
 
<p><b style="color: rgb(102, 204, 255);">Anna Maximova, Sun Resorts:</b> Thank you very much for a well-organized event in Kiev. Everything was of the highest quality, as always!</p>
 
<p><b style="color: rgb(102, 204, 255);">Denis Dolmatov, Destinations of the World:</b> I personally think the event was absolutely superb and very professionally organized, we loved it and we are already getting the right business from new accounts from that show.</p>
 
<p><b style="color: rgb(102, 204, 255);">Polina Raeyvskaya, Le Mirador Kempinski:</b> In our opinion the organization was perfect and we really enjoyed all the event. It was very productive as well! Thank you to you and your team!</p>
 
<p><b style="color: rgb(102, 204, 255);">Justin Jacob, Connect World Travel &amp; Tourism:</b> I would like to express my sincere thanks to you and your team for all your hard work for well organizing such a wonderful event in Kiev. It was rather impressive and extremely fruitful. We have been participating in various trade shows all around the globe along with various exhibition organizers and Government bodies, with each one lasting from 3-4 days, but I would say this was the best. LTM Kiev was short, simple but the most effective and economical. There was no waste of time, right from the first appointment to the last one with the hosted buyers, everything was superbly organised. Post lunch, we even got the opportunity to meet every single hosted buyer to discuss business. So practically either as an exhibitor or as a participant, we had every opportunity to meet all prospective business partners. Right from fixing of appointments, complete information packs and kits provided, along with complete guest lists etc, these all show us how very professional you all are. </p>
 
<p><b style="color: rgb(102, 204, 255);">Mario Cardone, Il Pellicano and La Posta Vecchia Hotels:</b> The organization was superb and both the morning and afternoon were well ran and highly productive! It was great to have an opportunity both to be searched for (morning session) and look out for others (afternoon session) within the same context &ndash; great job!</p>
  
<p><b style="color: rgb(102, 204, 255);">Marina Werbitzky, Genolier Swiss Medical Network:</b> Bravo, excellent organization, high-quality participants, a professional atmosphere, a good hotel, good food. It was a pleasure to work with you!</p>
 
<p><b style="color: rgb(102, 204, 255);">Natalia Kuznetsova, The Westbury, London:</b> Everythign was just wonderful!!! You were excellent. I have already written to my people in London to say that we should participate everywhere and in everything that your organize.</p>
 
<p><b style="color: rgb(102, 204, 255);">Nadia Prokhorets, SinElite:</b> For us this exhibition was VERY successful!</p>
 
<p><b style="color: rgb(102, 204, 255);">Nina Kamenskaya, Ars-Vitae Hotels Representing Company:</b> It is totally obvious that the exhibition was a great success, and I am absolutely sure that nobody other than you could have organized it so well! We will, of course, take part in your future projects.</p>
 
<p><b style="color: rgb(102, 204, 255);">Anna Naumenko, Top Signature:</b> On behalf of TOP Signature company (Moscow hotel representative company) we would like to thank you and your team for the professional organisation of the Luxury Travel Mart Showcase. It was a real pleasure to participate in such a fabulous event with our hotel Maradiva Hotel&amp;Resorts (Maia, Seychelles) and The Nam Hai hotel (Hoi An, Vietnam) - GHM Management Company. Participation in such an event is an excellent and rare opportunity to promote our hotels on the Ukrainian market and to get acquainted with the Luxury travel companies. I hope that next time we can attract much more of our hotels to participate in LTM.</p>
 
<p><b style="color: rgb(102, 204, 255);">Maria Lysenko, Exclusive Travel:</b> I would like to thank you for the wonderful organization of the event in Kiev!</p>
 
<p><b style="color: rgb(102, 204, 255);">Anna Dobrovolskaya, Sayyama Travel:</b> Thank you for the excellent organization of the Luxury Travel Mart Kiev.</p>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>