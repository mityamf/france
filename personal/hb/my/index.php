<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("My schedule for Hosted Buyers Session");
?> 
<div class="text_page"> 
  <p><strong>Time</strong>: 15:30 &ndash; 18:00</p>
 
  <p><strong>Place</strong>: Conference Hall I &amp; II.</p>
 
  <p><strong>Format</strong>: Buyers will be seated, and exhibitors will move from one buyer to another, without pre- scheduled appointments. 5 minutes strictly for each appointment; at the end of each appointment a bell will ring. Please respect other exhibitors and move forward to another buyer immediately after the bell.</p>
 
  <p>Please note that if you are sharing space with another hotel or colleague, you must attend all meetings with the hosted buyers together, no separate appointments are allowed in this case.</p>
 
<!--  <p>Click <a style="color:#42c4f2" href="/files/hb_plan.pdf" target="_blank" >here</a> to download Hosted Buyers floor plan, on which you will see how the tables are assigned in all halls. If you know where the people you would like to see are located, this will maximize your benefit from the Hosted Buyers session.</p> -->
<p>Hosted Buyers floor plan will be uploaded by the 20th of September 2013.</p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>