<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Register for LTM 2014");
?> 
<div class="text_page"> 
  <p>The 4rd Luxury Travel Mart Kiev will be held in September 2014 (exact date to be confirmed).</p>
 
  <p><strong>Luxury Travel Mart Kiev 2014:</strong></p>
 
  <ul> 	 
    <li>Participation is limited to 148 exhibitors only; confirmation of participation is based on market interest.</li>
   	 
    <li>Participating companies will include five-star hotels and resorts, elite car companies, cruise and yacht charter companies, private jet charters, DMC and luxury operators</li>
   	 
    <li>Hosted buyers program, with over 40-50 companies from Ukrainian regions and CIS.</li>
   	 
    <li>Pre-selected buyers and visitors by invitation only, which will guarantee a high-caliber of clientele and travel industry professionals</li>
   	 </ul>
 
  <p><strong>Participation fee scheme for LTM Kiev 2014 is as follows:</strong>:</p>
 
  <p>2500 euro &ndash; if registered and paid by February 28, 2014 
    <br />
   2750 euro &ndash; if registered and paid after February 28, 2014</p>
 
  <p>All taxes are included in the above-mentioned fees. Payment terms are strict, and not negotiable.</p>
 
  <p><strong>Included in the fee:</strong> participation at all events of the Luxury Travel Mart &ndash; morning session for top managers and directors of Ukrainian travel trade with pre-scheduled appointments (maximum 17), midday networking lunch, session for hosted buyers and evening workshop-cocktail for over 300 travel agents, corporate clients and press.</p>
 
  <p>All <strong>cancelation</strong> of participation must be received in writing by fax or email. A full refund of the participation fee will be made if canceled by March 30, 2014. If canceled in the period from March 30, 2014 to May 15, 2014 - 50% of the participation fee will be charged for the cancelation. No refunds of the participation fees will be made if canceled after the 15th of May 2014.</p>
 
  <p id="reg_next"><a href="/personal/next_reg/reg/regist.php" >Register for LTM 2014 &gt;</a></p>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>