<?
$MESS ['COMP_FORM_GROUP_PARAMS'] = "Component parameters";
$MESS ['COMP_FORM_PARAMS_WEB_FORM_ID'] = "Web form ID";
$MESS ['COMP_FORM_PARAMS_RESULT_ID'] = "Result ID";
$MESS ['COMP_FORM_PARAMS_LIST_URL'] = "Result list page";
$MESS ['COMP_FORM_PARAMS_EDIT_URL'] = "Result editing page";
$MESS ['COMP_FORM_PARAMS_SUCCESS_URL'] = "Success page URL";
$MESS ['COMP_FORM_PARAMS_CHAIN_ITEM_TEXT'] = "Name of additional navigation chain item";
$MESS ['COMP_FORM_PARAMS_CHAIN_ITEM_LINK'] = "Link for additional navigation chain item";
$MESS ['COMP_FORM_PARAMS_IGNORE_CUSTOM_TEMPLATE'] = "Ignore custom template";
$MESS ['COMP_FORM_PARAMS_USE_EXTENDED_ERRORS'] = "Use extended error messages output";
$MESS ['COMP_FORM_PARAMS_STEP'] = "Choose is there a meeting";
$MESS ['COMP_FORM_PARAMS_SECOND_FORM'] = "Choose the second form id";
$MESS ['COMP_FORM_PARAMS_REGIST_USER'] = "New user registration";
$MESS ['COMP_FORM_PARAMS_REGIST_GROUP'] = "A group for new user";
$MESS ['COMP_FORM_PARAMS_LOGIN_FIELD'] = "Login field";
$MESS ['COMP_FORM_PARAMS_PASS_FIELD'] = "Passworf field";
$MESS ['COMP_FORM_PARAMS_EMAIL_FIELD'] = "Email field";
$MESS ['COMP_FORM_PARAMS_CHECK_LOGIN'] = "Field for check login";
?>