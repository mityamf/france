<?
$MESS ['F_DATE_TIME_FORMAT'] = "Datum- und Zeitformat";
$MESS ['F_DATE_FORMAT'] = "Datumsformat";
$MESS ['F_DISPLAY_PANEL'] = "Buttons f�r diese Komponente zum administrativen Panel hinzuf�gen";
$MESS ['PM_DEFAULT_FID'] = "Ordner ID";
$MESS ['F_PAGE_NAVIGATION_TEMPLATE'] = "Vorlagenname f�r die Seitennavigation";
$MESS ['PM_PER_PAGE'] = "Beitr�ge pro Seite";
$MESS ['PM_EDIT_TEMPLATE'] = "Seite zum Bearbeiten (Erstellen) privater Nachrichten";
$MESS ['PM_READ_TEMPLATE'] = "Seite zum Lesen der privaten Nachrichten";
$MESS ['PM_LIST_TEMPLATE'] = "Seite mit  pers�nlichen Nachrichten";
$MESS ['F_PROFILE_VIEW_TEMPLATE'] = "Seite mit dem Userprofil";
$MESS ['F_SET_NAVIGATION'] = "Breadcrumb-Navigation anzeigen";
$MESS ['F_URL_TEMPLATES'] = "URL verwalten";
$MESS ['F_PM_FOLDER'] = "Seite mit der Verzeichnisverwaltung f�r pers�nlichen Nachrichten";
?>