<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true

    )die();
/* --------------- TO DO ------------------- */

$arResult["ERROR_MESSAGE"] = "";
$arResult["MESSAGE"] = "";

if (strLen($arParams["PATH_TO_KAB"]) <= 0) {
    $arParams["PATH_TO_KAB"] = "/admin/";
}

if (strLen($arParams["GROUP_ID"]) <= 0) {
    $arParams["GROUP_ID"] = "CURE";
}

if (strLen($arParams["AUTH_PAGE"]) <= 0) {
    $arParams["AUTH_PAGE"] = "/admin/login.php";
}

if (strLen($arParams["USER"]) <= 0) {
    $arResult["ERROR_MESSAGE"] = "�� ������� ������ �� �������������!<br />";
}

if (strLen($arParams["FORM_ID"]) <= 0) {
    $arResult["ERROR_MESSAGE"] = "�� ������� ������ �� ����������� �������������!<br />";
}
/* --------------------------------------------------- */
//          ��������� ������ �� ��������             //
/* --------------------------------------------------- */
$letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0";
$letter_filt = '';
$isLetter = true;
$thisUrl = $APPLICATION->GetCurPage();
for ($i = 0; $i < 27; $i++) {
    if (isset($_REQUEST['letter']) && $_REQUEST['letter'] == $letters[$i]) {
        if ($i == 26) {
            $letter_filt .= '<span style="margin:0 3px 0; color:#cc0033; font-weight:bold;">0-9</span>';
            $isLetter = false;
        } else {
            $letter_filt .= '<span style="margin:0 3px 0; color:#cc0033; font-weight:bold;">' . $letters[$i] . '</span>';
            $isLetter = false;
        }
    } else {
        if ($i == 26) {
            $letter_filt .= '<a href="' . $thisUrl . '?letter=' . $letters[$i] . '" style="margin:0 3px 0;">0-9</a>';
        } else {
            $letter_filt .= '<a href="' . $thisUrl . '?letter=' . $letters[$i] . '" style="margin:0 3px 0;">' . $letters[$i] . '</a>';
        }
    }
}
if ($isLetter) {
    $letter_filt = $letter_filt . '<span style="margin:0 3px 0; color:#cc0033; font-weight:bold;">All</span>';
} else {
    $letter_filt = $letter_filt . '<a href="' . $thisUrl . '" style="margin:0 3px 0;">All</a>';
}
$arResult["FILTER"]["ALP"] = $letter_filt;

/* --------------------------------------------------- */
//           ��������� ����� ��� �������             //
/* --------------------------------------------------- */
if ($arResult["ERROR_MESSAGE"] == '') {
    //������ �������������
    $filter = Array(
        "GROUPS_ID" => Array($arParams["USER"])
    );
    $rsUsers = CUser::GetList(($by = "work_company"), ($order = "asc"), $filter, array("SELECT" => array("UF_*"))); // �������� �������������
    $countUsers = 0;
    $resultFormId = "";
    while ($arUsersTemp = $rsUsers->Fetch()) {
        $arUsers[$countUsers]["ID"] = $arUsersTemp["ID"];
        if ($arParams["GROUP_ID"] == "PREV") {
            $arUsers[$countUsers]["UF_ANKETA"] = $arUsersTemp["UF_ANKETA_PREV"];
        } else {
            $arUsers[$countUsers]["UF_ANKETA"] = $arUsersTemp["UF_ANKETA"];
        }
        $arUsers[$countUsers]["UF_PAY_COUNT"] = $arUsersTemp["UF_PAY_COUNT"];
        $resultFormId .= " | " . $arUsers[$countUsers]["UF_ANKETA"];
        $countUsers++;
    }
    $resultFormId = substr($resultFormId, 3);
    $arResult["USERS"]["COUNT"] = $countUsers;
	//echo "<pre>"; print_r($arUsers); echo "</pre>";

    //���������� �������������
    CForm::GetResultAnswerArray($arParams["FORM_ID"], $arrColumns, $arrAnswers, $arrAnswersVarname, array("RESULT_ID" => $resultFormId));

    //������ ������� ��� �������
    $countReal = 0;
    $QUESTION_ID = "";
    $arResult["FIELDS"]["COUNT"] = 0;
    foreach ($arrColumns as $columnName) {
        $arResult["FIELDS"][$countReal]["ID"] = $columnName["ID"];
        $arResult["FIELDS"][$countReal]["TITLE"] = $columnName["TITLE"];
        $countReal++;
        if ($columnName["TITLE"] == "Area of business") {
            $QUESTION_ID = $columnName["ID"];
        }
    }
    $arResult["FIELDS"]["COUNT"] = $countReal;

    //������ ���������
    $arCategory = array();
    $countCategory = 0;
    $filterAr = array();
    $rsAnswersMean = CFormAnswer::GetList($QUESTION_ID, $by = "s_sort", $order = "asc", array(), $is_filtered);
    while ($arAnswer = $rsAnswersMean->Fetch()) {
        $arCategory[$countCategory]["TITLE"] = $arAnswer["MESSAGE"];
        $arCategory[$countCategory]["COMPANYS"] = array();
        $arCategory[$countCategory]["COUNT"] = 0;
        $countCategory++;
    }
	$fullArr["COUNT"] = 0;
	$fullArr["DATA"] = array();

    for ($i = 0; $i < $countUsers; $i++) {
        for ($k = 0; $k < $countCategory; $k++) {
            $arResult["USERS"][$i]["ID"] = $arUsers[$i]["ID"];
            $arResult["USERS"][$i]["ANKETA"] = $arUsers[$i]["UF_ANKETA"];
            $flag = false;
            $flagCat = false;
            foreach ($arrAnswers[$arUsers[$i]["UF_ANKETA"]][$QUESTION_ID] as $ansField) {
                if ($ansField["ANSWER_TEXT"] == $arCategory[$k]["TITLE"]) {
                    $flagCat = true;
                }
                if (isset($_REQUEST['letter']) && $_REQUEST['letter'] != '') {
                    if ($arParams["GROUP_ID"] == "PREV") {
                        if ($_REQUEST['letter'] == '0' && ctype_digit($arrAnswers[$arUsers[$i]["UF_ANKETA"]][228][687]["USER_TEXT"][0])) {
                            $flag = true;
                        } else {
                            if ($arrAnswers[$arUsers[$i]["UF_ANKETA"]][228][687]["USER_TEXT"][0] == $_REQUEST['letter']) {
                                $flag = true;
                            } else {
                                $flag = false;
                            }
                        }
                    } else {
                        if ($_REQUEST['letter'] == '0' && ctype_digit($arrAnswers[$arUsers[$i]["UF_ANKETA"]][6][10]["USER_TEXT"][0])) {
                            $flag = true;
                        } else {
                            if ($arrAnswers[$arUsers[$i]["UF_ANKETA"]][6][10]["USER_TEXT"][0] == $_REQUEST['letter']) {
                                $flag = true;
                            } else {
                                $flag = false;
                            }
                        }
                    }
                } else {
                    $flag = true;
                }
            }
            if ($flag && $flagCat) {
                for ($j = 0; $j < $countReal; $j++) {
                    foreach ($arrAnswers[$arUsers[$i]["UF_ANKETA"]][$arResult["FIELDS"][$j]["ID"]] as $ansMeaning) {
                        $arCategory[$k]["COMPANYS"]["ID"][$arCategory[$k]["COUNT"]] = $arUsers[$i]["ID"];
                        if ($ansMeaning["TITLE"] == "Company or Hotel") {
                            $arCategory[$k]["COMPANYS"]["COMPANY"][$arCategory[$k]["COUNT"]] = $ansMeaning["USER_TEXT"];
                        } elseif (strpos($ansMeaning["TITLE"], "Short company description") !== false) {
                            $arCategory[$k]["COMPANYS"]["DESC"][$arCategory[$k]["COUNT"]] = $ansMeaning["USER_TEXT"];
                        } elseif ($ansMeaning["TITLE"] == "Company's web-site") {
                            $arCategory[$k]["COMPANYS"]["SITE"][$arCategory[$k]["COUNT"]] = $ansMeaning["USER_TEXT"];
                        } elseif ($ansMeaning["TITLE"] == "First Name") {
                            $arCategory[$k]["COMPANYS"]["NAME"][$arCategory[$k]["COUNT"]] = $ansMeaning["USER_TEXT"];
                        } elseif ($ansMeaning["TITLE"] == "Last Name") {
                            $arCategory[$k]["COMPANYS"]["LAST_NAME"][$arCategory[$k]["COUNT"]] = $ansMeaning["USER_TEXT"];
                        } elseif ($ansMeaning["TITLE"] == "Job Title") {
                            $arCategory[$k]["COMPANYS"]["JOB"][$arCategory[$k]["COUNT"]] = $ansMeaning["USER_TEXT"];
                        } elseif ($ansMeaning["TITLE"] == "First Name College") {
                            $arCategory[$k]["COMPANYS"]["COLLEGE_NAME"][$arCategory[$k]["COUNT"]] = $ansMeaning["USER_TEXT"];
                        } elseif ($ansMeaning["TITLE"] == "Last Name College") {
                            $arCategory[$k]["COMPANYS"]["COLLEGE_LAST_NAME"][$arCategory[$k]["COUNT"]] = $ansMeaning["USER_TEXT"];
                        } elseif ($ansMeaning["TITLE"] == "Job Title College") {
                            $arCategory[$k]["COMPANYS"]["COLLEGE_JOB"][$arCategory[$k]["COUNT"]] = $ansMeaning["USER_TEXT"];
                        }
                    }
                }
				$fullArr["DATA"][$fullArr["COUNT"]]["ID"] = $arCategory[$k]["COMPANYS"]["ID"][$arCategory[$k]["COUNT"]];
				$fullArr["DATA"][$fullArr["COUNT"]]["DESC"] = $arCategory[$k]["COMPANYS"]["DESC"][$arCategory[$k]["COUNT"]];
				$fullArr["DATA"][$fullArr["COUNT"]]["COMPANY"] = $arCategory[$k]["COMPANYS"]["COMPANY"][$arCategory[$k]["COUNT"]];
				$fullArr["DATA"][$fullArr["COUNT"]]["SITE"] = $arCategory[$k]["COMPANYS"]["SITE"][$arCategory[$k]["COUNT"]];
				$fullArr["DATA"][$fullArr["COUNT"]]["NAME"] = $arCategory[$k]["COMPANYS"]["NAME"][$arCategory[$k]["COUNT"]];
				$fullArr["DATA"][$fullArr["COUNT"]]["LAST_NAME"] = $arCategory[$k]["COMPANYS"]["LAST_NAME"][$arCategory[$k]["COUNT"]];
				$fullArr["DATA"][$fullArr["COUNT"]]["JOB"] = $arCategory[$k]["COMPANYS"]["JOB"][$arCategory[$k]["COUNT"]];
				$fullArr["DATA"][$fullArr["COUNT"]]["COLLEGE_NAME"] = $arCategory[$k]["COMPANYS"]["COLLEGE_NAME"][$arCategory[$k]["COUNT"]];
				$fullArr["DATA"][$fullArr["COUNT"]]["COLLEGE_LAST_NAME"] = $arCategory[$k]["COMPANYS"]["COLLEGE_LAST_NAME"][$arCategory[$k]["COUNT"]];
				$fullArr["DATA"][$fullArr["COUNT"]]["COLLEGE_JOB"] = $arCategory[$k]["COMPANYS"]["COLLEGE_JOB"][$arCategory[$k]["COUNT"]];
                $arCategory[$k]["COUNT"]++;
				$fullArr["COUNT"]++;
                break;
            }
            //print_r($arrAnswers[$arUsers[$i]["UF_ANKETA"]][$QUESTION_ID]);
        }
    }

    //���������� ��������������� �������
    for ($k = 0; $k < $countCategory; $k++) {
        array_multisort($arCategory[$k]["COMPANYS"]["COMPANY"], $arCategory[$k]["COMPANYS"]["ID"], $arCategory[$k]["COMPANYS"]["DESC"], $arCategory[$k]["COMPANYS"]["SITE"], $arCategory[$k]["COMPANYS"]["NAME"], $arCategory[$k]["COMPANYS"]["LAST_NAME"], $arCategory[$k]["COMPANYS"]["JOB"], $arCategory[$k]["COMPANYS"]["COLLEGE_NAME"], $arCategory[$k]["COMPANYS"]["COLLEGE_LAST_NAME"],  $arCategory[$k]["COMPANYS"]["COLLEGE_JOB"]);
    }
    $arResult["COUNT"] = $countCategory;
    $arResult["CATEGORIES"] = $arCategory;
	$arResult["FULL"] = $fullArr;
}
//echo "<pre>"; print_r($arResult["FULL"]); echo "</pre>";
//echo "<pre>"; print_r(); echo "</pre>";

$this->IncludeComponentTemplate();
?>