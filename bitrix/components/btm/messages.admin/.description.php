<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("ADMIN_MAIN"),
	"DESCRIPTION" => GetMessage("ADMIN_MAIN_DESC"),
	"ICON" => "/images/icon.gif",
	"PATH" => array(
		"ID" => "utility",
	),
);
?>