<?
$MESS ['APPOINTMENT_EDIT_APP_ID_ERROR'] = "Information on appointment not entered! Possible incorrect request.";
$MESS ['APPOINTMENT_EDIT_APP_ACTION_ERROR'] = "Information on action for appointment not entered! Possible incorrect request.";
$MESS ['APPOINTMENT_EDIT_AUTH_ERROR'] = "You are not authorized!";
$MESS ['APPOINTMENT_EDIT_BLOCKED'] = "Appointments schedule blocked by the organizers";
$MESS ['APPOINTMENT_EDIT_EDIT_ERROR'] = "You may not change this appointment.";
$MESS ['APPOINTMENT_EDIT_APP_CHANGE_ERROR'] = "Error, please try again later.";
$MESS ['APPOINTMENT_EDIT_APP_ACCEPT_ERROR'] = "You cannot approve this appointment.";
$MESS ['APPOINTMENT_EDIT_APP_ACCEPT'] = "Appointment successfully confirmed.";
$MESS ['APPOINTMENT_EDIT_APP_ACCEPT_DEJA'] = "This appointment has already been confirmed.";
$MESS ['APPOINTMENT_EDIT_APP_DECLINE'] = "Appointment successfully cancelled.";
$MESS ['APPOINTMENT_EDIT_APP_DECLINE_DEJA'] = "This appointment has already been cancelled.";
$MESS ['APPOINTMENT_EDIT_APP_EXIST_ERROR'] = "Appointment not found.";
?>