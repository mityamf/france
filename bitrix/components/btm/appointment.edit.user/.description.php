<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("ADMIN_USER_COUNT"),
	"DESCRIPTION" => GetMessage("ADMIN_USER_COUNT_DESC"),
	"ICON" => "/images/icon.gif",
	"PATH" => array(
		"ID" => "utility",
	),
);
?>