<?
$MESS  ['APPOINTMENT_USER_ERROR']  = "Recipient information not entered! Possible incorrect request.";
$MESS ['APPOINTMENT_NO_USER'] = "Recipient information not entered! Possible incorrect request.";
$MESS ['APPOINTMENT_AUTH_ERROR'] = "You are not authorized!";
$MESS ['APPOINTMENT_IS_BLOCKED'] = "Scheduling of appointments blocked by administration";
$MESS ['APPOINTMENT_TIME_RECIVER_BUSY'] = "The user is engaged at this time. Please try another time slot.";
$MESS ['APPOINTMENT_TIME_SENDER_BUSY'] = "You are already booked at this time. Please try another time slot.";
$MESS ['APPOINTMENT_SEND_ERROR'] = "Unfortunately, your request was not sent. Please try again later.";
$MESS ['APPOINTMENT_GROUP_ERROR'] = "You may not schedule appointments with participants from your group!";
$MESS ['APPOINTMENT_PERMISSION_ERROR'] = "You are not authorized to view this page!";
?>