<?
$MESS['ADMIN_MENU_ERROR_ENTER'] = " You are not authorized to enter this section.";
$MESS['F_NO_MODULE'] = "Forum module not found.";
$MESS['UPDATE_SUCCESS'] = "Thank you! Your registration information has been updated.";
$MESS['SAVE_SUCCESS'] = " Thank you! Your registration information has been saved.";
$MESS['FORM_NEV_YEAR_SAVE_ERROR'] = "There was an error saving your information, please try again later.";
$MESS['FORM_NEV_YEAR_CREATE_ERROR'] = "Failed to create form, please try later.";
$MESS['FORM_NEV_YEAR_ERROR'] = "Error, contract administration.";
$MESS['FORM_NEV_YEAR_ALREADY_CREATE'] = "You have already registered for next year. You can edit some of your information here.";
?>