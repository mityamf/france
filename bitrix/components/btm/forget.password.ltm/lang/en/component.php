<?
$MESS['ADMIN_MENU_ERROR_ENTER'] = "You are not authorized to enter this section.";
$MESS['F_NO_MODULE'] = "Forum module not found.";
$MESS['IS_BLOCKED'] = "Personal cabinets blocked by administrators.";
$MESS['PASS_OK'] = "The password has been sent to the e-mail provided by you";
$MESS['PASS_ERROR'] = "Error, try again later";
$MESS['PASS_WRONG_EMAIL'] = "There is no user registered with the e-mail provided.";
$MESS['PASS_WRONG_LOGIN'] = "There is no user registered with the login provided.";
$MESS['PASS_WRONG_USER'] = "This email address is not registered in our database.";
$MESS['AUTH_HetEAD'] = "Recovering password";
?>
