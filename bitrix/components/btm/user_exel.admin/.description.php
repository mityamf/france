<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("ADMIN_GUEST_EXEL"),
	"DESCRIPTION" => GetMessage("ADMIN_GUEST_EXEL_DESC"),
	"ICON" => "/images/icon.gif",
	"PATH" => array(
		"ID" => "utility",
	),
);
?>