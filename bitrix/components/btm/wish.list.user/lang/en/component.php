<?
$MESS['WISH_LIST_WTF_TO_RECEIVE_THIS']   =   " Recipient information not entered! Possible incorrect request.";
$MESS['WISH_LIST_WTF_IS_THIS_ACTION']   =   "Information on action not entered! Possible incorrect request.";
$MESS['WISH_LIST_NOT_LOGGED_IN'] = "You are not authorized!";
$MESS['WISH_LIST_MEETINGS_ARRANGMENT_LOCKED_BY_ADMIN'] = " Scheduling of appointments blocked by administration";
$MESS['WISH_LIST_SEND_REQUEST_TO_WISH_LIST'] = "Send request to Wish List";
$MESS['WISH_LIST_USER_ALREADY_IN_WISH_LIST'] = "This user is already on your wish list.";
$MESS['WISH_LIST_ALREADY_IN_USERS_WISH_LIST'] = "You are already on the wish list of this user.";
$MESS['WISH_LIST_REQUEST_SEND_SUCCESSFULLY'] = "Your request has been successfully sent";
$MESS['WISH_LIST_NO_PERMISSION_TO_VIEW_THIS_PAGE'] = "You are not authorized to view this page!";
?>









