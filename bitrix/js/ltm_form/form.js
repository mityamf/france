// JavaScript Document
var error_field = $("div.form_error");
var error_text = '';

function validate(){
	var f = document.guest_form;
	$("div.form_error").html("");

	var valid = check_field('���', notEmpty(f.form_text_128.value)) +
	  check_field('�������', notEmpty(f.form_text_129.value)) +
	  check_field('��������', notEmpty(f.form_text_130.value)) +
	  check_field('���������', notEmpty(f.form_text_131.value)) +
	  check_field('�����', notEmpty(f.form_text_132.value)) +
	  check_field('�����', notEmpty(f.form_text_133.value)) +
	  check_field('��� ������', notEmpty(f.form_text_156.value)) +
	  check_field('�������', notEmpty(f.form_text_157.value)) +
	  check_field('Email � ���� ����������� email', onlyEmail(f.form_email_159.value, f.form_email_conf.value)) +
	  check_field('�������������� e-mail', onlyEmailAlt(f.form_email_160.value)) +
	  /*check_field('����� ���������', moreChekes('form_checkbox_visit_format[]', 1));*/
	  check_field('����� ���������', moreOneChekes('form_checkbox_visit_format[]', 1));
	  if(valid == 0){
		 /*	  $("div.step1").hide();
			  $("div.step4").show();*/

         $("div.step1").hide();
         $("div.step2").show();
	  }
}

function validate_step2(){
	var f = document.guest_form;
	$("div.form_error").html("");
	var valid = check_field('�������� ���', onlyRussianLimit(f.form_text_167.value, 5)) +
	  check_field('������', onlyLimit(f.form_password_168.value, 6)) +
	  check_field('Email �������', onlyEmailAlt(f.form_text_172.value));
	  if(valid == 0){
		  $.ajax({  
				 type: "POST",  
				 url: "/exist_user.php",  
				 data: "username="+$("input[name='form_text_167']").val(),  
				 success: function(html){  
					 if(html == "Exist"){
						 $("div.form_error").append("������������ � ����� �������� ������ ��� ����������. ���������� ������ ������ ��������.<br />");
					}
					else{
						$("div.step2").hide();
						$("div.step3").show();
					}
				 }  
			 });
	  }
}

function validate_step3(){
	var f = document.guest_form;
	$("div.form_error").html("");
	var valid = check_field('�������� ������������ ��������', onlyMinMore(f.form_textarea_173.value, 1, 100000));
	  if(valid == 0){
		$("div.content_part form").submit();
	  }
}

function validate_step4(){
	var f = document.guest_form;
	$("div.form_error").html("");
	var valid = 0;
	if(valid == 0){
		$("div.content_part form").submit();
	}
}

function onlyRussian(s)
{
  if (notEmpty(s) == 0)
  {
    if (s.search(/[�-��-�]+/) != -1)
    {
      error_text = '����� ������������ ������ ��������';  
      return 1;
    }
    return 0;
  }
  return 1;
}

function onlyRussianAlt(s)
{
  if (s != "")
  {
    if (s.search(/[�-��-�]+/) != -1)
    {
      error_text = '����� ������������ ������ ��������';  
      return 1;
    }
    return 0;
  }
  return 0;
}

function onlyRussianLimit(s, lim)
{
  if (notEmpty(s) == 0)
  {
    if (s.search(/[�-��-�]+/) != -1)
    {
      error_text = '����� ������������ ������ ��������';  
      return 1;
    }
	if(s.length < lim){
	  error_text = '���������� ������ ������� '+lim+' ��������';
      return 1;
	}
    return 0;
  }
  return 1;
}

function onlyLimit(s, lim)
{
  if (notEmpty(s) == 0)
  {
	if(s.length < lim){
	  error_text = '���������� ������ ������� '+lim+' ��������';
      return 1;
	}
    return 0;
  }
  return 1;
}

function onlyRussianMinMore(s, limMin, limMax)
{
  if (notEmpty(s) == 0)
  {
    if (s.search(/[�-��-�]+/) != -1)
    {
      error_text = '����� ������������ ������ ��������';  
      return 1;
    }
	if(s.length < limMin){
	  error_text = '���������� ������ ������� '+limMin+' ��������';
      return 1;
	}
	var countProb = s.split(/ +(?:\S)/).length;
	  if (countProb >= limMax)
	  {
		error_text = '������������ ���������� ���� '+limMax+', �� ����� '+countProb+' ����.';
		return 1;
	  }
    return 0;
  }
  return 1;
}

function onlyMinMore(s, limMin, limMax)
{
  if (notEmpty(s) == 0)
  {
	if(s.length < limMin){
	  error_text = '���������� ������ ������� '+limMin+' ��������';
      return 1;
	}
	var countProb = s.split(/ +(?:\S)/).length;
	  if (countProb >= limMax)
	  {
		error_text = '������������ ���������� ���� '+limMax+', �� ����� '+countProb+' ����.';
		return 1;
	  }
    return 0;
  }
  return 1;
}

function notEmpty(s)
{
  if (s == "")
  {
    error_text = '����������� ��� ����������';
    return 1;
  }
  return 0;
}

function onlyEmail(s, s_conf)
{
  if (notEmpty(s) == 0)
  { 
    var oRegExp = /^[A-Za-z0-9][-\w]*(\.[A-Za-z0-9][-\w]*)*@[A-Za-z0-9][-\w]*(\.[A-Za-z0-9][-\w]*)*\.[a-zA-Z]{2,8}$/
    if (!oRegExp.test(s))
    {
      error_text = '������������ �������� email';  
      return 1;
    }
    if(s != s_conf){
      error_text = '�������� ����� �� ���������';  
	  return 1;
	}
	return 0;
  }
  return 1;
}

function onlyEmailAlt(s)
{
  if (s != "")
  { 
    var oRegExp = /^[A-Za-z0-9][-\w]*(\.[A-Za-z0-9][-\w]*)*@[A-Za-z0-9][-\w]*(\.[A-Za-z0-9][-\w]*)*\.[a-zA-Z]{2,8}$/
    if (!oRegExp.test(s))
    {
      error_text = '������������ �������� email';  
      return 1;
	}
	return 0;
  }
  return 0;
}

function moreChekes(checkName, minNumb)
{
	var f = document.guest_form;
	var counter = 0;

	for(var i = 0; i < f[checkName].length; i++)
	{
	  if(f[checkName][i].checked){
		  counter++;
	  }
	}


  if(counter < minNumb){
	if(minNumb == 1){
		error_text = '���������� ������� ������� '+minNumb+' �����';
	}
	else{
		error_text = '���������� ������� ������� '+minNumb+' �������';
	}
    return 1;
  }
  return 0;
}

function moreOneChekes(checkName, minNumb)
{
	var f = document.guest_form;
	var counter = 0;


	/*  ���� �������� ������ 1 �����*/
         if(f[checkName].checked){
		  counter++;
	  }



  if(counter < minNumb){
	if(minNumb == 1){
		error_text = '���������� ������� ������� '+minNumb+' �����';
	}
	else{
		error_text = '���������� ������� ������� '+minNumb+' �������';
	}
    return 1;
  }
  return 0;
}

function check_field(id, result)
{
	if(result == 1){
		$("div.form_error").append("���� "+id+": "+ error_text + "<br />");
	}
  return result;
}

function change_text(){
	if($(this).attr("name") == 'form_text_156'){
		if($(this).val() == '��� ������'){
			$(this).val('');
		}
	}
	if($(this).attr("name") == 'form_text_157'){
		if($(this).val() == '�������'){
			$(this).val('');
		}
	}
}

function back_text(){
	if($(this).attr("name") == 'form_text_156'){
		if($(this).val() == ''){
			$(this).val('��� ������');
		}
	}
	if($(this).attr("name") == 'form_text_157'){
		if($(this).val() == ''){
			$(this).val('�������');
		}
	}
}

    function is_array( mixed_var ) {  
        return ( mixed_var instanceof Array );  
    }