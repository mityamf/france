var error_text = '';

function check_form1()
{
  var f = document.form1;
  if (onlyEmail(f.EMAIL.value))
  {
    new Ajax.Request('/en/check_email.php', {
      method: 'post',
      postBody: 'email=' + f.EMAIL.value,
      onSuccess: function(transport) {
        var valid = check_field('NAME', onlyRussian(f.NAME.value)) +
          check_field('LAST_NAME', onlyRussian(f.LAST_NAME.value)) +
          check_field('WORK_COMPANY', onlyRussian(f.WORK_COMPANY.value)) +
          check_field('WORK_PROFILE', notEmpty(f.WORK_PROFILE.value)) +
          check_field('WORK_COUNTRY', onlyRussian(f.WORK_COUNTRY.value)) +
          check_field('WORK_CITY', onlyRussian(f.WORK_CITY.value)) +
          check_field('WORK_POSITION', onlyRussian(f.WORK_POSITION.value));
          
        if(transport.responseText == 1)
        {
          if (valid == 7) f.submit();
          else return false;
        }
        else
        {
          error_text = 'This email is already in use';
          return check_field('EMAIL', false);
        }
      }
    });
  }
}

function check_form2()
{
  var f = document.form1;
  var valid = check_field('NAME', onlyRussian(f.NAME.value)) +
    check_field('LAST_NAME', onlyRussian(f.LAST_NAME.value)) +
    check_field('WORK_COMPANY', onlyRussian(f.WORK_COMPANY.value)) +
    check_field('WORK_PROFILE', notEmpty(f.WORK_PROFILE.value)) +
    check_field('WORK_COUNTRY', onlyRussian(f.WORK_COUNTRY.value)) +
    check_field('WORK_CITY', onlyRussian(f.WORK_CITY.value)) +
    check_field('WORK_POSITION', onlyRussian(f.WORK_POSITION.value)) +
    check_field('EMAIL', onlyEmail(f.EMAIL.value)) +
    check_field('UF_ALT_EMAIL', onlyEmail0(f.UF_ALT_EMAIL.value)) +
    check_field('WORK_STREET', onlyRussian(f.WORK_STREET.value)) +
    check_field('UF_PERSONAL_SALUT', selectRadio(f.UF_PERSONAL_SALUT)) +
    check_field('PERSONAL_PHONE', onlyRussian(f.PERSONAL_PHONE.value)) +
    check_field('PERSONAL_FAX', onlyRussian0(f.PERSONAL_FAX.value)) +
    check_field('WORK_WWW', onlyRussian0(f.WORK_WWW.value)) +
    check_field('WORK_NOTES', onlyRussian(f.WORK_NOTES.value)) +
    check_field('UF_COL_NAME', onlyRussian0(f.UF_COL_NAME.value)) +
    check_field('UF_COL_LAST_NAME', onlyRussian0(f.UF_COL_LAST_NAME.value)) +
    check_field('UF_COL_WORK_POSITION', onlyRussian0(f.UF_COL_WORK_POSITION.value)) +
    check_field('UF_COL_EMAIL', onlyEmail0(f.UF_COL_EMAIL.value)) +
    check_field('UF_COL_ALT_EMAIL', onlyEmail0(f.UF_COL_ALT_EMAIL.value));
  
  if (valid == 20) return true;
  else return false;
}

function check_form3()
{
  var f = document.form1;
  var valid = check_field('UF_INTERPRETER', selectRadio(f.UF_INTERPRETER)) +
    check_field('UF_PREF_LANGUAGE', onlyRussian0(f.UF_PREF_LANGUAGE.value)) +
    check_field('UF_LAPTOP', selectRadio(f.UF_LAPTOP)) +
    check_field('UF_INTEREST_WORKSHOP', selectRadio(f.UF_INTEREST_WORKSHOP)) +
    check_field('UF_OTHER_OPPORT', selectRadio(f.UF_OTHER_OPPORT));
//    check_field('UF_FULL_HOTEL_NAME', onlyRussian0(f.UF_FULL_HOTEL_NAME.value)) +
//    check_field('PERSONAL_NOTES', onlyRussian0(f.PERSONAL_NOTES.value));
  
  if (valid == 5) return true;
  else return false;
}

function onlyRussian(s)
{
  if (notEmpty(s))
  {
    if (s.search(/[�-��-�]+/) != -1)
    {
      error_text = 'Only english letters';  
      return false;
    }
    return true;
  }
  return false;
}

function onlyRussian0(s)
{
  if (notEmpty(s))
  {
    if (s.search(/[�-��-�]+/) != -1)
    {
      error_text = 'Only english letters';  
      return false;
    }
    return true;
  }
  return true;
}

function onlyEmail(s)
{
  if (notEmpty(s))
  { 
    var oRegExp = /^[A-Za-z0-9][-\w]*(\.[A-Za-z0-9][-\w]*)*@[A-Za-z0-9][-\w]*(\.[A-Za-z0-9][-\w]*)*\.[a-zA-Z]{2,4}$/
    if (!oRegExp.test(s))
    {
      error_text = 'Please check your email address';  
      return false;
    }
    return true;
  }
  return false;
}

function onlyEmail0(s)
{
  if (notEmpty(s))
  { 
    var oRegExp = /^[A-Za-z0-9][-\w]*(\.[A-Za-z0-9][-\w]*)*@[A-Za-z0-9][-\w]*(\.[A-Za-z0-9][-\w]*)*\.[a-zA-Z]{2,4}$/
    if (!oRegExp.test(s))
    {
      error_text = 'Please check your email address';  
      return false;
    }
    return true;
  }
  return true;
}

function notEmpty(s)
{
  if (s == "")
  {
    error_text = 'This field is mandatory';
    return false;
  }
  return true;
}

function selectRadio(obj)
{
  var f = document.form1;
  for (var i = 0; i < obj.length; i++)
  {
    if (obj[i].checked) return true;
  }
  error_text = 'This field is mandatory';
  return false;
}

function check_field(id, result)
{
  if (result) hide_error(id + '_error');
  else show_error(id + '_error');
  return result;
}

function hide_error(id)
{
  document.getElementById(id).style.display = 'none';
  document.getElementById(id).innerHTML = '';
}

function show_error(id)
{
  document.getElementById(id).innerHTML = error_text;
  document.getElementById(id).style.display = 'block';
}
