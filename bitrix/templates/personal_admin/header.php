<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?$APPLICATION->ShowHead()?>
<title><?$APPLICATION->ShowTitle()?></title>
<script type="text/javascript">
	function newWind(url, width, height){
		var recHref = url;
		window.open(recHref,'particip_write', 'scrollbars=yes,resizable=yes,width='+width+', height='+height+', left='+(screen.availWidth/2-width/2)+', top='+(screen.availHeight/2-height/2)+'');
		return false;
	}
</script>
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
</head>
<body>
    <div class="marg">
        <?$APPLICATION->IncludeComponent(
            "btm:menu.admin",
            "",
            Array(
                "PATH_TO_KAB" => "/admin/",
                "GROUP_ID" => "1",
                "AUTH_PAGE" => "/admin/login.php"
            ),
        false
        );?>
        <div class="main_content">
