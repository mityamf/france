<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?$APPLICATION->ShowHead()?>
<title><?$APPLICATION->ShowTitle()?></title>
<meta http-equiv="X-UA-Compatible" content="IE=7" /> 
<script type="text/javascript" src="/bitrix/js/btm/change_totle.js"></script>
</head>
<body onload='accordionAddBehivior();'>
<?include($_SERVER['DOCUMENT_ROOT']."/bitrix/php_interface/admin_header.php")?>
<?$APPLICATION->ShowPanel();?>
<div class="marg">
    <div class="container">
    	<div class="top">
            <div class="logo"><a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.svg" alt="Logo" /></a></div>
            <!-- <div class="title"><img src="<?=SITE_TEMPLATE_PATH?>/images/title_spb_en.gif" alt="" /></div> -->
            <div class="title">Moscou, la Russie<br>
                <span class="date">le 3 Octobre 2018</span>
            </div>
        </div>
        <div class="content">
          <div class="left_col">
          <!--[if IE]>
              <div class="shadow"></div>
          <![endif]-->
              <div class="inner">
              <?$APPLICATION->IncludeComponent("bitrix:menu", "template", Array(
                  "ROOT_MENU_TYPE" => "left",	// ��� ���� ��� ������� ������
                  "MAX_LEVEL" => "1",	// ������� ����������� ����
                  "CHILD_MENU_TYPE" => "left",	// ��� ���� ��� ��������� �������
                  "USE_EXT" => "N",	// ���������� ����� � ������� ���� .���_����.menu_ext.php
                  "ALLOW_MULTI_SELECT" => "N",	// ��������� ��������� �������� ������� ������������
                  "MENU_CACHE_TYPE" => "N",	// ��� �����������
                  "MENU_CACHE_TIME" => "3600",	// ����� ����������� (���.)
                  "MENU_CACHE_USE_GROUPS" => "Y",	// ��������� ����� �������
                  "MENU_CACHE_GET_VARS" => "",	// �������� ���������� �������
                  ),
                  false
              );?>
              </div>
          </div>
          <div class="right_col">
          	<div class="content_part">