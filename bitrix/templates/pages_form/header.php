<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?$APPLICATION->ShowHead()?>
<title><?$APPLICATION->ShowTitle()?></title>
<script type="text/javascript" src="/bitrix/js/btm/change_totle.js"></script>
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

<script type="text/javascript" src="/bitrix/js/btm/prototype.js"></script>
<script type="text/javascript" src="/bitrix/js/btm/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="/bitrix/js/btm/lightbox.js"></script>
<script type="text/javascript" src="/bitrix/js/btm/form.js"></script>
<script type="text/javascript" src="/bitrix/js/btm/jquery.maxlength.js"></script>
<script type="text/javascript" src="/bitrix/js/btm/jquery.maxlength.js-min.js"></script>
<link rel="stylesheet" href="/bitrix/js/btm/css/lightbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/bitrix/js/jquery.js"></script>
<script type="text/javascript" src="/bitrix/js/ltm_form/form.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	    $("div.step1 #step1_send input").bind('click', validate);
	    $("div.step2 #step2_send input").bind('click', validate_step2);
	    $("div.step3 #step3_send input").bind('click', validate_step3);
	    $("div.step4 #step4_send input").bind('click', validate_step4);
		$("input[name='form_text_156']").bind('focus', change_text);
		$("input[name='form_text_157']").bind('focus', change_text);
		$("input[name='form_text_156']").bind('focusout', back_text);
		$("input[name='form_text_157']").bind('focusout', back_text);
	});
</script>
<script type="text/javascript" src="/bitrix/js/main/ajax.js"></script>
</head>
<body onload='accordionAddBehivior();'>
<?include($_SERVER['DOCUMENT_ROOT']."/bitrix/php_interface/admin_header.php")?>
<?$APPLICATION->ShowPanel();?>
<div class="marg">
	<div class="marg_area">
    	<div class="top"><a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.gif" width="935" height="64" alt="Luxury Travel Mart Kiev" /></a></div>
        <div class="adress"><img src="<?=SITE_TEMPLATE_PATH?>/images/adress.gif" width="223" height="41" /></div>
        <div class="content_marg">
        	<div class="leften_menu">
            <?$APPLICATION->IncludeComponent("bitrix:menu", "template", Array(
                "ROOT_MENU_TYPE" => "left",	// ��� ���� ��� ������� ������
                "MAX_LEVEL" => "1",	// ������� ����������� ����
                "CHILD_MENU_TYPE" => "left",	// ��� ���� ��� ��������� �������
                "USE_EXT" => "N",	// ���������� ����� � ������� ���� .���_����.menu_ext.php
                "ALLOW_MULTI_SELECT" => "N",	// ��������� ��������� �������� ������� ������������
                "MENU_CACHE_TYPE" => "N",	// ��� �����������
                "MENU_CACHE_TIME" => "3600",	// ����� ����������� (���.)
                "MENU_CACHE_USE_GROUPS" => "Y",	// ��������� ����� �������
                "MENU_CACHE_GET_VARS" => "",	// �������� ���������� �������
                ),
                false
            );?>
            <div class="banners">
            <?$APPLICATION->IncludeComponent("bitrix:news.list", "banners", Array(
                "DISPLAY_DATE" => "N",	// Display element date
                "DISPLAY_NAME" => "N",	// Display element title
                "DISPLAY_PICTURE" => "N",	// Display element preview picture
                "DISPLAY_PREVIEW_TEXT" => "Y",	// Display element preview text
                "AJAX_MODE" => "N",	// Enable AJAX mode
                "IBLOCK_TYPE" => "-",	// Type of information block (used for verification only)
                "IBLOCK_ID" => "1",	// Information block code
                "NEWS_COUNT" => "20",	// News per page
                "SORT_BY1" => "ACTIVE_FROM",	// Field for the news first sorting pass
                "SORT_ORDER1" => "DESC",	// Direction for the news first sorting pass
                "SORT_BY2" => "SORT",	// Field for the news second sorting pass
                "SORT_ORDER2" => "ASC",	// Direction for the news second sorting pass
                "FILTER_NAME" => "",	// Filter
                "FIELD_CODE" => "",	// Fields
                "PROPERTY_CODE" => "",	// Properties
                "CHECK_DATES" => "Y",	// Show only currently active elements
                "DETAIL_URL" => "",	// Detail page URL (from information block settings by default)
                "PREVIEW_TRUNCATE_LEN" => "",	// Maximum preview text length (for Text type only)
                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Date display format
                "DISPLAY_PANEL" => "N",	// Display panel buttons for this component
                "SET_TITLE" => "N",	// Set page title
                "SET_STATUS_404" => "N",	// Set 404 status if no element or section was found
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Include information block into navigation chain
                "ADD_SECTIONS_CHAIN" => "Y",	// Add Section name to breadcrumb navigation
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Hide link to the details page if no detailed description provided
                "PARENT_SECTION" => "1",	// Section ID
                "PARENT_SECTION_CODE" => "",	// Section code
                "CACHE_TYPE" => "A",	// Cache type
                "CACHE_TIME" => "3600",	// Cache time (sec.)
                "CACHE_FILTER" => "N",	// Cache if the filter is active
                "CACHE_GROUPS" => "Y",	// Respect Access Permissions
                "DISPLAY_TOP_PAGER" => "N",	// Display at the top of the list
                "DISPLAY_BOTTOM_PAGER" => "Y",	// Display at the bottom of the list
                "PAGER_TITLE" => "�������",	// Category name
                "PAGER_SHOW_ALWAYS" => "Y",	// Always show the pager
                "PAGER_TEMPLATE" => "",	// Name of the pager template
                "PAGER_DESC_NUMBERING" => "N",	// Use reverse page navigation
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Cache time for pages with reverse page navigation
                "PAGER_SHOW_ALL" => "Y",	// Show the ALL link
                "AJAX_OPTION_SHADOW" => "Y",	// Enable shadowing
                "AJAX_OPTION_JUMP" => "N",	// Enable scrolling to component's top
                "AJAX_OPTION_STYLE" => "Y",	// Enable styles loading
                "AJAX_OPTION_HISTORY" => "N",	// Emulate browser navigation
                ),
                false
            );?>         
			</div>
            </div>
            <div class="content_area">
            	<div class="content">
