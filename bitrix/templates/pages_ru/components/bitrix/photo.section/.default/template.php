<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="photo-section">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<table cellpadding="0" cellspacing="0" border="0" class="data-table">
	<?foreach($arResult["ROWS"] as $arItems):?>
		<tr class="head-row" valign="top">
		<?foreach($arItems as $arItem):?>
			<?if(is_array($arItem)):?>
				<td  width="<?=$arResult["TD_WIDTH"]?>">
					<a href="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" title="Luxury Travel Mart" rel="lightbox[roadtrip]"><img border="0" src="<?=$arItem["PICTURE"]["SRC"]?>" width="<?=$arItem["PICTURE"]["WIDTH"]?>" height="<?=$arItem["PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a></td>
			<?else:?>
				<td width="<?=$arResult["TD_WIDTH"]?>" rowspan="<?=$arResult["nRowsPerItem"]?>">&nbsp;</td>
			<?endif;?>
		<?endforeach?>
		</tr>
	<?endforeach?>
</table>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
<?
	//print_r($arResult);
?>