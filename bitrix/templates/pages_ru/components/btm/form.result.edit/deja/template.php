<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<p><strong><?=$arResult["FORM_DESCRIPTION"]?></strong></p>
<table width="100%" cellspacing="0" cellpadding="5" border="0" class="form_cont"> 
  <tbody> 
    <tr> <td width="150">First name</td> <td><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_605"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td>Last name</td> <td><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_151"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td>Title</td> <td><?=$arResult["QUESTIONS"]["select_choose"]["HTML_CODE"]?> 
        <div id="select_choose_ans"> <?=$arResult["QUESTIONS"]["select_choose_ans"]["HTML_CODE"]?> </div>
       </td> </tr>
   
    <tr> <td>Job Title</td> <td><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_675"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td>Company or Hotel</td> <td><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_961"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td>Area of business</td> <td><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_716"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td>City</td> <td><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_653"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td>Company/Hotel full address</td> <td><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_700"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td>Country</td> <td><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_876"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td valign="top"> Email</td> <td valign="top"><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_579"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td valign="top">Please confirm your email </td> <td valign="top"><input type="text" size="0" value="" name="form_email_conf" /></td> </tr>
   
    <tr> <td valign="top">Alternative email</td> <td valign="top"><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_662"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td valign="top">Telephone number</td> <td valign="top"><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_250"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td valign="top">Company's web-site</td> <td valign="top"><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_973"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td valign="top" colspan="2">Short company description</td> </tr>
   
    <tr> <td valign="top">(which will appear on the web-site in registered exhibitors section, maximum 800 symbols)</td> <td valign="top"><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_182"]["HTML_CODE"]?></td> </tr>
   </tbody>
 </table>
<br />
 
<br />
 
<p><strong>You may register one additional colleague from your company for the LTM 2011</strong></p>
 
<table width="100%" cellspacing="0" cellpadding="5" border="0" class="form_cont"> 
  <tbody> 
    <tr> <td width="150" valign="top">Title </td> <td valign="top"><?=$arResult["QUESTIONS"]["select_choose1"]["HTML_CODE"]?> 
        <div id="select_choose1_ans"> <?=$arResult["QUESTIONS"]["select_choose1_ans"]["HTML_CODE"]?> </div>
       </td> </tr>
   
    <tr> <td valign="top">First Name</td> <td valign="top"><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_605_zway4"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td valign="top">Last Name</td> <td valign="top"><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_151_far0b"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td valign="top">Job Title</td> <td valign="top"><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_675_Zm0wR"]["HTML_CODE"]?></td> </tr>
   
    <tr> <td valign="top">Email</td> <td valign="top"><?=$arResult["QUESTIONS"]["SIMPLE_QUESTION_579_7Bk0B"]["HTML_CODE"]?></td> </tr>
   
<?
if($arResult["isUseCaptcha"] == "Y")
{
?>
		<tr> 
			<td>&nbsp;</td>
			<td><input type="hidden" name="captcha_sid" value="<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialchars($arResult["CAPTCHACode"]);?>" width="180" height="40" /></td>
		</tr>
		<tr> 
			<td><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?></td>
			<td><input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" /></td>
		</tr>
<?
} // isUseCaptcha
?>
	</tbody>
 </table>
<input name="send_first" type="hidden" value="send" />
<p class="send_but">
			<input type="hidden" name="web_form_apply" value="Y" /><input type="submit" name="web_form_apply" value="Send" />
			&nbsp;<input type="reset" value="<?=GetMessage("FORM_RESET");?>" />
</p>
