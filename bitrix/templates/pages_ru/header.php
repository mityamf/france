<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?$APPLICATION->ShowHead()?>
<title><?$APPLICATION->ShowTitle()?></title>
<script type="text/javascript" src="/bitrix/js/btm/change_totle.js"></script>
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="/bitrix/js/btm/prototype.js"></script>
<script type="text/javascript" src="/bitrix/js/btm/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="/bitrix/js/btm/jquery.maxlength.js"></script>
<script type="text/javascript" src="/bitrix/js/jquery.js"></script>
<script type="text/javascript" src="/bitrix/js/ltm_form/form.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	    $("div.step1 #step1_send input").bind('click', validate);
	    $("div.step2 #step2_send input").bind('click', validate_step2);
	    $("div.step3 #step3_send input").bind('click', validate_step3);
	    $("div.step4 #step4_send input").bind('click', validate_step4);
		$("input[name='form_text_156']").bind('focus', change_text);
		$("input[name='form_text_157']").bind('focus', change_text);
		$("input[name='form_text_156']").bind('focusout', back_text);
		$("input[name='form_text_157']").bind('focusout', back_text);
	});
</script>
<script type="text/javascript" src="/bitrix/js/main/ajax.js"></script>
</head>
<body onload='accordionAddBehivior();'>
<?include($_SERVER['DOCUMENT_ROOT']."/bitrix/php_interface/admin_header.php")?>
<?$APPLICATION->ShowPanel();?>
<div class="marg">
    <div class="container">
    	<div class="top">
            <div class="logo"><a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.svg" alt="Logo" /></a></div>
            <div class="title">
                ������, ������<br />
                <span class="date">3 ������� 2018�.</span>
            </div>
        </div>
        <div class="content">
          <div class="left_col">
          <!--[if IE]>
              <div class="shadow"></div>
          <![endif]-->
              <div class="inner">
              <?$APPLICATION->IncludeComponent("bitrix:menu", "template", Array(
                  "ROOT_MENU_TYPE" => "left",	// ��� ���� ��� ������� ������
                  "MAX_LEVEL" => "1",	// ������� ����������� ����
                  "CHILD_MENU_TYPE" => "left",	// ��� ���� ��� ��������� �������
                  "USE_EXT" => "N",	// ���������� ����� � ������� ���� .���_����.menu_ext.php
                  "ALLOW_MULTI_SELECT" => "N",	// ��������� ��������� �������� ������� ������������
                  "MENU_CACHE_TYPE" => "N",	// ��� �����������
                  "MENU_CACHE_TIME" => "3600",	// ����� ����������� (���.)
                  "MENU_CACHE_USE_GROUPS" => "Y",	// ��������� ����� �������
                  "MENU_CACHE_GET_VARS" => "",	// �������� ���������� �������
                  ),
                  false
              );?>
              </div>
          </div>
          <div class="right_col">
          	<div class="content_part">