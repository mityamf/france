<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<?$APPLICATION->ShowHead()?>
<title><?$APPLICATION->ShowTitle()?></title>
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<script type="text/javascript" src="/bitrix/js/jquery.js"></script>
</head>
<body>
<div class="marg">
	<div class="marg_area">
        <div class="top">
            <div class="logo"><a href="/ru"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.svg" alt="Logo" /></a></div>
            <div class="title">
               ������, ������<br />
                <span class="date">3 ������� 2018�.</span>
            </div>
        </div>
        <div class="hello_str">
        	<div class="welcome">
                <?$APPLICATION->IncludeComponent(
                    "btm:welcome.user",
                    "guest",
                    Array(
                        "ADMIN" => "1",
                        "GROUP_ID" => "6",
                        "AUTH_PAGE" => "/ru/personal/login.php"
                    )
                );?>            
            </div>
            <div class="logout"><a href="/ru/personal/login.php?action=logout"><img src="<?=SITE_TEMPLATE_PATH?>/images/logout.gif" width="63" height="21" alt="Logout" border="0" /></a></div>
        </div>
        <div class="content_marg">
        	<div class="left_col">
                <?$APPLICATION->IncludeComponent(
                    "btm:menu.user",
                    "",
                    Array(
                        "ADMIN" => "1",
                        "AUTH_PAGE" => "/ru/personal/login.php",
                        "PATH_TO_KAB" => "/ru/personal/",
                        "GROUP_ID" => "6",
                        "TYPE" => "GUEST"
                    ),
                false
                );?>
                <div class="content_block">
	                <div class="main_content">
