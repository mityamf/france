<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Bitrix Site Manager Demo version");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Home");
?>

<p>The <b>Bitrix Site Manager</b> is the technological base for building and managing web sites. It offers efficient way to develop, support and maintain your web projects.</p>
<p>The <b>Bitrix Site Manager</b> can be used by both corporate customers and individual web developers as the base to develop new sites from scratch and manage the existing sites.</p>
<p>The <b>Bitrix Site Manager</b> allows to build as many sites as you need, using a single copy (license) of the product. This means that you have to keep a single copy of the system and database on
the server.</p>

<p>What is more, with the <b>Bitrix Site Manager</b> you can:</p>

<ul>
	<li>

manage the structure and content of your site;</li>
	<li>publish news, press releases and other of frequently updated information;</li>
	<li>manage advertising shows on your site;</li>

	<li>create forums;</li>
	<li>create mailing lists and send messages to your subscribers;</li>
	<li>keep visitor statistics;</li>
	<li>control advertising campaigns;</li>
	<li>perform many other operations in managing your web project.</li>
</ul>

<p>The product will minimize web site maintenance costs owing to simple process of management of static and dynamic information. You will not need any web design or programming expert service in
maintaining and managing your site. A web site built with the <b>Bitrix Site Manager</b> can be managed by an office employee, i.e. a common PC user without any programming, HTML layout or web design skills.</p>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>