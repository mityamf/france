<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="information-block">
	<div class="information-block-head">Auto-Cache mode</div>
	Please note that the data cache is <b>ON</b> by default to boost demo site performance. You can change cache settings <a href="/bitrix/admin/cache.php">here</a>.
</div>

<!-- SOCNETWORK_GROUPS -->

<!--VOTE_FORM-->

<!--PHOTO_RANDOM-->
