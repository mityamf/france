<?xml version="1.0" encoding="windows-1251"?>
<content type="LES"><id>393</id>
<timestamp_x>1162482875</timestamp_x>
<date_create>1153755984</date_create>
<created_by>1837</created_by>
<active>Y</active>
<course_id>10</course_id>
<chapter_id>147</chapter_id>
<name>Controlling advertising shows using keywords</name>
<sort>300</sort>
<preview_picture></preview_picture>
<preview_text></preview_text>
<preview_text_type>text</preview_text_type>
<detail_picture></detail_picture>
<detail_text><![CDATA[
<p>One of the methods that can be applied to control the banner shows and to target the advertising precisely is using<strong> keywords</strong>. The distinctive advantage of this method is that it allows to drive the advertising campaign aimed to reach the well defined target group among your visitors.</p>

<p>Using the <strong>Advertising</strong> and <strong>Site Explorer</strong> modules of the Bitrix Site Manager, you can:</p>

<ul>
  <li><strong>target advertisement to a specific user group</strong>. This means that, in respect of a user group, you can show advertisement on the most frequently viewed pages, or on pages whose content can be of particular interest to this user group; </li>

  <li><strong>confine banner impressions within the defined rules</strong>, for example, depending on the level of correlation between the advertisement subject and the information on a page. </li>
</ul>

<p>To control advertising show with use of the keywords mechanism&nbsp;you can adjust the following types of keywords:</p>

<ul>
  <li>banner keywords; </li>

  <li>page keywords: 
    <ul>
      <li><strong>desired</strong>: if a site page is assigned the desired keywords, all the banners that have <em>at least one matching keyword</em> in their keyword sets can be shown on that page. 
        <p>If no banners with the matching keywords can be found, then the page will show banners that are not assigned any keywords. In this situation, the system uses own standard algorithm to select banners to be displayed.</p>
      </li>
    
      <li><strong>required</strong>: if a site page is assigned the required keywords, all the banners that have <em>all keywords</em> in their keyword sets can be shown on that page. 
        <p>If no such banners can be found, then the page will show banners that are not assigned any keywords. In this situation, the system uses own standard algorithm to select banners to be displayed.</p>
      </li>
    </ul>
  </li>
</ul>

<p>You can assign keywords to a banner in the <strong>Keywords</strong> field (the <strong>Targeting </strong>tab) on the banner editing page:</p>

<p><em>Services -&gt; Advertising -&gt; Banners</em> (go to a banner editing or creation)<em> </em></p>

<p><img src="cid:resources/res94/RW2zD.L9rvv.banner_keywords.png" height="271" width="476"/></p>

<p>A special property named <code>adv_desired_target_keywords</code> is used to manage the desired keywords. This property is predefined, i.e. its name and behavoiur are hard-coded in the system.</p>

<p><img src="cid:resources/res94/lfvY9.zMNDU.page_prop.png" height="206" width="490"/></p>

<p>You can specify the desired page keywords by calling the method <code>SetDesiredKeywords()</code>. This method accepts the following parameters:</p>

<ul>
  <li><code>keywords</code> &ndash; array of one or more desired keywords; </li>

  <li><code>TIP_SID</code> &ndash; the symbolic identifier of the advertisement type. Describes the advertisement type to which the keywords are to be assigned. Leave this parameter empty to assign keywords to all types. </li>
</ul>

<p></p>

<pre class="syntax">CAdvBanner::SetDesiredKeywords(array(&quot;Partners&quot;,
                                     &quot;Cooperation&quot;,
                                     &quot;Company&quot;,
                                     &quot;Contacts&quot;),
                               &quot;RIGHT&quot;);</pre>

<p></p>

<p></p>

<div class="important"><b>Note!</b> If the were not set any keywords in a page source code then the function <code>SetDesiredKeywords()</code> parameter <code>keywords</code> assumes a value of page property <code>adv_desired_target_keywords</code>. If value of this property was not assigned then the function <code>SetDesiredKeywords()</code> uses value of the page property <code>keywords</code>.</div>

<p></p>

<p>The function <code>SetDesiredKeywords()</code> is called automatically at the moment of page generation. That is why it is not required to call this function additionally in the <code>header.php</code> if you do not want to redefine page desired keywords. </p>

<p>The <code>required keywords</code> impose active constraints on the banner shows. If the banner keywords does not contain all of the page keywords, the banner will not be shown on the page.</p>

<p>Developers have the opportunity to assign the required keywords to a page by calling the method <code>SetRequiredKeywords()</code>. This method accepts the following parameters:</p>

<ul>
  <li><code>keywords </code>&ndash; array of one or more required keywords; </li>

  <li><code>TIP_SID</code> &ndash; the symbolic identifier of the advertisement type. Describes the advertisement type to which the keywords are to be assigned. Leave this parameter empty to assign keywords to all types. </li>
</ul>

<p>The following call assigns the following two required keywords to a page: partners and cooperation.</p>

<p></p>

<pre class="syntax">CAdvBanner::SetRequiredKeywords(array(&quot;Partners&quot;,
                                      &quot;Cooperation&quot;),
                                &quot;RIGHT&quot;);</pre>

<p></p>

<p>In this case, only a banner the set of keywords of which contains all required keywords assigned by the call of function <code>SetRequiredKeywords</code> can be shown on the page. </p>
]]></detail_text>
<detail_text_type>html</detail_text_type>
</content>