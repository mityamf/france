<?
$MESS["SEC_OTP_PARAMETERS_TAB"] = "Parameter";
$MESS["SEC_OTP_WINDOW_SIZE"] = "Gr��e des Fensters f�r die Passwort�berpr�fung";
$MESS["SEC_OTP_DEFAULT_YPE"] = "Algorithmus der standardm��igen Passwortgenerierung";
$MESS["SEC_OTP_ALLOW_REMEMBER"] = "Persistente OTP's erlauben";
$MESS["SEC_OTP_MANDATORY_SKIP_DAYS"] = "Tage verblieben f�r Verbindung";
$MESS["SEC_OTP_MANDATORY_RIGHTS_SELECT"] = "Nutzer hinzuf�gen";
$MESS["SEC_OTP_NEW_TITLE"] = "Zwei-Faktor-Authentifizierung";
$MESS["SEC_OTP_NEW_MAIN_TAB"] = "Zwei-Faktor-Authentifizierung";
$MESS["SEC_OTP_NEW_MAIN_TAB_TITLE"] = "Zwei-Faktor-Authentifizierung aktivieren.";
$MESS["SEC_OTP_NEW_ON"] = "Zwei-Faktor-Authentifizierung wurde aktiviert";
$MESS["SEC_OTP_NEW_OFF"] = "Zwei-Faktor-Authentifizierung wurde deaktiviert";
$MESS["SEC_OTP_NEW_BUTTON_OFF"] = "Zwei-Faktor-Authentifizierung deaktivieren";
$MESS["SEC_OTP_NEW_BUTTON_ON"] = "Zwei-Faktor-Authentifizierung aktivieren";
$MESS["SEC_OTP_NEW_PARAMETERS_TAB_TITLE"] = "Zwei-Faktor-Authentifizierung konfigurieren.";
$MESS["SEC_OTP_ALLOW_RECOVERY_CODES"] = "Reserve-Codes erlauben";
$MESS["SEC_OTP_NEW_MANDATORY_HEADER"] = "Zwei-Faktor-Authentifizierung erforderlich machen";
$MESS["SEC_OTP_NEW_MANDATORY_USING"] = "Zwei-Faktor-Authentifizierung immer verlangen";
$MESS["SEC_OTP_NEW_MANDATORY_RIGHTS"] = "Nutzer, welche die Zwei-Faktor-Authentifizierung immer nutzen m�ssen";
?>