<?
$MESS["SEC_OTP_RECOVERY_TITLE"] = "Reserve-Codes f�r Verifizierung";
$MESS["SEC_OTP_RECOVERY_NOTE"] = "Ein Code kann nur einmal benutzt werden. Hinweis: Bereits benutzte Codes k�nnen von der Liste gestrichen werden.";
$MESS["SEC_OTP_RECOVERY_CREATED"] = "Erstellt am: #DATE#";
$MESS["SEC_OTP_RECOVERY_ISSUER"] = "Ausgestellt von: #ISSUER#";
$MESS["SEC_OTP_RECOVERY_LOGIN"] = "Login: #LOGIN#";
?>