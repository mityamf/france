<?
$MESS["SEC_OTP_INIT"] = "Initialisierung";
$MESS["SEC_OTP_SECRET_KEY"] = "Geheimes Kennwort (wird mit dem Ger�t mitgeliefert)";
$MESS["SEC_OTP_PASS1"] = "Der erste Wert des Passworts (dr�cken Sie die Taste auf dem Ger�t)";
$MESS["SEC_OTP_PASS2"] = "Weiterer Wert des Passworts (dr�cken Sie erneut die Taste auf dem Ger�t)";
$MESS["SEC_OTP_TYPE"] = "Algorithmus der Passwortgenerierung";
$MESS["SEC_OTP_STATUS"] = "Aktueller Status";
$MESS["SEC_OTP_STATUS_ON"] = "Aktiviert";
$MESS["SEC_OTP_NEW_ACCESS_DENIED"] = "Zugriff auf die Verwaltung von Zwei-Faktor-Authentifizierung wurde verweigert.";
$MESS["SEC_OTP_NEW_SWITCH_ON"] = "Zwei-Faktor-Authentifizierung aktivieren";
$MESS["SEC_OTP_DESCRIPTION_INTRO_TITLE"] = "Einmalpasswort";
$MESS["SEC_OTP_DESCRIPTION_INTRO_SITE"] = "Derzeit  verwendet ein Nutzer einen Login und ein Passwort, um sich auf Ihrer Website anzumelden. Allerding gibt es diverse Schadprogramme, mit denen Kriminelle auf einen Computer eindringen und dort Daten stehlen k�nnen, wenn bspw. ein Nutzer sein Passwort speichert.<br>
<b>Zwei-Faktor-Authentifizierung</b> ist eine empfohlene Option, um sich gegen Hacker-Software zu sch�tzen. Jedes Mal, wenn Nutzer sich im System einloggen, m�ssen sie zwei Verifizierungsschritte machen. Zuerst m�ssen sie Login und Passwort eingeben. Dann geben sie einen einmaligen Sicherheitscode ein, den sie auf ihre mobilen Ger�te gesendet bekommen. Selbst wenn Login und Passwort gestohlen werden, k�nnen Kriminelle diese Daten nicht missbrauchen, weil sie den Sicherheitscode nicht wissen.
";
$MESS["SEC_OTP_DESCRIPTION_INTRO_INTRANET"] = "Derzeit  verwendet ein Nutzer einen Login und ein Passwort, um sich auf Ihrer Website anzumelden. Allerding gibt es diverse Schadprogramme, mit denen Kriminelle auf einen Computer eindringen und dort Daten stehlen k�nnen, wenn bspw. ein Nutzer sein Passwort speichert.<br>
<b>Zwei-Faktor-Authentifizierung</b> ist eine empfohlene Option, um sich gegen Hacker-Software zu sch�tzen. Jedes Mal, wenn Nutzer sich im System einloggen, m�ssen sie zwei Verifizierungsschritte machen. Zuerst m�ssen sie Login und Passwort eingeben. Dann geben sie einen einmaligen Sicherheitscode ein, den sie auf ihre mobilen Ger�te gesendet bekommen. Selbst wenn Login und Passwort gestohlen werden, k�nnen Kriminelle diese Daten nicht missbrauchen, weil sie den Sicherheitscode nicht wissen.
";
$MESS["SEC_OTP_DESCRIPTION_USING_TITLE"] = "Wie benutzt man Einmalpassw�rter";
$MESS["SEC_OTP_DESCRIPTION_USING_STEP_0"] = "Schritt 1";
$MESS["SEC_OTP_DESCRIPTION_USING_STEP_1"] = "Schritt 2";
$MESS["SEC_OTP_DESCRIPTION_USING"] = "Wenn Zwei-Faktor-Authentifizierung aktiviert ist, m�ssen Nutzer zwei Schritte der Verifizierung beim Einloggen machen. <br>
Zuerst geben sie wie immer ihre E-Mail und das Passwort ein. <br>
Dann geben sie einen einmaligen Sicherheitscode ein, den sie auf ihre mobile Ger�te zugesendet oder mithilfe eines speziellen Dongles bekommen.
";
$MESS["SEC_OTP_DESCRIPTION_ACTIVATION_TITLE"] = "Aktivierung";
$MESS["SEC_OTP_DESCRIPTION_ACTIVATION"] = "Ein Enmalcode f�r Zwei-Faktor-Authentifizierung kann mit einem speziellen Ger�t (Dongle) oder mit einer kostenlosen Anwendung (Bitrix OTP) bekommen werden, welche jeder Nutzer auf seinem mobilen Ger�t installiert haben soll.<br>
Um einen Dongle zu aktivieren, muss der Administrator das Nutzerprofil �ffnen und dort zwei Passw�rter, generiert durch Dongle, eins nach dem anderen eingeben.<br>
Um einen Einmalcode auf einem mobilen Ger�t erhalten zu k�nnen, m�ssen Nutzer die App herunterladen und starten, sodass sie den QR-Code auf der Seite der Einstellungen in ihrem Nutzerprofil einscannen oder Account-Daten manuell eingeben k�nnen.
";
$MESS["SEC_OTP_DESCRIPTION_ABOUT_TITLE"] = "Beschreibung";
$MESS["SEC_OTP_DESCRIPTION_ABOUT"] = "Einmalpasswort (OTP) wurde als Teil der OATH-Initiative entwickelt.<br>
OTP basiert auf HMAC und SHA-1/SHA-256/SHA-512. Zurzeit werden zwei Algorithmen der Codegenerierung unterst�tzt:
<ul><li>Z�hler-basiert (HMAC-basiertes Einmalpasswort, HOTP) ist beschrieben in <a href=\"https://tools.ietf.org/html/rfc4226\" target=\"_blank\">RFC4226</a></li>
<li>Zeit-basiert (Zeit-basiertes Einmalpasswort, TOTP) ist beschrieben in <a href=\"https://tools.ietf.org/html/rfc6238\" target=\"_blank\">RFC6238</a></li></ul>
Um den OTP-Wert zu kalkulieren, nimmt der Algorithmus zwei Eingabeparameter: einen Geheimschl�ssel (Ursprungswert) und einen aktuellen Z�hlerwert (die Anzahl erforderlicher Zyklen der Generierung oder die aktuelle Zeit, abh�ngig vom Algorithmus). Der Ursprungswert wird auf dem Ger�t sowie auf der Website gespeichert, sobald das Ger�t initialisiert wurde. Wenn HOTP benutzt wird, wird Z�hler im Ger�t bei jeder OTP-Generierung erh�ht, w�hrend der Server-Z�hler bei jeder erfolgreichen OTP-Authentifizierung ge�ndert wird. Bei Nutzung von TOTP werden keine Z�hler im Ger�t gespeichert, der Server kontrolliert alle m�glichen Zeit�nderungen am Ger�t bei jeder erfolgreichen OTP-Authentifizierung.<br>
Jedes OTP-Ger�t in der Partie enth�lt eine verschl�sselte Datei mit Ursprungswerten (Geheimschl�ssel), die Datei ist verbunden mit der Seriennummer des jeweiligen Ger�ts, welche auf dem Ger�t selbst zu finden ist.<br>
Wird die Synchronisierung der Ger�t- und Server-Z�hler gest�rt, kann die Synchronisierung wiederhergestellt werden, indem die Serverwerte denen auf dem Ger�t gleichgesetzt werden. Daf�r muss der Administrator (oder ein Nutzer mit entsprechenden Zugriffsrechten) zwei nacheinander folgende OTPs generieren und diese auf der Website eingeben.<br>
Sie k�nnen die mobile App in AppStore und GooglePlay finden.
";
$MESS["SEC_OTP_CONNECT_MOBILE_TITLE"] = "Mobiles Ger�t anbinden";
$MESS["SEC_OTP_CONNECT_MOBILE_STEP_1"] = "Downloaden Sie die mobile App Bitrix OTP f�r Ihr Telefon auf <a href=\" https://itunes.apple.com/de/app/bitrix24-otp/id929604673?l=de \" target=\"_new\">AppStore</a> oder auf <a href=\"https://play.google.com/store/apps/details?id=com.bitrixsoft.otp\" target=\"_new\">GooglePlay</a>";
$MESS["SEC_OTP_CONNECT_MOBILE_STEP_2"] = "Starten Sie die Anwendung und klicken Sie auf <b>Konfigurieren</b>";
$MESS["SEC_OTP_CONNECT_MOBILE_STEP_3"] = "W�hlen Sie, wie Sie die Daten eingeben m�chten: mit dem QR-Code oder manuell";
$MESS["SEC_OTP_CONNECT_MOBILE_SCAN_QR"] = "Halten Sie Ihr mobiles Ger�t vor dem Bildschirm und warten Sie, bis die Anwendung den Code eingescannt hat.";
$MESS["SEC_OTP_CONNECT_MOBILE_MANUAL_INPUT"] = "Um Daten manuell einzugeben, m�ssen Sie die Website-Adresse, Ihre E-Mail oder Login, den Geheimcode auf dem Bild angeben und dann den Schl�sseltyp ausw�hlen.";
$MESS["SEC_OTP_CONNECT_MOBILE_MANUAL_INPUT_HOTP"] = "Z�hler-basiert";
$MESS["SEC_OTP_CONNECT_MOBILE_MANUAL_INPUT_TOTP"] = "Zeit-basiert";
$MESS["SEC_OTP_CONNECT_MOBILE_INPUT_DESCRIPTION"] = "Nachdem der Code erfolgreich eingescannt oder manuell eingegeben wurde, zeigt Ihr mobiles Ger�t einen Code an, den Sie unten eingeben m�ssen.";
$MESS["SEC_OTP_CONNECT_MOBILE_ENTER_CODE"] = "Code eingeben";
$MESS["SEC_OTP_CONNECT_MOBILE_INPUT_NEXT_DESCRIPTION"] = "Der OTP-Algorithmus verlangt zwei Codes f�r Authentifizierung. Generieren Sie bitte den n�chsten Code und geben Sie ihn unten ein.";
$MESS["SEC_OTP_CONNECT_MOBILE_ENTER_NEXT_CODE"] = "N�chsten Code eingeben";
$MESS["SEC_OTP_CONNECT_DONE"] = "Fertig";
$MESS["SEC_OTP_CONNECT_DEVICE_TITLE"] = "Dongle anbinden";
$MESS["SEC_OTP_CONNECTED"] = "Angebunden";
$MESS["SEC_OTP_ENABLE"] = "Aktivieren";
$MESS["SEC_OTP_DISABLE"] = "Deaktivieren";
$MESS["SEC_OTP_SYNC_NOW"] = "Synchronisieren";
$MESS["SEC_OTP_MOBILE_INPUT_METHODS_SEPARATOR"] = "oder";
$MESS["SEC_OTP_MOBILE_SCAN_QR"] = "QR-Code einscannen";
$MESS["SEC_OTP_MOBILE_MANUAL_INPUT"] = "Code manuell eingeben";
$MESS["SEC_OTP_CONNECT_DEVICE"] = "Dongle anbinden";
$MESS["SEC_OTP_CONNECT_MOBILE"] = "Mobiles Ger�t anbinden";
$MESS["SEC_OTP_CONNECT_NEW_DEVICE"] = "Neuen Dongle anbinden";
$MESS["SEC_OTP_CONNECT_NEW_MOBILE"] = "Neues mobiles Ger�t anbinden";
$MESS["SEC_OTP_ERROR_TITLE"] = "Fehler beim Speichern.";
$MESS["SEC_OTP_UNKNOWN_ERROR"] = "Unerwarteter Fehler. Versuchen Sie bitte sp�ter erneut.";
$MESS["SEC_OTP_RECOVERY_CODES_BUTTON"] = "Reserve-Codes";
$MESS["SEC_OTP_RECOVERY_CODES_TITLE"] = "Reserve-Codes";
$MESS["SEC_OTP_RECOVERY_CODES_DESCRIPTION"] = "Kopieren Sie die Reserve-Codes, die Sie ben�tigen k�nnen, wenn Sie Ihr mobiles Ger�t verloren haben oder einen Code via App aus irgendeinem Grund nicht erhalten k�nnen.";
$MESS["SEC_OTP_RECOVERY_CODES_WARNING"] = "Bewahren Sie diese sorgf�ltig auf, bspw. in Ihrer Brieftasche. Jeder Code kann nur einmal benutzt werden.";
$MESS["SEC_OTP_RECOVERY_CODES_PRINT"] = "Drucken";
$MESS["SEC_OTP_RECOVERY_CODES_SAVE_FILE"] = "Als Text-Datei speichern";
$MESS["SEC_OTP_RECOVERY_CODES_REGENERATE_DESCRIPTION"] = "Werden die Reserve-Codes knapp?<br/>
Erstellen Sie neue. <br/><br/>
Wenn Sie neue Codes erstellen, <br/>machen Sie dadurch die vorher erstellten ung�ltig.
";
$MESS["SEC_OTP_RECOVERY_CODES_REGENERATE"] = "Neue Codes generieren";
$MESS["SEC_OTP_RECOVERY_CODES_NOTE"] = "Ein Code kann nur einmal benutzt werden. Hinweis: Bereits benutzte Codes k�nnen von der Liste gestrichen werden.";
$MESS["SEC_OTP_WARNING_RECOVERY_CODES"] = "Zwei-Faktor Authentifizierung wurde aktiviert, aber Sie haben keine Reserve-Codes erstellt. Diese k�nnen aber erforderlich sein, wenn Sie Ihr mobiles Ger�t verlieren oder den Code via App aus irgendeinem Grund nicht erhalten k�nnen.";
$MESS["SEC_OTP_NO_DAYS"] = "F�r immer";
$MESS["SEC_OTP_DEACTIVATE_UNTIL"] = "Deaktiviert bis #DATE#";
$MESS["SEC_OTP_MANDATORY_EXPIRED"] = "Die Frist f�r die Einstellung der Zwei-Faktor-Authentifizierung durch den Nutzer ist jetzt abgelaufen.";
$MESS["SEC_OTP_MANDATORY_ALMOST_EXPIRED"] = "Die Frist f�r die Einstellung der Zwei-Faktor-Authentifizierung durch den Nutzer endet am #DATE#.";
$MESS["SEC_OTP_MANDATORY_DISABLED"] = "Erforderliche Zwei-Faktor-Authentifizierung ist deaktiviert.";
$MESS["SEC_OTP_MANDATORY_ENABLE_DEFAULT"] = "Aktivierung der Zwei-Faktor-Authentifizierung erfordern";
$MESS["SEC_OTP_MANDATORY_ENABLE"] = "Aktivierung der Zwei-Faktor-Authentifizierung erfordern in";
$MESS["SEC_OTP_MANDATORY_DEFFER"] = "Verl�ngern";
?>