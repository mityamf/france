<?
$MESS["SECURITY_SITE_CHECKER_FilePermissionsTest_NAME"] = "�berpr�fung der Dateizug�nglichkeit.";
$MESS["SECURITY_SITE_CHECKER_FILE_PERM_ADDITIONAL"] = "Letzte #COUNT# Dateien/Verzeichnisse:";
$MESS["SECURITY_SITE_CHECKER_FILE_PERM_TITLE"] = "Es gibt mindestens #COUNT# Dateien oder Verzeichnisse mit dem vollen Zugriffsrecht f�r alle in der aktuellen Umgebung (fremde Nutzer von Bitrix Framework).";
$MESS["SECURITY_SITE_CHECKER_FILE_PERM_DETAIL"] = "Der volle Zugriff f�r alle Systemnutzer kann Ihr Projekt komplett gef�hrden, weil Ihr Code durch Dritte ge�ndert werden kann.";
$MESS["SECURITY_SITE_CHECKER_FILE_PERM_RECOMMENDATION"] = "�berfl�ssige Zugriffsrechte l�schen.";
$MESS["SECURITY_SITE_CHECKER_FILE_PERM_TIMEOUT"] = "Entschuldigung, die System-Performance ist zu niedrig.";
$MESS["SECURITY_SITE_CHECKER_FILE_PERM_SMALL_MAX_EXEC"] = "Die Ausf�hrungszeit der PHP-Script (max_execution_time) ist zu kurz. Der empfohlene Wert sind 30 Sekunden oder noch mehr.";
?>