<?
$MESS["SECURITY_SITE_CHECKER_OTP_NOT_USED"] = "2-Stufen-�berpr�fung ist deaktiviert.";
$MESS["SECURITY_SITE_CHECKER_OTP_NOT_USED_DETAIL"] = "2-Stufen-�berpr�fung ist dazu gedacht, den Schutz Ihrer Website gegen Fishing wesentlich zu verbessern.";
$MESS["SECURITY_SITE_CHECKER_OTP_NOT_USED_RECOMMENDATION"] = "Hier aktivieren: <a href=\"/bitrix/admin/security_otp.php\" target=\"_blank\">Einmalpassw�rter</a>";
$MESS["SECURITY_SITE_CHECKER_ADMIN_OTP_NOT_USED"] = "Nicht alle Administratoren benutzen OTP";
$MESS["SECURITY_SITE_CHECKER_ADMIN_OTP_NOT_USED_DETAIL"] = "2-Stufen-�berpr�fung ist dazu gedacht, den Schutz Ihrer Website gegen Fishing wesentlich zu verbessern. Aktivieren Sie das immer f�r die Nutzer mit Administrator-Rechten.";
$MESS["SECURITY_SITE_CHECKER_ADMIN_OTP_NOT_USED_RECOMMENDATION"] = "Administratoren dar�ber benachrichtigen, dass Sie die Nutzung von OTP's empfehlen.";
$MESS["SECURITY_SITE_CHECKER_UsersTest_NAME"] = "Nutzer�berpr�fung";
$MESS["SECURITY_SITE_CHECKER_ADMIN_WEAK_PASSWORD"] = "Einige Nutzer mit administrativen Zugriffsrechten haben ein schwaches Passwort.";
$MESS["SECURITY_SITE_CHECKER_ADMIN_WEAK_PASSWORD_DETAIL"] = "Privilegierte Nutzer m�ssen ein sicheres Passwort nutzen, welches aus Buchstaben in Gro�- und Kleinschreibung sowie aus Zahlen und Satzzeichen besteht.";
$MESS["SECURITY_SITE_CHECKER_ADMIN_WEAK_PASSWORD_USER_LIST"] = "Nutzer:";
$MESS["SECURITY_SITE_CHECKER_ADMIN_WEAK_PASSWORD_RECOMMENDATIONS"] = "Nutzer m�ssen ein st�rkeres Passwort verwenden";
?>