<?
$MESS["SECURITY_SITE_CHECKER_SiteConfigurationTest_NAME"] = "Test der Website-Konfiguration";
$MESS["SECURITY_SITE_CHECKER_WAF_OFF"] = "Proaktiver Filter ist deaktiviert.";
$MESS["SECURITY_SITE_CHECKER_ADMIN_SECURITY_LEVEL"] = "Die Sicherheitsstufe der Nutzergruppe Administratoren ist nicht Hoch.";
$MESS["SECURITY_SITE_CHECKER_ADMIN_SECURITY_LEVEL_DETAIL"] = "Eine nicht ausreichende Sicherheitsstufe kann potenziell von einem Angreifer genutzt werden.";
$MESS["SECURITY_SITE_CHECKER_ERROR_REPORTING"] = "Der Warnungslevel muss auf \"nur Fehler\" oder \"keiner\" gesetzt werden.";
$MESS["SECURITY_SITE_CHECKER_ERROR_REPORTING_DETAIL"] = "PHP-Warnungen k�nnen den vollst�ndigen physischen Pfad zu Ihrem Web-Projekt anzeigen.";
$MESS["SECURITY_SITE_CHECKER_DB_DEBUG"] = "Fehlerbehebung f�r SQL-Abfragen ist aktiviert (\$DBDebug hat den Wert true)";
$MESS["SECURITY_SITE_CHECKER_DB_EMPTY_PASS"] = "Das Datenbank-Passwort ist leer.";
$MESS["SECURITY_SITE_CHECKER_DB_EMPTY_PASS_RECOMMENDATION"] = "Passwort definieren";
$MESS["SECURITY_SITE_CHECKER_DB_SAME_REGISTER_PASS"] = "Die Zeichen des Datenbank-Passwortes m�ssen nur klein oder nur gro� geschrieben werden.";
$MESS["SECURITY_SITE_CHECKER_DB_SAME_REGISTER_PASS_RECOMMENDATION"] = "Klein- und Gro�schreibung im Passwort erlauben.";
$MESS["SECURITY_SITE_CHECKER_DB_NO_DIT_PASS"] = "Das Datenbank-Passwort enth�lt keine Zahlen.";
$MESS["SECURITY_SITE_CHECKER_DB_NO_DIT_PASS_RECOMMENDATION"] = "Zahlen zum Passwort hinzuf�gen.";
$MESS["SECURITY_SITE_CHECKER_DB_NO_SIGN_PASS"] = "Das Datenbank-Passwort enth�lt keine Satzzeichen.";
$MESS["SECURITY_SITE_CHECKER_DB_NO_SIGN_PASS_RECOMMENDATION"] = "Satzzeichen zum Passwort hinzuf�gen.";
$MESS["SECURITY_SITE_CHECKER_DB_MIN_LEN_PASS"] = "Das Datenbank-Passwort enth�lt weniger als 8 Zeichen.";
$MESS["SECURITY_SITE_CHECKER_DB_MIN_LEN_PASS_RECOMMENDATION"] = "Das Passwort l�nger machen.";
$MESS["SECURITY_SITE_CHECKER_WAF_OFF_DETAIL"] = "Deaktivierter Proaktiver Filter n�tzt Ihrer Website nichts.";
$MESS["SECURITY_SITE_CHECKER_WAF_OFF_RECOMMENDATION"] = "Proaktiven Filter aktivieren: <a href=\"/bitrix/admin/security_filter.php\" target=\"_blank\">Aktivieren</a>";
$MESS["SECURITY_SITE_CHECKER_ADMIN_SECURITY_LEVEL_RECOMMENDATION"] = "Sicherheitslevel der administrativen Nutzergruppe erh�hen.";
$MESS["SECURITY_SITE_CHECKER_ERROR_REPORTING_RECOMMENDATION"] = "Warnungslevel auf \"keiner\" �ndern in <a href=\"/bitrix/admin/settings.php?mid=main\" target=\"_blank\">Einstellungen des Hauptmodul</a>";
$MESS["SECURITY_SITE_CHECKER_DB_DEBUG_DETAIL"] = "In den Fehlerbehebungen von SQL-Abfragen k�nnen wichtige Informationen enthalten sein.";
$MESS["SECURITY_SITE_CHECKER_DB_DEBUG_RECOMMENDATION"] = "Deaktivieren, indem \$DBDebug auf false gesetzt wird.";
$MESS["SECURITY_SITE_CHECKER_DB_EMPTY_PASS_DETAIL"] = "Ein leeres Datenbank-Passwort kann dazu f�hren, dass Sie die Kontrolle �ber Ihr Projekt verlieren.";
$MESS["SECURITY_SITE_CHECKER_DB_SAME_REGISTER_PASS_DETAIL"] = "Das Passwort ist nicht sicher. Ihr Account des Datenbank-Nutzers ist ungesch�tzt.";
$MESS["SECURITY_SITE_CHECKER_DB_NO_DIT_PASS_DETAIL"] = "Das Passwort ist nicht sicher. Ihr Account des Datenbank-Nutzers ist ungesch�tzt.";
$MESS["SECURITY_SITE_CHECKER_DB_NO_SIGN_PASS_DETAIL"] = "Das Passwort ist nicht sicher. Ihr Account des Datenbank-Nutzers ist ungesch�tzt.";
$MESS["SECURITY_SITE_CHECKER_DB_MIN_LEN_PASS_DETAIL"] = "Das Passwort ist nicht sicher. Ihr Account des Datenbank-Nutzers ist ungesch�tzt.";
$MESS["SECURITY_SITE_CHECKER_EXCEPTION_DEBUG"] = "Erweiterter Modus der Fehlerberichte wurde aktiviert.";
$MESS["SECURITY_SITE_CHECKER_EXCEPTION_DEBUG_DETAIL"] = "Erweiterter Modus der Fehlerberichte kann vertrauliche Informationen �ber Ihr Projekt offenlegen.";
$MESS["SECURITY_SITE_CHECKER_EXCEPTION_DEBUG_RECOMMENDATION"] = "Erweiterten Modus der Fehlerberichte in .settings.php deaktivieren.";
$MESS["SECURITY_SITE_CHECKER_MODULES_VERSION"] = "Veraltete Module werden immer noch benutzt";
$MESS["SECURITY_SITE_CHECKER_MODULES_VERSION_DETAIL"] = "Es sind neue Versionen verf�gbar";
$MESS["SECURITY_SITE_CHECKER_MODULES_VERSION_RECOMMENDATION"] = "Es wird empfohlen, die Module zu aktualisieren, sobald neue Versionen verf�gbar sind: <a href=\"/bitrix/admin/update_system.php\" target=\"_blank\">Plattform aktualisieren</a>";
$MESS["SECURITY_SITE_CHECKER_MODULES_VERSION_ERROR"] = "Die Verf�gbarkeit der Plattform-Updates konnte nicht gepr�ft werden";
$MESS["SECURITY_SITE_CHECKER_MODULES_VERSION_ERROR_DETAIL"] = "SiteUpdate muss wom�glich aktualisiert werden, oder aber Ihr Abonnement f�r Aktualisierungen ist abgelaufen.";
$MESS["SECURITY_SITE_CHECKER_MODULES_VERSION_ERROR_RECOMMENDATION"] = "N�here Informationen finden Sie auf der Seite der <a href=\"/bitrix/admin/update_system.php\" target=\"_blank\">Systemaktualisierung</a>.";
$MESS["SECURITY_SITE_CHECKER_REDIRECT_OFF"] = "Der Weiterleitungsschutz ist deaktiviert";
$MESS["SECURITY_SITE_CHECKER_REDIRECT_OFF_DETAIL"] = "Eine Weiterleitung an eine beliebige Dritt-Website kann verschiedene Angriffe verursachen. Aktivieren Sie den Schutz vor Weiterleitungen, damit Ihre Website sicher ist (bei Nutzung von Standard-API).";
$MESS["SECURITY_SITE_CHECKER_REDIRECT_OFF_RECOMMENDATION"] = "Den Weiterleitungsschutz <a href=\"/bitrix/admin/security_redirect.php\" target=\"_blank\">hier</a> aktivieren.";
$MESS["SECURITY_SITE_CHECKER_DANGER_EXTENSIONS"] = "Die Liste der potenziell gef�hrlichen Dateierweiterungen ist nicht vollst�ndig.";
$MESS["SECURITY_SITE_CHECKER_DANGER_EXTENSIONS_DETAIL"] = "Die aktuelle Liste der potenziell gef�hrlichen Dateierweiterungen enth�lt nicht alle empfohlenen Werte. Sie m�ssen diese Liste immer auf dem aktuellen Stand pflegen.";
$MESS["SECURITY_SITE_CHECKER_DANGER_EXTENSIONS_RECOMMENDATION"] = "Sie k�nnen die Liste der Dateierweiterungen auf der Seite mit Einstellungen von <a href=\"/bitrix/admin/settings.php?mid=fileman\" target=\"_blank\">Datei-Manager</a> bearbeiten.";
$MESS["SECURITY_SITE_CHECKER_DANGER_EXTENSIONS_ADDITIONAL"] = "Aktuell: #ACTUAL#<br>
Empfohlen (ohne Parameter auf der Server-Seite): #EXPECTED#<br>
Fehlen: #MISSING#
";
$MESS["SECURITY_SITE_CHECKER_MODULES_VERSION_ARRITIONAL"] = "Aktualisierungen sind verf�gbar f�r:<br>#MODULES#";
?>