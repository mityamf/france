<?
$MESS["B24C_CHAT_ACCESS_DENIED"] = "Zugriff verweigert";
$MESS["B24C_CHAT_TITLE"] = "Onlinechat";
$MESS["B24C_CHAT_P1"] = "�ber den Onlinechat k�nnen Sie noch mehr Feedback von Ihren Kunden bekommen. Dieses Tool ist ideal daf�r geeignet, Kunden durch Ihre Website zu begleiten, oder ihnen bei der Produktauswahl und dann beim Bestellverfahren zu helfen.";
$MESS["B24C_CHAT_P2"] = "Nutzen Sie den Onlinechat auf Ihrer Website, damit die Zufriedenheit Ihrer Kunden noch h�her wird.";
$MESS["B24C_CHAT_LI1"] = "Beantworten Sie Fragen Ihrer Kunden in Echtzeit.";
$MESS["B24C_CHAT_LI2"] = "Die Mitarbeiter k�nnen Fragen schnell und sachlich beantworten, weil sie wissen, wof�r sich der Kunde interessiert.";
$MESS["B24C_CHAT_LI3"] = "Speichern Sie die komplette Kommunikation im CRM im Kundenprofil ab.";
$MESS["B24C_CHAT_BUTT_SETT"] = "Onlinechat konfigurieren";
$MESS["B24C_CHAT_BUTT_GET_B24"] = "Bitrix24 Onlinechat anfordern (kostenlos)";
?>