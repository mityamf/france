<?
$MESS["B24C_CRMF_TITLE"] = "CRM-Formulare";
$MESS["B24C_CRMF_DESCR1"] = "CRM-Formulare helfen Ihnen dabei, Ihre Verkaufszahlen zu erh�hen, mehr Kunden zu gewinnen, Kundenprofile rechtzeitig zu aktualisieren sowie die Ergebnisse Ihrer Kundenumfragen im CRM zu speichern.";
$MESS["B24C_CRMF_TYPES"] = "Es gibt drei Typen von CRM-Formularen:";
$MESS["B24C_CRMF_DESCR2"] = "<b>Einfache Formulare</b> werden meist zum Datensammeln verwendet: Registrierung, Feedback, CV und andere Formen der Nutzerdaten.";
$MESS["B24C_CRMF_DESCR3"] = "<b>Formulare mit Bedingungen</b> sind sehr wichtig f�r komplexe und umfangreiche Formulare. Bedingungen und Regeln, die Sie hier bei Erstellung des Formulars angeben, helfen dem Kunden, Fragen schneller zu beantworten.";
$MESS["B24C_CRMF_DESCR4"] = "<b>Zahlungsformulare</b>: diese Formulare erm�glichen es, Produkte oder Services den Kunden zur Auswahl anzuzeigen und ihnen auch verschiedene Zahlungsoptionen anzubieten.";
$MESS["B24C_CRMF_DESCR5"] = "Platzieren Sie ein CRM-Formular auf Ihrer Website (Sie k�nnen es auf einer Website einf�gen oder ein Widget hinzuf�gen) und sammeln Sie so alle m�glichen Kundendaten f�r Ihr CRM.";
$MESS["B24C_CRMF_GET_FORMS"] = "Bitrix24 CRM-Formulare �ffnen";
?>