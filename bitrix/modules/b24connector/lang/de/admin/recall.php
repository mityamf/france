<?
$MESS["B24C_REC_TITLE"] = "R�ckruf";
$MESS["B24C_REC_DESC1"] = "Geben Sie Ihren potenziellen Kunden eine Chance, zu echten Kunden zu werden.";
$MESS["B24C_REC_DESC2"] = "Das R�ckruf-Widget f�ngt einen Kunden, w�hrend dieser sich auf der Website umschaut, macht ihn auf die M�glichkeit eines kostenlosen R�ckrufs aufmerksam, ruft dann die Nummer an, die der Kunde angibt, und leitet die Konversation an verf�gbare Mitarbeiter weiter.";
$MESS["B24C_REC_DESC3"] = "Installieren Sie das Widget auf Ihrer Website und erh�hen Sie Ihre Verkaufszahlen sowie die Zufriedenheit des Kunden, indem Sie ihm eine f�r ihn kostenlose Sprachkommunikation schnell anbieten.";
$MESS["B24C_REC_GET_RECALL"] = "Einen Bitrix24 R�ckruf erstellen";
?>