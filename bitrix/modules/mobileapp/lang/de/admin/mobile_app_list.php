<?
$MESS["MOBILEAPP_ADD_APP"] = "Anwendung hinzuf�gen";
$MESS["MOBILEAPP_EDIT_APP"] = "Bearbeiten";
$MESS["MOBILEAPP_REMOVE_APP"] = "L�schen";
$MESS["MOBILEAPP_REMOVE_APP_CONFIRM"] = "M�chten Sie die Anwendung wirklich l�schen? Alle Parameter und Dateien, die mit dieser Anwendung zusammenh�ngen, werden auch gel�scht.";
?>