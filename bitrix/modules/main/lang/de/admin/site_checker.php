<?
$MESS["SC_SUBTITLE_DISK"] = "Festplattenzugriffs-�berpr�fung";
$MESS["SC_SUBTITLE_DISK_DESC"] = "Die Skripte m�ssen Schreibzugriff auf alle Dateien haben. Dies ist f�r die ordnungsgem��e Funktion des Datei-Managers, des Uploads und des Update-Systems erforderlich.";
$MESS["SC_VER_ERR"] = "Die PHP-Version ist #CUR#, erforderlich ist jedoch #REQ# oder h�her.";
$MESS["SC_MOD_XML"] = "XML Unterst�tzung";
$MESS["SC_MOD_PERL_REG"] = "Unterst�tzung f�r regul�re Ausdr�cke (Perl kompatibel)";
$MESS["SC_MOD_GD"] = "GD Library";
$MESS["SC_MOD_GD_JPEG"] = "Unterst�tzung f�r JPEG in GD";
$MESS["SC_MOD_JSON"] = "JSON kompatibel";
$MESS["SC_UPDATE_ACCESS"] = "Der Zugriff zum Update-Server";
$MESS["SC_UPDATE_ERROR"] = "Keine Verbindung zum Update-Server.";
$MESS["SC_TMP_FOLDER_PERMS"] = "Unzureichende Zugriffsberechtigung, um im tempor�ren Ordner zu schreiben.";
$MESS["SC_NO_TMP_FOLDER"] = "Tempor�rer Ordner existiert nicht.";
$MESS["ERR_NO_MODS"] = "Die erforderlichen Erweiterungen sind nicht installiert:";
$MESS["SC_ERR_DNS"] = "Der MX Eintrag f�r die Domain #DOMAIN# kann nicht angefordert werden.";
$MESS["SC_ERR_DNS_WRONG"] = "DNS-Konfiguration ist nicht korrekt. Dort muss ein einziger MX Eintrag sein: mail-001.bitrix24.com (aktuell: #DOMAIN#).";
$MESS["SC_ERR_CONNECT_MAIL001"] = "Der Mail-Server mail-001.bitrix24.com kann nicht verbunden werden.";
$MESS["ERR_NO_SSL"] = "Die SSL-Unterst�tzung ist f�r PHP nicht aktiviert";
$MESS["SC_RUS_L1"] = "Nachricht von der Seite";
$MESS["SC_TIK_SEND_SUCCESS"] = "Die Nachricht wurde erfolgreich gesendet. Bitte pr�fen Sie Ihr E-Mail-Postfach #EMAIL# ,um die Best�tigungsmail des Support-Teams zu lesen.";
$MESS["SC_TIK_TITLE"] = "E-Mail an den Technischen Support senden";
$MESS["SC_TIK_DESCR"] = "Problembeschreibung";
$MESS["SC_TIK_DESCR_DESCR"] = "Folge der Arbeitsschritte, die den Fehler verursachten, Fehlerbeschreibung,... ";
$MESS["SC_TIK_LAST_ERROR"] = "Letzte Fehlermeldung";
$MESS["SC_ERR_TEST_MAIL_PUSH"] = "Die Verbindung zu #DOMAIN# kann vom E-Mail Server nicht hergestellt werden.";
$MESS["SC_TIK_LAST_ERROR_ADD"] = "angeh�ngt";
$MESS["SC_TIK_SEND_MESS"] = "Nachricht senden";
$MESS["SC_TAB_2"] = "Zugriffs�berpr�fung";
$MESS["SC_TAB_5"] = "Technischer Support";
$MESS["SC_ERROR0"] = "Fehler!";
$MESS["SC_ERROR1"] = "Der Test ist fehlgeschlagen.";
$MESS["SC_CHECK_FILES"] = "Dateiberechtigungen �berpr�fen";
$MESS["SC_CHECK_FILES_WARNING"] = "Datei-Zugriffs�berpr�fung verursacht hohe Last auf dem Server.";
$MESS["SC_CHECK_FILES_ATTENTION"] = "Achtung!";
$MESS["SC_TEST_CONFIG"] = "Konfigurationspr�fung";
$MESS["SC_TESTING"] = "�berpr�fung l�uft...";
$MESS["SC_FILES_CHECKED"] = "Gepr�fte Dateien: <b>#NUM#</b><br>Aktueller Pfad: <i>#PATH#</i>";
$MESS["SC_FILES_OK"] = "Alle gepr�ften Dateien sind verf�gbar zum Lesen und Schreiben.";
$MESS["SC_FILES_FAIL"] = "Nicht verf�gbar zum Lesen und Schreiben (die ersten 10):";
$MESS["SC_SITE_CHARSET_FAIL"] = "Gemischte Zeichens�tze: UTF-8 und nicht UTF-8";
$MESS["SC_PATH_FAIL_SET"] = "Der Pfad zum Website-Root muss leer sein, der aktuelle Pfad ist:";
$MESS["SC_NO_ROOT_ACCESS"] = "Kein Zugriff auf den Ordner ";
$MESS["SC_SOCKET_F"] = "Sockelsupport";
$MESS["SC_CHECK_FULL"] = "Komplettpr�fung";
$MESS["SC_CHECK_UPLOAD"] = "Pr�fung des Uploadordners";
$MESS["SC_CHECK_KERNEL"] = "Kernpr�fung";
$MESS["SC_CHECK_FOLDER"] = "Ordnerpr�fung";
$MESS["SC_CHECK_B"] = "Pr�fung";
$MESS["SC_STOP_B"] = "Stop";
$MESS["SC_TEST_FAIL"] = "�ng�ltiger Serverantwort. Der Test kann nicht abgeschlossen werden.";
$MESS["SC_START_TEST_B"] = "Test starten";
$MESS["SC_STOP_TEST_B"] = "Stop";
$MESS["SC_T_SOCK"] = "Sockel verwenden";
$MESS["SC_T_UPLOAD"] = "Datei hochladen";
$MESS["SC_T_UPLOAD_BIG"] = "�ber 4 Mb gro�e Dateien hochladen";
$MESS["SC_T_UPLOAD_RAW"] = "Datei hochladen via php://input";
$MESS["SC_T_POST"] = "POST-Anfragen mit mehreren Parametern";
$MESS["SC_T_MAIL"] = "E-Mail wird gesendet";
$MESS["SC_T_MAIL_BIG"] = "Gro�e E-Mail wird gesendet (�ber 64 KB)";
$MESS["SC_T_MAIL_B_EVENT"] = "Auf ungesendete Nachrichten �berpr�fen";
$MESS["SC_T_MAIL_B_EVENT_ERR"] = "Fehler beim Versand von Systemnachrichten. Folgende Nachrichten wurden nicht gesendet:";
$MESS["SC_T_REDIRECT"] = "Lokale Weiterleitungen (LocalRedirect-Funktion)";
$MESS["SC_T_MEMORY"] = "Speicherlimit";
$MESS["SC_T_SESS"] = "Sitzung beibehalten";
$MESS["SC_T_SESS_UA"] = "Sitzung beibehalten ohne NutzerAgent";
$MESS["SC_T_CACHE"] = "Cache-Dateien verwenden";
$MESS["SC_T_AUTH"] = "HTTP-Autorisierung";
$MESS["SC_T_EXEC"] = "Dateierstellung und Ausf�hrung";
$MESS["SC_T_DBCONN"] = "Redundante Ausgabe in den Konfigurationsdateien";
$MESS["SC_T_DBCONN_SETTINGS"] = "Verbindungsparameter f�r die Datenbank";
$MESS["SC_ERR_CONN_DIFFER"] = "sind in .settings.php und dbconn.php unterschiedlich.";
$MESS["SC_T_MYSQL_VER"] = "MySQL-Version";
$MESS["SC_T_TIME"] = "Datenbank- und Webserverzeiten";
$MESS["SC_T_SQL_MODE"] = "MySQL-Modus";
$MESS["SC_T_CHARSET"] = "Datenbanktabellenzeichensatz";
$MESS["SC_T_STRUCTURE"] = "Datenbank-Struktur";
$MESS["SC_DB_CHARSET"] = "Zeichenkodierung der Datenbank";
$MESS["SC_MBSTRING_NA"] = "Pr�fung ist wegen der UTF-Konfigurationsfehler fehlgeschlagen";
$MESS["SC_CONNECTION_CHARSET"] = "Zeichenkodierung der Verbindung";
$MESS["SC_TABLES_NEED_REPAIR"] = "Tabellenintegrit�t ist verletzt, eine Korrektur ist erforderlich.";
$MESS["SC_TABLE_ERR"] = "Fehler in der Tabelle #VAL#:";
$MESS["SC_T_CHECK"] = "Tabellenpr�fung";
$MESS["SC_TEST_SUCCESS"] = "Erfolg";
$MESS["SC_SENT"] = "Gesendet in:";
$MESS["SC_SEC"] = "Sek.";
$MESS["SC_DB_ERR"] = "Fehler bei der Datenbankversion:";
$MESS["SC_DB_ERR_MODE"] = "Die sql_mode Variable in MySQL muss leer sein. Aktueller Wert:";
$MESS["SC_NO_PROXY"] = "Kann keine Verbindung zum Proxyserver aufbauen.";
$MESS["SC_PROXY_ERR_RESP"] = "Ung�lgige Antwort des Updateservers wegen dem Proxy";
$MESS["SC_UPDATE_ERR_RESP"] = "Ung�ltige Antwort des Updateservers.";
$MESS["SC_FILE_EXISTS"] = "Datei vorhanden:";
$MESS["SC_WARN_SUHOSIN"] = "Der Suhosin-Modul wurde geladen, es k�nnen Probleme mit dem administrativen Panel auftreten.";
$MESS["SC_WARN_SECURITY"] = "Der mod_security-Modul wurde geladen, es k�nnen Probleme mit dem administrativen Panel auftreten.";
$MESS["SC_WARN_DAV"] = "WebDav ist deaktiviert, weil das Modul mod_dav/mod_dav_fs geladen wird.";
$MESS["SC_DELIMITER_ERR"] = "Aktuelles Trennzeichen: &quot;#VAL#&quot;, &quot;.&quot; ist erforderlich.";
$MESS["SC_DB_MISC_CHARSET"] = "Der Zeichensatz (#T_CHAR#) der Tabelle #TBL# entspricht nicht dem Datenbankzeichensatz (#CHARSET#).";
$MESS["SC_COLLATE_WARN"] = "Der Vergleichswert f�r &quot;#TABLE#&quot; (#VAL0#) weicht von dem Datenbankwert (#VAL1#) ab. ";
$MESS["SC_TABLE_BROKEN"] = "Die Tabelle &quot;#TABLE#&quot; wurde infolge von einem internen MySQL Fehler zerst�rt. Automatische Wiederherstellung wird eine leere Tabelle erstellen.";
$MESS["SC_TABLE_CHARSET_WARN"] = "Die &quot;#TABLE#&quot;-Tabelle enth�lt Felder, die in der Kodierung nicht mit der Datenbankkdierung �bereinstimmen. ";
$MESS["SC_FIELDS_COLLATE_WARN"] = "Das Ergebnis des Feldes &quot;#FIELD#&quot; in der Tabelle &quot;#TABLE#&quot;  (#VAL1#) stimmt nicht mit dem der Datenbank (#VAL1#) �berein.";
$MESS["SC_TABLE_SIZE_WARN"] = "Die Gr��e der &quot;#TABLE#&quot;-Tabelle ist m�glicherweise zu gro� (#SIZE# M).";
$MESS["SC_NOT_LESS"] = "Nicht kleiner als #VAL# M.";
$MESS["SC_MEMORY_CHANGED"] = "Der Wert von memory_limit wurde erh�ht von #VAL0# auf #VAL1#, w�hrend beim Testen ini_set genutzt wurde.";
$MESS["SC_CRON_WARN"] = "Die Konstante BX_CRONTAB_SUPPORT ist in /bitrix/php_interface/dbconn.php definiert, dabei m�ssen Agenten via Cron gestartet werden.";
$MESS["SC_CACHED_EVENT_WARN"] = "Informationen �ber das Versenden von E-Mails sind im Cache, was ein Fehler sein mag. Versuchen Sie Cache zu leeren.";
$MESS["SC_TIK_ADD_TEST"] = "Testlog senden";
$MESS["SC_SUPPORT_COMMENT"] = "Wenn Sie Probleme mit dem Nachrichtenversand haben, verwenden Sie sich bitte die Kontaktform auf unserer Website:";
$MESS["SC_NOT_FILLED"] = "Problembeschreibung erforderlich.";
$MESS["SC_TEST_WARN"] = "Der Konfigurationsbericht wird abgeschlossen.
Wenn Fehler auftreten, bitte entfernen Sie die Markierung \"Testlog senden\", und versuchen Sie es noch ein mal.";
$MESS["SC_SOCK_NA"] = "Pr�fung ist wegen Socket-Fehler fehlgeschlagen.";
$MESS["SC_T_CLONE"] = "Objekt-�bergabe mit dem Verweis";
$MESS["SC_T_GETIMAGESIZE"] = "Getimagesize-Unterst�tzung f�r SWF";
$MESS["SC_TEST_DOMAIN_VALID"] = "Die aktuelle Domain ist ung�ltig (#VAL#). Der Domainname kann nur Ziffern, lateinische Buchstaben und Bindestriche enthalten. Die Top-Level-Domain muss durch einen Punkt getrennt werden (z.B. .com).";
$MESS["SC_SWF_WARN"] = "Einf�gen der SWF-Objekte wird eventuell nicht funktionieren.";
$MESS["SC_TIME_DIFF"] = "Der Zeitunterschied betr�gt #VAL# Sekunden.";
$MESS["SC_T_MODULES"] = "Erforderliche PHP-Module";
$MESS["SC_MOD_MBSTRING"] = "Mbstring-Unterst�tzung";
$MESS["SC_MB_UTF"] = "Die Website funktioniert in der UTF-Kodierung";
$MESS["SC_MB_NOT_UTF"] = "Die Website funktioniert in der Einzelbyte-Kodierung";
$MESS["SC_MB_CUR_SETTINGS"] = "Mbstring-Parameter:";
$MESS["SC_MB_REQ_SETTINGS"] = "Erforderlich:";
$MESS["SC_T_MBSTRING"] = "Parameter der UTF-Konfiguration (mbstring und BX_UTF)";
$MESS["SC_T_SITES"] = "Website-Parameter";
$MESS["SC_BX_UTF"] = "Benutzen Sie folgenden Code in <i>/bitrix/php_interface/dbconn.php</i>:
<code>define('BX_UTF', true);</code> 
";
$MESS["SC_BX_UTF_DISABLE"] = "Die Konstante BX_UTF muss nicht bestimmt werden";
$MESS["SC_T_PHP"] = "Erforderliche PHP-Parameter";
$MESS["SC_ERR_PHP_PARAM"] = "Der Parameter #PARAM# ist #CUR#, erforderlich ist jedoch #REQ#.";
$MESS["SC_MYSQL_ERR_VER"] = "Aktuell ist die MySQL-Version #CUR# installiert, erforderlich ist jedoch #REQ#.";
$MESS["SC_T_SERVER"] = "Server-Variablen";
$MESS["SC_CONNECTION_CHARSET_WRONG"] = "Die Zeichenkodierung der Verbindung mit der Datenbank muss #VAL# sein, der aktuelle Wert ist #VAL1#.";
$MESS["SC_CONNECTION_CHARSET_WRONG_NOT_UTF"] = "Die Zeichenkodierung der Verbindung mit der Datenbank soll nicht UTF-8 sein, der aktuelle Wert ist: #VAL#.";
$MESS["SC_CONNECTION_COLLATION_WRONG_UTF"] = "Die alphabetische Sortierung der Verbindung mit der Datenbank muss utf8_unicode_ci sein, der aktuelle Wert ist #VAL#.";
$MESS["SC_TABLE_CHECK_NA"] = "Pr�fung ist wegen eines Fehlers der Datenbank-Zeichenkodierung fehlgeschlagen.";
$MESS["SC_TABLE_COLLATION_NA"] = "�berpr�fung wurde wegen Fehler der Tabellenkodierung nicht durchgef�hrt";
$MESS["SC_FIX"] = "Korrigieren";
$MESS["SC_FIX_DATABASE"] = "Datenbank-Fehler korrigieren";
$MESS["SC_FIX_DATABASE_CONFIRM"] = "Das System wird jetzt versuchen, die Datenbank-Fehler zu korrigieren. Diese Aktion kann gef�hrlich sein. Erstellen Sie eine Datenbank-Sicherungskopie, bevor Sie weitere Schritte unternehmen.

Fortfahren?";
$MESS["SC_CHECK_TABLES_ERRORS"] = "Datenbank-Tabellen enthalten #VAL# Fehler der Zeichenkodierung, #VAL1# von ihnen k�nnen automatisch korrigiert werden.";
$MESS["SC_CONNECTION_CHARSET_NA"] = "Pr�fung ist wegen eines Fehlers der Verbindungskodierung fehlgeschlagen.";
$MESS["SC_DATABASE_COLLATION_DIFF"] = "Die alphabetische Sortierung der Datenbank (#VAL1#) stimmt nicht mit der alphabetischen Sortierung der Verbindung  (#VAL0#) �berein.";
$MESS["SC_DATABASE_CHARSET_DIFF"] = "Die Datenbank-Zeichenkodierung (#VAL1#) stimmt nicht mit der Zeichencodierung der alphabetischen Sortierung (#VAL0#) �berein.";
$MESS["SC_HELP_NOTOPIC"] = "Zu diesem Thema gibt es leider keine Informationen.";
$MESS["SC_HELP_CHECK_INSTALL_SCRIPTS"] = "Manchmal k�nnen Nutzer vergessen, die Installationsskripts (restore.php, bitrixsetup.php) zu l�schen, nachdem das System wiederhergestellt oder installiert wurde. Das kann zu einem ernsthaften Sicherheitsproblem werden und  zum eventuellen Website-Hijacking f�hren. Wenn Sie die Meldung �ber das automatische L�schen ignoriert haben, m�ssen Sie diese Dateien manuell entfernen.";
$MESS["SC_HELP_CHECK_MAIL_PUSH"] = "Die Funktion <a href=\"https://helpdesk.bitrix24.ru/open/1602367/\" target=_blank>Aisrichtung der Nachricht</a> wird die Nachrichten aus E-Mails im Activity Stream ver�ffentlichen, sodass auch andere Nutzer, welche keinen Account in Ihrem Bitrix24 haben, an der Diskussion teilnehmen k�nnen.

Sie m�ssen <a href=\"https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=71&LESSON_ID=7655&LESSON_PATH=6415.6420.3698.7655\" target=_blank>DNS konfigurieren</a>, damit Ihr Bitrix24 auch von au�en erreichbar wird.";
$MESS["SC_HELP_CHECK_PHP_MODULES"] = "Hier werden die f�r das System erforderlichen PHP-Erweiterungen gepr�ft. Fehlen einige solche Erweiterungen, dann werden die Module angezeigt, welche ohne diese Erweiterungen nicht funktionieren k�nnen.

Um fehlende PHP-Erweiterungen hinzuzuf�gen, kontaktieren Sie den Technischen Support Ihres Hosting-Anbieters. Wenn Sie das System auf einem lokalen Computer installieren, werden Sie diese Erweiterungen manuell installieren m�ssen. Benutzen Sie daf�r Dokumentation auf php.net.
";
$MESS["SC_HELP_CHECK_PHP_SETTINGS"] = "Hier werden kritische Parameter gepr�ft, die in der Datei php.ini bestimmt werden. Bei Fehlern werden nicht korrekt eingestellte Parameter angezeigt. Eine detaillierte Parameterbeschreibung finden Sie auf php.net.";
$MESS["SC_HELP_CHECK_SERVER_VARS"] = "Hier werden die Server-Variablen gepr�ft.

Der Wert von HTTP_HOST ist vom aktuellen virtuellen Host (der Domain) abgeleitet. Einige Browser k�nnen Cookies f�r nicht korrekte Domainnamen nicht speichern, weswegen auch eine Cookie-Autorisierung nicht m�glich sein wird.
";
$MESS["SC_HELP_CHECK_MBSTRING"] = "Das Modul mbstring ist erforderlich, um mit mehreren Sprachen arbeiten zu k�nnen. Die Einstellungen dieses Moduls m�ssen sehr genau bestimmt werden in Abh�ngigkeit von der aktuellen Website-Kodierung: Die Parameter der UTF-8 Kodierung unterscheiden sich von denen einer nationalen Zeichenkodierung (z.B. cp1252).

Folgende Parameter sind f�r die Websites auf der Basis von UTF-8 erforderlich: 
<b>mbstring.func_overload=2</b>
<b>mbstring.internal_encoding=utf-8</b>

Der erste Parameter leitet Aufrufe der Funktionen von PHP-Zeichenketten implizit auf mbstring Funktionen um. Der zweite Parameter bestimmt die Text-Kodierung.

Wenn Ihre Website nicht UTF-8 benutzt, muss der erste Parameter 0 sein:
<b>mbstring.func_overload=0</b>

Wenn Sie aus irgend einem Grund die Funktionsumleitung nicht deaktivieren k�nnen, versuchen Sie eine Einzelbyte-Kodierung zu benutzen:
<b>mbstring.func_overload=2</b>
<b>mbstring.internal_encoding=latin1</b>

Wenn die festgelegten Werte mit den Parametern der Website nicht �bereinstimmen, werden Sie merkw�rdige und bizarre Fehler wie etwa abgeschnittene W�rter, den nicht funktionierenden XML-Import etc. feststellen.

<b>Beachten Sie,</b> dass der Parameter <b>mbstring.func_overload</b>in der globalen Datei  php.ini (oder in httpd.conf f�r einen virtuellen Server) bestimmt wird, w�hrend sich der Parameter der Kodierung inn .htaccess befindet. 

Alle Bitrix Module benutzen die Konstante <i>BX_UTF</I>, um die aktuelle Kodierung zu erkennen. Eine UTF-8-Website erfordert einen folgenden Code in <i>/bitrix/php_interface/dbconn.php</i>:
<code>define('BX_UTF', true);</code>
";
$MESS["SC_HELP_CHECK_SITES"] = "Allgemeine Multisite-Parameter werden gepr�ft. Wenn f�r eine Website der Pfad zum Root-Verzeichnis angegeben ist (was nur dann erforderlich ist, wenn die Websites auf verschiedenen Domains existieren), muss dieses Verzeichnis eine symbolische Verlinkung zum beschreibbaren \"bitrix\" Ordner enthalten.

Alle Websites, die mit demselben Bitrix System eingerichtet wurden, m�ssen dieselbe Kodierung benutzen: Entweder UTF-8 oder Einzelbyte-Kodierung.
";
$MESS["SC_HELP_CHECK_SOCKET"] = "Hier wird der Web-Server eingestellt, um die Verbindung mit sich selbst herstellen zu k�nnen. Dies ist erforderlich, um die Netzwerk-Funktionen zu pr�fen und einige nachfolgende Tests durchzuf�hren.

Wenn dieser Test fehlschl�gt, k�nnen nachfolgende Tests, bei denen ein extra PHP-Prozess ben�tigt wird, nicht durchgef�hrt werden. If this test fails, the subsequent tests requiring a child PHP process cannot be performed. Dieses Problem kann durch eine Firewall, einen beschr�nkten IP-Zugriff oder eine HTTP/HTLM Autorisierung verursacht werden. Deaktivieren Sie diese Funktionen, wenn Sie den Test durchf�hren.
";
$MESS["SC_HELP_CHECK_DBCONN_SETTINGS"] = "Dieser Test wird Verbindungsparameter f�r die Datenbank, welche in <i>/bitrix/php_interface/dbconn.php</i> angegeben sind mit denen in <i>/bitrix/.settings.php</i> vergleichen. 
Diese Einstellungen m�ssen in beiden Dateien gleich sein. Anderenfalls werden einige SQL-Anfragen an eine andere Datenbank umgeleitet, was unberechenbare Folgen verursachen kann.

Der neue D7-Kernel benutzt Parameter aus <i>.settings.php</i>. Aufgrund einer R�ckkompatibilit�t kann auf <i>dbconn.php</i> nicht verzichtet werden.

Wenn in <i>.settings.php</i> Verbindungsparameter nicht angegeben sind, benutzt der neue Kernel die aus <i>dbconn.php</i>.
";
$MESS["SC_HELP_CHECK_DBCONN"] = "Hier wird die Textausgabe in den Konfigurationsdateien  <i>dbconn.php</i> und <i>init.php</i> gepr�ft.

Selbst ein Leerzeichen oder ein Zeilenumbruch k�nnen dazu f�hren, dass eine komprimierte Seite von dem Client-Browser nicht entpackt und gelesen werden kann.

Dar�ber hinaus k�nnen Probleme mit Autorisierung und CAPTCHA auftreten.
";
$MESS["SC_HELP_CHECK_UPLOAD"] = "Hier wird versucht, die Verbindung mit dem Web-Server herzustellen und bin�re Daten als eine Datei zu �bertragen. Der Server wird dann die erhaltenen Daten mit den urspr�nglichen vergleichen. Wird ein Problem entstehen, so kann es durch einige Parameter in <i>php.ini</I> verursacht werden, denn diese Datei l�sst �bertragung von bin�ren Daten nicht zu, oder durch einen nicht verf�gbaren tempor�ren Ordner (oder <i>/bitrix/tmp</i>).

Soll das Problem auftreten, kontaktieren Sie Ihren Hosting-Anbieter. Wenn Sie das System auf einem lokalen Computer installieren, werden Sie den Server manuell konfigurieren m�ssen.
";
$MESS["SC_HELP_CHECK_UPLOAD_BIG"] = "Hier wird eine gro�e bin�re Datei (�ber 4 Mb) hochgeladen. Wenn nun dieser Test fehlschl�gt, der vorherige jedoch erfolgreich war, kann das Problem in der Einschr�nkung in php.ini (<b>post_max_size</b> oder <b>upload_max_filesize</b>) liegen. Benutzen Sie phpinfo, um aktuelle Werte zu setzen (Einstellungen - Tools - Konfiguration - PHP Einstellungen).

Auch ein unzureichender Festplattenspeicher kann dieses Problem verursachen.
";
$MESS["SC_HELP_CHECK_UPLOAD_RAW"] = "Sendet Bin�rdaten im K�rper einer POST-Anfrage. Auf der Serverseite k�nnen diese Daten manchmal besch�digt werden: In diesem Fall wird der Flach-Lader f�r Bilder nicht funktionieren.";
$MESS["SC_HELP_CHECK_POST"] = "Hier wird eine POST-Anfrage mit mehreren Parametern gesendet. Einige Softwares, die den Server sch�tzen, beispielsweise \"suhosin\", k�nnen ausf�hrliche Anfragen blockieren. In diesem Fall k�nnen die Informationsblockelemente meistens nicht gespeichert werden.";
$MESS["SC_HELP_CHECK_MAIL"] = "Hier wird eine E-Mail-Nachricht an hosting_test@bitrixsoft.com via PHP-Standardfunktion \"mail\" gesendet. Dabei gibt es ein spezielles Postfach, damit die Testbedingungen an die des wirklichen Lebens maximal angepasst werden. 

Dieser Test sendet das Skript der Seitenpr�fung als eine Testnachricht, aber  <b>er sendet nie irgendwelche Nutzerdaten</b>.

Beachten Sie, dass der Test den Nachrichtempfang nicht �berpr�ft. Der Empfang bei den anderen Postf�chern kann ebenso nicht �berpr�ft werden.

Wenn Versenden von E-Mails l�nger als eine Sekune dauern, kann die Server-Leistungsst�rke wesentlich beeintr�chtigt werden. Kontaktieren Sie den Technischen Support Ihres Hosting-Anbieters, damit er das Versenden der E-Mails �ber einen Spooler konfiguriert.

Alternativ k�nnen Sie cron benutzen, um die E-Mails zu versenden. Daf�r f�gen Sie <code>define('BX_CRONTAB_SUPPORT', true);</code> zu dbconn.php hinzu. Dann stellen Sie cron ein, <i>php /var/www/bitrix/modules/main/tools/cron_events.php</I> jede Minute auszuf�hren (ersetzen Sie <i>/var/www</i> durch das Root-Verzeichnis Ihrer Website).

Wenn der Aufruf der Funktion mail() fehlgeschlagen wurde, werden Sie die E-Mails von Ihrem Server mit Standardverfahren nicht versenden k�nnen.

Wenn Ihr Hosting-Anbieter alternative Services zum Versenden von E-Mails anbietet, k�nnen Sie diese via Funktion \"custom_mail\" benutzen. Bestimmen Sie diese Funktion in <i>/bitrix/php_interface/dbconn.php</I>. Wird diese Funktion bestimmt, wird sie im System anstatt der PHP-Funktion \"mail\" mit denselben Ausgabeparametern benutzt.
";
$MESS["SC_HELP_CHECK_MAIL_BIG"] = "Beim Versenden einer umfangreichen Nachricht wird der Text der vorherigen Mail (Skript der Seitenpr�fung) 10 Mal wiederholt. Dar�ber hinaus wird die Betreff-Zeile in zwei Zeilen aufgeteilt sowie das BCC-Feld zum Senden an noreply@bitrixsoft.com hinzugef�gt.

Wenn der Server nicht korrekt konfiguriert ist, k�nnen solche Nachrichten nicht versendet werden.

Sollten etwaige Probleme entstehen, kontaktieren Sie Ihren Hosting-Anbieter. Wenn Sie das System auf einem lokalen Computer installieren, werden Sie den Server manuell konfigurieren m�ssen.
";
$MESS["SC_HELP_CHECK_MAIL_B_EVENT"] = "In der Datenbank-Tabelle B_EVENT werden die E-Mail-Warteschlangen von der Website gespeichert sowie die Aktivit�ten zum Versenden der E-Mails registriert. Wenn einige Nachrichten nicht versendet werden k�nnen, sind m�gliche Problemursachen eine ung�ltige Empf�ngeradresse, nicht korrekte Parameter der E-Mail-Vorlage oder das E-Mailsystem des Servers.";
$MESS["SC_HELP_CHECK_LOCALREDIRECT"] = "Nachdem das Formular des administrativen Bereichs gespeichert ist (also auf Speichern oder Anwenden geklickt wurde), wird der Client auf die urspr�ngliche Seite umgeleitet. Das wird gemacht, um wiederholte Formulareintr�ge zu vermeiden, wenn ein Nutzer die Seite aktualisiert. Damit diese Umleitung erfolgreich funktioniert, muss eine ganze Reihe von wichtigen Variablen auf dem Web-Server korrekt bestimmt sind sowie das �berschreiben der http-�berschriften erlaubt ist.

Wenn einige der Server-Variablen in <i>dbconn.php</i> neu bestimmt wurden, wird der Test eben diese Neubestimmungen benutzen. Mit anderen Worten, bei der Umleitung werden reale Lebenssituationen komplett simuliert.
";
$MESS["SC_HELP_CHECK_MEMORY_LIMIT"] = "Bei diesem Test wird ein extra PHP-Prozess erstellt, um eine Variable mit der schrittweise inkrementierten Gr��e zu generieren. Zum Schluss wird dadurch der Speicherumfang festgelegt, welcher f�r den PHP-Prozess verf�gbar sein wird.

PHP bestimmt die Speichereinschr�nkungen in php.ini , indem der Parameter <b>memory_limit</b> eingestellt wird. Aber Sie sollten diesem Parameter nicht vertrauen, da auf den Hostings auch noch weitere Einschr�nkungen gesetzt werden k�nnen.

Der Test versucht, den Wert von <b>memory_limit</b> zu erh�hen, indem er den folgenden Code benutzt:
<code>ini_set(&quot;memory_limit&quot;, &quot;512M&quot;)</code>

Wenn der aktuelle Wert kleiner ist, f�gen Sie die Zeile vom Code zu <i>/bitrix/php_interface/dbconn.php</i> hinzu.
";
$MESS["SC_HELP_CHECK_SESSION"] = "Dieser Test pr�ft, ob der Server die Daten mithilfe von Sitzungen speichern kann. Das ist erforderlich, damit die Autorisierung zwischen den Hits verf�gbar bleibt.

Dieser Test wird fehlschlagen, wenn auf dem Server die Unterst�tzung f�r Sitzungen nicht installiert ist, ein nicht g�ltiges Sitzungsverzeichnis angegeben ist oder wenn dieses Verzeichnis schreibgesch�tzt ist.
";
$MESS["SC_HELP_CHECK_SESSION_UA"] = "Hier wird die F�higkeit gepr�ft, Sitzungen zu speichern, ohne dabei die http-�berschrift <i>User-Agent</i> einzustellen. 

Mehrere externe Anwendungen und Add-Ons stellen diese �berschrift nicht ein, beispielsweise Uploader f�r Dateien und Fotos, WebDav-Clients etc. 

Wenn der Test fehlschl�gt, liegt das Problem h�chstwahrscheinlich bei der nicht korrekten Konfiguration des PHP-Moduls <b>suhosin</b>.
";
$MESS["SC_HELP_CHECK_CACHE"] = "Bei diesem Test wird gepr�ft, ob ein PHP-Prozess eine <b>.tmp</b> Datei im Cache-Verzeichnis erstellen und diese dann zu <b>.php</b> umbenennen kann. Einige Web-Server f�r Windows k�nnen beim Umbenennen versagen, wenn die Nutzer-Zugriffsberechtigungen nicht korrekt eingestellt sind.";
$MESS["SC_HELP_CHECK_UPDATE"] = "Hier wird versucht, mithilfe von den aktuellen Einstellungen des Hauptmoduls eine Testverbindung zum Update-Server herzustellen. Kann die Verbindung nicht hergestellt werden, werden Sie die Updates nicht installieren oder Ihre Testversion aktivieren k�nnen.

Die wahrscheinlichen Problemursachen  daf�r sind nicht korrekte Proxy-Einstellungen, Firewall-Einschr�nkungen oder ung�ltige Netzwerk-Einstellungen des Servers.
";
$MESS["SC_HELP_CHECK_HTTP_AUTH"] = "Mithilfe von den HTTP-�berschriften werden bei diesem Test die Autorisierungsdaten gesendet. Dann wird versucht, diese Daten mit der Server-Variablen REMOTE_USER (oder REDIRECT_REMOTE_USER) zu bestimmen. Die HTTP-Autorisierung ist f�r die Integration mit den Softwares der Dritthersteller erforderlich.



Wenn PHP im Modus CGI/FastCGI funktioniert (fragen Sie dies bei Ihrem Hosting-Anbieter nach), verlangt der Apache-Server das Modul mod_rewrite module und eine folgende Regel in .htaccess:

<b>RewriteRule .* - [E=REMOTE_USER:%{HTTP:Authorization}]</b>



Wenn m�glich, konfigurieren Sie PHP als ein Apache-Modul.
";
$MESS["SC_HELP_CHECK_EXEC"] = "Wenn PHP im Modus CGI/FastCGI auf einem Unix-System funktioniert, verlangen Skripts bestimmte Berechtigungen zur Ausf�hrung, sonst werden sie nicht funktionieren.

Wenn dieser Test fehlschl�gt, kontaktieren Sie den Technischen Support Ihres Hosting-Anbieters, um ben�tigte Zugriffsberechtigungen f�r Dateien zu bekommen und stellen Sie dann die Konstanten <b>BX_FILE_PERMISSIONS</b> und <b>BX_DIR_PERMISSIONS</b> in <i>dbconn.php</i> entsprechend ein.



Wenn m�glich, konfigurieren Sie PHP als ein Apache-Modul.
";
$MESS["SC_HELP_CHECK_BX_CRONTAB"] = "Um die aperiodischen Agenten und die E-Mail auf cron zu �bertragen, f�gen Sie die folgende Konstante zu <i>/bitrix/php_interface/dbconn.php</i> hinzu:

<code>define('BX_CRONTAB_SUPPORT', true);</code>



Wenn bei dieser Konstante der Wert \"true\" gesetzt wird, werden im System nur periodische Agenten bei Hits ausgef�hrt. Nun f�gen Sie zu cron eine Aufgabe hinzu, das Skript <i>/var/www/bitrix/modules/main/tools/cron_events.php</i> jede Minute auszuf�hren (ersetzen Sie  <i>/var/www</i> durch den Pfad zum Root-Verzeichnis Ihrer Website).



Das Skript bestimmt die Konstante <b>BX_CRONTAB</b>, die zeigt, dass das Skript von cron aus aktiviert ist und nur aperiodische Agenten ausf�hrt. Wenn Sie diese Konstante aus Versehen in <i>dbconn.php</i> bestimmen, werden periodische Agenten nie ausgef�hrt.
";
$MESS["SC_HELP_CHECK_SECURITY"] = "Das Apache-Modul  mod_security ist genauso wie das PHP-Modul suhosin dazu gedacht, die Website gegen Hacker zu sch�tzen. In der Tat st�rt es aber meistens, normale Nutzeraktivit�ten auszuf�hren. Es wird also empfohlen, das Standardmodul \"Proaktiver Schutz\" anstatt von mod_security zu benutzen.";
$MESS["SC_HELP_CHECK_CLONE"] = "Seit der Version 5 werden im PHP die Objekte eher mit dem Verweis �bertragen als kopiert. Es gibt aber nach wie vor PHP 5 Sets, die das Vererben unterst�tzen, so dass Objekte als Kopien �bertragen werden.



Um dieses Problem zu l�sen, laden Sie einen aktuelleren PHP 5 Set herunter und installieren Sie ihn.
";
$MESS["SC_HELP_CHECK_GETIMAGESIZE"] = "Wenn Sie ein Flash-Objekt hinzuf�gen, braucht der visuelle Editor die Objektgr��e zu erkennen. Dazu f�hrt er die PHP-Standardfunktion <b>getimagesize</b> aus, welche die Erweiterung <b>Zlib</b> erfordert. Bei komprimierten Flash-Objekten kann diese Funktion fehlschlagen, wenn die Erweiterung  <b>Zlib</b> als ein Modul installiert ist. Die Erweiterung muss also statisch aufgebaut werden.



Zur L�sung dieses Problem kontaktieren Sie den Technischen Support Ihres Hosting-Anbieters.
";
$MESS["SC_HELP_CHECK_MYSQL_BUG_VERSION"] = "Es gibt einige MySQL-Versionen, in denen Fehler enthalten sind, welche ein fehlerhaftes Funktionieren der Website verursachen k�nnen.
<b>4.1.21</b> - Sortierung funktioniert unter bestimmten Bedingungen nicht korrekt;
<b>5.0.41</b> - Die Funktion EXISTS funktioniert nicht korrekt; die Suchfunktionen lassen nicht korrekte Ergebnisse anzeigen;
<b>5.1.34</b> - Der Schritt auto_increment ist standardm��ig 2, w�hrend 1 erforderlich ist.
<b>5.1.66</b> - Falsche Z�hler der Forenthemen. Als Ergebnis kann m�glichweise die Seite des Nutzers fehlerhaft funktionieren.

Haben Sie bei Ihnen eine dieser MySQL-Versionen installiert, sollten Sie MySQL aktualisieren.";
$MESS["SC_HELP_CHECK_MYSQL_TIME"] = "Hier werden die Systemzeiten der Datenbank und des Web-Servers verglichen. Diese k�nnen nicht synchron laufen, wenn sie auf zwei verschiedenen Maschinen installiert sind, aber �fter passiert das wegen einer nicht korrekten Einstellung der Zeitzone.

Die PHP-Zeitzone kann hier eingestellt werden: <i>/bitrix/php_interface/dbconn.php</i>, z.B.:
<code>date_default_timezone_set(&quot;Europe/Berlin&quot;);</code> (benutzen Sie Ihre Region und Stadt)

Die Datenbank-Zeitzone kann eingestellt werden, indem ein folgender Code hinzugef�gt wird: <i>/bitrix/php_interface/after_connect_d7.php</i>:
<code>\$connection = Bitrix\\Main\\Application::getConnection(); 
\$connection->queryExecute(&quot;SET LOCAL time_zone='&quot;.date('P').&quot;'&quot;);</code>

N�heres entnehmen Sie der Seite http://en.wikipedia.org/wiki/List_of_tz_database_time_zones, um die Liste der standardm��igen Regionen und St�dte zu bekommen.
";
$MESS["SC_HELP_CHECK_MYSQL_MODE"] = "Der Parameter <i>sql_mode</i> bestimmt die Arbeitsweise von MySQL. Dieser Parameter kann Werte enthalten, die mit Bitrix nicht kompatibel sind. Um die standardm��ige Arbeitsweise auszuw�hlen, f�gen Sie den folgenden Code hinzu: <i>/bitrix/php_interface/after_connect_d7.php</i>:
<code>\$connection = Bitrix\\Main\\Application::getConnection(); 
\$connection->queryExecute(&quot;SET sql_mode=''&quot;);</code>
";
$MESS["SC_HELP_CHECK_MYSQL_TABLE_CHARSET"] = "Die Zeichenkodierung aller Tabellen und Felder muss mit der Zeichenkodierung der datenbank �bereinstimmen. Unterscheidet sich die Zeichenkodierung von irgendeiner der Tabellen, m�ssen Sie dies manuell mithilfe von SQL-Befehlen korrigieren.



Die alphabetische Sortierung der Tabellen muss mit der alphabetischen Sortierung der Datenbank �bereinstimmen. Sind die Zeichenkodierungen korrekt konfiguriert, dann wird das Nicht�bereinstimmen der alphabetischen Sortierungen automatisch korrigiert.



<b>Achtung!</b> Erstellen Sie immer eine komplette Sicherungskopie von der Datenbank, bevor Sie die Zeichenkodierung �ndern.
";
$MESS["SC_HELP_CHECK_MYSQL_TABLE_STATUS"] = "In diesem Test werden die MySQL-�blichen Mechanismen zur Tabellenpr�fung verwendet. Wird der Test eine oder mehrere besch�digte Tabellen feststellen, wird Ihnen vorgeschlagen, Korrekturen vorzunehmen.";
$MESS["SC_HELP_CHECK_MYSQL_DB_CHARSET"] = "Bei diesem Test wird gepr�ft, ob die Zeichenkodierung und alphabetische Sortierung der Datenbank mit denen der Verbindung �bereinstimmen. MySQL verwendet diese Parameter, um neue Tabellen zu erstellen.



Solche Fehler, falls sie auftreten, k�nnen automatisch korrigiert werden, wenn der aktuelle Nutzer Schreibrechte f�r die Datenbank hat (ALTER DATABASE).
";
$MESS["SC_HELP_CHECK_MYSQL_CONNECTION_CHARSET"] = "Hier werden die Codierung und der Verglich gepr�ft, welche bei Daten�bertragung an den MySQL-Server verwendet werden.

F�r eine Website in der Codierung <i>utf8</i> muss die Codierung <i>utf8</i> sein, und der Vergleich <i>utf8_unicode_ci</i>.Wenn die Website in der Codierung <i>cp1251</i> ist, muss die Verbindung auch diese Codierung nutzen.

Um die Codierung der Verbindung zu �ndern, f�gen Sie in <i>/bitrix/php_interface/after_connect_d7.php</i> den folgenden Code ein (ein Beispiel f�r <i>utf8</i>):
<code>\$connection = Bitrix\\Main\\Application::getConnection(); 
\$connection->queryExecute('SET NAMES &quot;utf8&quot;');</code>
Um den Vergleich genau einzustellen, f�gen Sie <b>nach der Angabe der Codierung</b> folgenden Code ein:
<code>\$connection->queryExecute('SET collation_connection = &quot;utf8_unicode_ci&quot;');</code>
<b>Wichttig!</b>Nachdem neue Werte definiert werden, stellen Sie sicher, dass die Daten auf der Website korrekt angezeigt werden.
";
$MESS["SC_READ_MORE_ANC"] = "Mehr Informationen finden Sie im <a href=\"#LINK#\" target=_blank>Protokoll der System�berpr�fung</a>.";
$MESS["SC_CHARSET_CONN_VS_RES"] = "Die Verbindungskodierung (#CONN#) unterscheidet sich von der Ergebniskodierung (#RES#).";
$MESS["SC_STRLEN_FAIL_PHP56"] = "String-Funktionen arbeiten nicht korrekt. Das kan durch einen PHP 5.6 Fehler verursacht sein (<a href=\"https://bugs.php.net/bug.php?id=68644\" target=_blank>https://bugs.php.net/bug.php?id=68644</a>). Es wird empfohlen, eine �ltere oder eine neuere Version von PHP zu nutzen.  ";
$MESS["SC_T_RECURSION"] = "Gr��e des Stapelspeichers; pcre.recursion_limit";
$MESS["SC_HELP_CHECK_PCRE_RECURSION"] = "Der Parameter <i>pcre.recursion_limit</i> ist standardm��ig 100000. Wenn die Rekursion mehr Speicher verbraucht als die Stapelspeichergr��e  es  erm�glicht (gew�hnlich 8 MB), wird PHP bei komplexen regul�ren Ausdr�cken fehlschlagen und den Fehler <i>Segmentation fault</i> aufweisen.
Um das Limit der Stapelspeichergr��e zu deaktivieren, muss das Skript zur Apache-Ausf�hrung bearbeitet werden: <code>ulimit -s unlimited</code>
Bei FreeBSD m�ssen Sie PCRE mithilfe von der Option disable-stack-for-recursion erneut zusammenstellen.

Alternativ kann man den Wert von <i>pcre.recursion_limit</i> auf 1000 oder noch weniger reduzieren.
So wird das Fehlschlagen von PHP vermieden, aber es kann zu einem nicht immer korrekten Verhalten von Zeilenfunktionen f�hren: Z.B. k�nnen die Foren als erstes leere Beitr�ge anzeigen.
";
$MESS["SC_PCRE_CLEAN"] = "Lange Textzeilen k�nnen wegen Systemeinschr�nkungen eine nicht korrekte Verarbeitung verursachen.";
$MESS["SC_T_METHOD_EXISTS"] = "method_exists in der Zeile ausf�hren";
$MESS["SC_HELP_CHECK_METHOD_EXISTS"] = "Das Skript schl�gt fehl, wenn <i>method_exists</I> an einigen PHP-Versionen ausgef�hrt wird. Hier finden Sie n�here Informationen zu diesem Problem: <a href='http://bugs.php.net/bug.php?id=51425' target=_blank>http://bugs.php.net/bug.php?id=51425</a>
Als Probleml�sung wird vorgeschlagen, einen andere PHP-Version zu installieren.
";
$MESS["SC_HELP_CHECK_MYSQL_TABLE_STRUCTURE"] = "Die Installationspakete der Module enthalten immer Informationen �ber die Struktur der Datenbanktabellen, die sie nutzen. Bei Updates k�nnen die Tabellenstruktur und die Moduldateien (Skripts) ge�ndert werden.

Wenn die Modulskripts der aktuellen Tabellenstruktur nicht entsprechen, f�hrt das zu den Systemfehlern.

Es k�nnen neue Datenbankindexe geben, die zu den neuen Produktpaketen hinzugef�gt, aber nicht in die Updates eingeschlossen wurden. Das h�ngt damit zusammen, dass eine Aktualisierung des Systems inklusive Indexe sehr viel Zeit in Anspruch nimmt und oft Fehler verursacht.

Die Website�berpr�fung erstellt eine Diagnose f�r die <b>installierten</b> Module und erstellt und/oder aktualisiert fehlende Indexe und Felder, um so die Datenintegrit�t sicherzustellen. Trotzdem werden Sie das Protokoll manuell �berpr�fen, wenn ein Feldtyp ge�ndert wurde.
";
$MESS["ERR_MAX_INPUT_VARS"] = "Der Wert von max_input_vars muss #MIN# oder noch h�her sein. Der aktuelle Wert ist: #CURRENT#";
$MESS["SC_T_APACHE"] = "Web-Servermodule";
$MESS["SC_T_INSTALL_SCRIPTS"] = "Service-Skripts im Website-Root";
$MESS["ERR_OLD_VM"] = "Sie nutzen eine veraltete Version von Bitrix Environment. Installieren Sie bitte eine aktuelle Version, um etwaige Konfigurationsprobleme zu vermeiden.";
$MESS["ERR_DNS"] = "Sie haben eine veraltete Version von Bitrix Environment. Installieren Sie bitte die aktuellste Version, um etwaige Konfigurationsfehler zu vermeiden.";
$MESS["SC_ERR_NO_FIELD"] = "In der Tabelle #TABLE# fehlt das Feld #FIELD#";
$MESS["SC_ERR_NO_VALUE"] = "Es gibt keinen Systemeintrag #SQL# f�r Tabelle #TABLE#";
$MESS["SC_ERR_FIELD_DIFFERS"] = "Tabelle #TABLE#: Das Feld #FIELD# \"#CUR#\" entspricht nicht der Beschreibung \"#NEW#\"";
$MESS["SC_ERR_NO_INDEX"] = "In der Tabelle #TABLE# fehlt der Index #INDEX#";
$MESS["SC_ERR_NO_TABLE"] = "Die Tabelle #TABLE# existiert nicht.";
$MESS["SC_CHECK_TABLES_STRUCT_ERRORS"] = "Es gibt Fehler in der Datenbankstruktur (fehlende Tabellen: #NO_TABLES#, fehlende Felder: #NO_FIELDS#, abweichende Felder: #DIFF_FIELDS#, fehlende Indexe: #NO_INDEXES#). Gesamt Fehler: #VAL#. #VAL1# k�nnen sofort behoben werden.";
$MESS["SC_CHECK_TABLES_STRUCT_ERRORS_FIX"] = "Die Felder wurden behoben, aber einige Felder (#VAL#) haben andere Typen. Sie werden es manuell beheben m�ssen, indem Sie das Website-Protokoll �berpr�fen.";
$MESS["SC_HELP_CHECK_PERF"] = "Hier wird die Server-Performance mithilfe des <a href=\"http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=20&CHAPTER_ID=04955\">Performance-Monitors</a> gepr�ft.

Es wird die Anzahl leerer Seiten angezeigt, welche der Server pro Sekunde ausgeben kann. Das ist ein Kehrwert f�r die Zeit der Generierung einer Seite, welche nur die Verbindung zum Produktkernel enth�lt.

Die <a href=\"http://www.bitrix.de/products/virtual_appliance/\">Bitrix Virtual Appliance</a> wird mit ca. 30 Einheiten bewertet.

Wenn bei einer niedrigeren Serverlast eine niedrige Bewertung erhalten wurde, zeugt das von den etwaigen Konfigurationsfehlern. Wenn die Bewertung erst unter hoher Belastung niedrig wird, kann das von etwaigen Hardwaremangeln zeugen.
";
$MESS["SC_HELP_CHECK_CA_FILE"] = "Der Test versucht eine Verbindung mit der Website www.bitrix.de herzustellen. 

Das ist erforderlich f�r die t�gliche Arbeit mit Bitrix Cloud Services &quot; Bitrix Cloud Services &quot; (CDN, Sicherungskopien, Sicherheits-Scanner usw.), wenn die Informationen �ber Disk Quota oder �ber den aktuellen Service-Status aktualisiert werden. Die Nutzerdaten werden dabei an unseren Server nicht �bertragen.

Mit diesem Test kann au�erdem eine Liste der Zertifizierungsstellen von unserer Website geladen werden: Diese Liste ist dann f�r den n�chsten Test zur Pr�fung der G�ltigkeit des SSL-Zertifikats einer aktuellen Website erforderlich.
";
$MESS["SC_HELP_CHECK_SOCKET_SSL"] = "Die verschl�sselte Verbindung zum Server erfolgt via <a href=\"http://de.wikipedia.org/wiki/HTTPS\">HTTPS</a>. Damit die Verbindung auch wirklich sicher ist, muss man �ber ein g�ltiges SSL-Zertifikat verf�gen.

Die G�ltigkeit des Zertifikats setzt voraus, dass es von der Zertifizierungsstelle �berpr�ft wurde und dem aktuellen Server auch wirklich geh�rt. Ein solches Zertifikat kann man �ber eigenen Hosting-Provider kaufen.

Wenn die Arbeit mit dem Intranet via HTTPS l�uft und das Zertifikat dabei nicht best�tigt ist, k�nnen Probleme mit externer Software auftreten, z.B. bei der Anbindung der Netzwerklaufwerke via WebDav oder bei der Integration mit Outlook.
";
$MESS["SC_HELP_CHECK_PULL_STREAM"] = "Die Unterst�tzung seitens des Servers ist erforderlich f�r eine korrekte Arbeitsweise des Moduls <a href=\"http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=26&LESSON_ID=5144\">Push and Pull</a>.

Dieses Modul erm�glicht eine Sofortzustellung von Nachrichten via Web-Messenger und Mobile Anwendung sowie eine sofortige Aktualisierung des Activity Streams.

Die <a href=\"http://www.bitrix.de/products/virtual_appliance /\">Bitrix Virtual Appliance</a> ab Version 4.2 unterst�tzt dieses Modul im vollen Umfang.
";
$MESS["SC_HELP_CHECK_PULL_COMMENTS"] = "Damit die Kommentare im Activity Stream gleich f�r alle Leser verf�gbar werden, muss das Modul Push and Pull zus�tzlich konfiguriert werden. Daf�r muss auf Ihrem Nginx-Server das Modul push-stream-module installiert, und dann in den Moduleinstellungen des Moduls <a href=\"http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=26&LESSON_ID=5144\">Push and Pull</a> aktiviert werden.

Die <a href=\"http://www.bitrixsoft.com/products/virtual_appliance/index.php\">Bitrix Virtual Appliance</a> unterst�tzt diese Funktion ab Version 4.2.
";
$MESS["SC_HELP_CHECK_CONNECT_MAIL"] = "Um Benachrichtigungen �ber neue E-Mail-Nachrichten direkt aus dem Intranet zu bekommen, muss der Nutzer die Parameter seiner Mailbox-Verbindung in seinem Intranet-Profil angeben.";
$MESS["SC_HELP_CHECK_SOCNET"] = "Um die Nachrichten aus den sozialen Netzwerken zu bekommen, muss das Modul <a href=\"http://www.bitrixsoft.com/company/blog/news/integration-with-social-networks.php\">Soziale Services</a> konfiguriert werden, und zwar so, dass f�r jeden Service, der genutzt wird, ein Authentifizierungsschl�ssel angegeben werden soll.";
$MESS["SC_HELP_CHECK_REST"] = "F�r Integration mit externen Apps und einigen Apps vom Marketplace ist das Modul \"rest\" erforderlich. Sie k�nnen Ihre Apps zu Bitrix24 hinzuf�gen, folgen Sie daf�r diesen <a href=\"https://dev.1c-bitrix.ru/learning/course/?COURSE_ID=43&LESSON_ID=3568\" target=\"_blank\">Richtlinien</a>.";
$MESS["SC_HELP_CHECK_EXTRANET"] = "Damit das Modul <a href=\"http://www.bitrixsoft.com/products/intranet/features/collaboration/extranet.php\">Extranet</a> funktioniert, muss Ihr Intranet von au�en via Internet erreichbar sein.

Wenn Sie die Funktionen, die dieses Modul anbietet, nicht ben�tigen, k�nnen Sie es einfach <a href=\"/bitrix/admin/module_admin.php\">deinstallieren</a>.
";
$MESS["SC_HELP_CHECK_WEBDAV"] = "<a href=\"http://en.wikipedia.org/wiki/WebDAV\">WebDAV</a> ist ein Protokoll, das einem Nutzer erlaubt, Dokumente in Microsoft Office direkt im Intranet zu �ffnen, zu bearbeiten und zu speichern, ohne sie also herunterladen bzw. hochzuladen zu m�ssen. Eine erforderliche Voraussetzung ist dabei, dass der Server, auf welchem das Intranet installiert ist, die WebDAV-Anfragen an PHP Scripts genauso sendet, wie er sie empf�ngt, also ohne �nderungen. Blockiert der Server diese Anfragen, ist die direkte Bearbeitung nicht m�glich.

Au�erdem kann auch auf der Client-Seite eine zus�tzliche Konfiguration <a href=\"http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=27&LESSON_ID=1466#office\">erforderlich sein</a>, um direkte Bearbeitung zu unterst�tzen, und es besteht keine M�glichkeit, diese Konfiguration per Fernzugriff zu pr�fen.
";
$MESS["SC_HELP_CHECK_AD"] = "Wenn in Ihrem lokalen Netzwerk ein Windows AD- oder LDAP-Server eingestellt ist, sollte �berpr�ft werden, ob AD <a href=\"http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=20&CHAPTER_ID=04264\">korrekt konfiguriert ist</a>.

Diese Funktion erfordert, dass das PHP ldap-Modul installiert ist.
";
$MESS["SC_HELP_CHECK_NTLM"] = "Die <a href=\"http://en.wikipedia.org/wiki/Single_sign-on\">Single sign-on</a> Authentifizierung erfordert, dass der Web-Server ganz bestimmt konfiguriert ist und die NTLM-Authentifizierung ist im Intranet aktiviert und entsprechend konfiguriert.

Einstellung von NTLM auf Linux ist keine einfache Aufgabe, aber die <a href=\"http://www.bitrixsoft.com/products/virtual_appliance/\">Bitrix Virtual Appliance</a> ab Version 4.2. enth�lt auch diese Funktion und sie muss lediglich aktiviert werden.
";
$MESS["SC_HELP_CHECK_TURN"] = "Damit die Videoanrufe funktionieren, muss zwischen Browsern entsprechender Nutzer Verbindung hergestellt werden. Wenn die Nutzer dabei von verschiedenen Netzwerken aus arbeiten � z.B. sie sitzen in verschiedenen B�ros, sodass eine direkte Verbindung nicht m�glich ist, wird ein spezieller TURN-Server ben�tigt, um Verbindung herzustellen.

Bitrix Inc. bietet kostenlos einen vorkonfigurierten TURN-Server unter turn.calls.bitrix24.com. 

Alternativ k�nnen Sie Ihren eigenen Server konfigurieren und die Server-URL in den Einstellungen des Moduls Web Messenger angeben.
";
$MESS["SC_HELP_CHECK_PUSH_BITRIX"] = "Das Modul <a href=\"http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=26&LESSON_ID=5144\">Push and Pull</a> erm�glicht eine Sofort�bertragung von Nachrichten mit der Technologie Pull und das Senden von Benachrichtigungen an mobile Ger�te mit der Technologie Push via <a href=\"http://www.bitrix.de/products/intranet/features/bitrixmobile.php\">Bitrix Mobile App</a>.";
$MESS["SC_HELP_CHECK_ACCESS_MOBILE"] = "Die mobile Anwendung erfordert, dass Ihr Intranet von au�en via Internet erreichbar ist.

Der Test verwendet einen speziellen Server auf checker.internal.bitrix24.com, der versucht eine Verbindung mit Ihrem Intranet anhand des URL herzustellen, welche im Web-Browser angegeben ist. W�hrend der Verbindung mit dem Fernserver werden keine Nutzerdaten �bertragen.

Der Instant Messenger erfordert, dass der Leseport von Nginx's push-stream-module verbunden werden kann. Die Portnummer kann in den Einstellungen des Moduls <a href=\"http://www.bitrixsoft.com/support/training/course/index.php?COURSE_ID=26&LESSON_ID=5144\">Push and Pull</a> gefunden werden.
";
$MESS["SC_HELP_CHECK_FAST_DOWNLOAD"] = "Schnelle Datei�bertragung benutzt eine interne Umleitung <a href=\"http://wiki.nginx.org/X-accel\">nginx</a>. In diesem Fall erfolgt die �berpr�fung des Zugriffs auf die Datei mithilfe von PHP, und die �bertragung selbst mithilfe von nginx. 

Die PHP-Ressourcen werden f�r die Bearbeitung einer n�chsten Anfrage freigemacht. Das erh�ht die Intranet-Performance und die Geschwindigkeit der Datei�bertragung �ber Bitrix24.Drive, optimiert die Arbeit mit der Dokumentenbibliothek und erm�glicht eine schnellere Datei�bertragung aus dem Activity Stream.

In den Einstellungen vom <a href=\"/bitrix/admin/settings.php?mid=main\">Hauptmodul</a> muss eine entsprechende Option aktiviert werden. Die <a href=\"http:// www.bitrix.de/products/virtual_appliance/\">Bitrix Virtual Appliance</a> unterst�tzt diese Option standardm��ig.
";
$MESS["SC_HELP_CHECK_COMPRESSION"] = "Eine html-Komprimierung ist erforderlich, um die Zeit der �bertragung sowie die allgemeine Wartezeit zum �ffnen von Seiten zu reduzieren.

Um die Serverlast zu reduzieren, muss die Komprimierung mit dem speziellen Modul des Web-Servers erfolgen. 

Ist diese M�glichkeit nicht verf�gbar, wird daf�r das Bitrix Modul Komprimierung eingesetzt, im anderen Fall muss das Modul Komprimierung  werden <a href=\"/bitrix/admin/module_admin.php\">nicht installiert</a> sein.
";
$MESS["SC_HELP_CHECK_ACCESS_DOCS"] = "Um Dokumente via Google Docs oder Microsoft Office Online anzeigen oder bearbeiten zu k�nnen, wird eine spezielle von extern aus erreichbare URL erstellt und an die Services gesendet, welche f�r das Dokument benutzt werden. Die URL ist einmalig und wird sofort ung�ltig, sobald das Dokument geschlossen wird.

Diese Funktion erfordert, dass Ihr Intranet von au�en via Internet erreichbar ist.
";
$MESS["SC_HELP_CHECK_SEARCH"] = "Das System kann nach Texten in Dokumenten von Open XML Format (eingef�hrt in Microsoft Office 2007) suchen. Damit auch andere Datei-Formate unterst�tzt werden k�nnen, m�ssen Pfade zu entsprechenden Anwendungen <a href=\"/bitrix/admin/settings.php?mid=intranet\">in den Einstellungen des Moduls Intranet angegeben werden</a>. Im anderen Fall wird das System lediglich nach Namen suchen.

Die <a href=\"http://www.1c-bitrix.ru/products/vmbitrix/index.php\">Bitrix Virtual Appliance</a> unterst�tzt das standardm��ig.
";
$MESS["MAIN_SC_AGENTS_CRON"] = "Agentenausf�hrung �ber Cron";
$MESS["MAIN_SC_PERF_TEST"] = "Bewertung der Server-Performance";
$MESS["MAIN_SC_COMP_DISABLED"] = "Die Komprimierung wird vom Server nicht unterst�tzt, es muss daf�r das php-Modul Komprimierung eingesetzt werden.";
$MESS["MAIN_SC_COMP_DISABLED_MOD"] = "Die Komprimierung wird vom Server nicht unterst�tzt, das Modul Komprimierung ist deaktiviert.";
$MESS["MAIN_SC_ENABLED"] = "Die Komprimierung wird vom Server unterst�tzt, das Modul Komprimierung muss  deinstalliert werden.";
$MESS["MAIN_SC_ENABLED_MOD"] = "Die Komprimierung erfolgt durch das Server-Modul.";
$MESS["MAIN_SC_TEST_SSL1"] = "Sichere HTTPS-Verbindung wurde hergestellt, die �berpr�fung der G�ltigkeit vom SSL-Zertifikat ist fehgeschlagen, weil die Liste der Zertifizierungsstellen von der Website &quot;Bitrix&quot; nicht heruntergeladen wurde.";
$MESS["MAIN_SC_TEST_SSL_WARN"] = "Eine sichere Verbindung konnte nicht hergestellt werden. Es k�nnen Probleme bei Integration mit externen Anwendungen entstehen.";
$MESS["MAIN_SC_SSL_NOT_VALID"] = "Der Server verf�gt �ber ein nicht g�ltiges SSL-Zertifikat.";
$MESS["MAIN_SC_PATH_PUB"] = "Der Pfad f�r die Ver�ffentlichung von Nachrichten in den Einstellungen des Moduls Push and Pull ist nicht korrekt.";
$MESS["MAIN_SC_PATH_SUB"] = "Die Lese-URL der Nachricht in den Einstellungen des Moduls Push and Pull ist nicht korrekt.";
$MESS["MAIN_SC_STREAM_DISABLED"] = "In den Einstellungen des Moduls Push and Pull ist die Option nginx-push-stream-module deaktiviert. ";
$MESS["MAIN_NO_PULL"] = "Das Modul Push and Pull ist nicht installiert.";
$MESS["MAIN_NO_PULL_MODULE"] = "Das Modul Push and Pull ist nicht installiert. Die PUSH-Benachrichtigungen werden an mobile Ger�te nicht gesendet.";
$MESS["MAIN_NO_OPTION_PULL"] = "Im Modul Push and Pull ist die Option zum Senden von PUSH-Benachrichtigungen nicht aktiviert. Die Benachrichtigungen werden an mobile Ger�te nicht gesendet.";
$MESS["MAIN_WRONG_ANSWER_PULL"] = "Eine unbekannte Antwort vom PUSH-Server.";
$MESS["MAIN_TMP_FILE_ERROR"] = "Eine tempor�re Datei f�r die Testzwecke konnte nicht erstellt werden.";
$MESS["MAIN_FAST_DOWNLOAD_SUPPORT"] = "Die Unterst�tzung f�r schnelle Datei�bertragung via nginx ist verf�gbar, aber die entsprechende Option ist in den Einstellungen des Hauptmoduls deaktiviert.";
$MESS["MAIN_FAST_DOWNLOAD_ERROR"] = "Schnelles Herunterladen von Dateien auf der Basis von nginx ist nicht verf�gbar, die entsprechende Option ist in den Einstellungen des Hauptmoduls aktiviert.";
$MESS["MAIN_PERF_VERY_LOW"] = "Unerlaubt niedrig";
$MESS["MAIN_PERF_LOW"] = "Niedrig";
$MESS["MAIN_PERF_MID"] = "Durchschnittlich";
$MESS["MAIN_PERF_HIGH"] = "Hoch";
$MESS["MAIN_PAGES_PER_SECOND"] = "Seiten pro Sekunde";
$MESS["MAIN_BX_CRONTAB_DEFINED"] = "Die Konstante BX_CRONTAB ist definiert, sie kann nur in den Scripts definiert werden, welche �ber cron funktionieren.";
$MESS["MAIN_CRON_NO_START"] = "cron_events.php ist nicht konfiguriert, um auf cron zu laufen; die letzte Ausf�hrung des Agenten liegt mehr als 24 Stunden zur�ck.";
$MESS["MAIN_AGENTS_HITS"] = "Die Agenten laufen �ber Hits, es wird empfohlen, die Agenten �ber cron auszuf�hren.";
$MESS["SC_GR_EXTENDED"] = "Zus�tzliche Funktionen";
$MESS["SC_GR_MYSQL"] = "Datenbanktest";
$MESS["SC_GR_FIX"] = "Fehler der Datenbank beheben";
$MESS["SC_WARN"] = "nicht konfiguriert";
$MESS["SC_PORTAL_WORK"] = "Intranet-Funktionsf�higkeit";
$MESS["SC_PORTAL_WORK_DESC"] = "Pr�fung der Intranet-Funktionsf�higkeit";
$MESS["SC_FULL_CP_TEST"] = "Vollst�ndiger Systemtest";
$MESS["SC_SYSTEM_TEST"] = "Systemtest";
$MESS["SC_ERRORS_NOT_FOUND"] = "Keine&nbsp;Fehler&nbsp;entdeckt";
$MESS["SC_ERRORS_FOUND"] = "Es&nbsp;wurden&nbsp;Fehler entdeckt";
$MESS["SC_WARNINGS_FOUND"] = "Es wurden keine Fehler entdeckt, aber es gibt Warnungen.";
$MESS["SC_TESTING1"] = "Wird getestet...";
$MESS["SC_HELP"] = "Hilfe.";
$MESS["SC_TEST_START"] = "Test starten";
$MESS["MAIN_SC_GENERAL"] = "Allgemeine Intranet-Funktionen";
$MESS["MAIN_SC_GENERAL_SITE"] = "Allgemeine Website-Funktionen";
$MESS["MAIN_SC_BUSINESS"] = "Business-Funktionen des Intranets";
$MESS["MAIN_SC_REAL_TIME"] = "Kommunikation in Echtzeit und Videoanrufe";
$MESS["MAIN_SC_EXTERNAL_CALLS"] = "Externe Videoanrufe";
$MESS["MAIN_SC_WARNINGS"] = "Mobile Benachrichtigungen";
$MESS["MAIN_SC_FAST_FILES_TEST"] = "Schneller Zugriff und Dateien und Dokumente";
$MESS["MAIN_SC_COMPRESSION_TEST"] = "Komprimierung und Beschleunigung der Seite";
$MESS["MAIN_SC_MAIL_TEST"] = "E-Mail-Benachrichtigungen";
$MESS["MAIN_SC_CLOUD_TEST"] = "Zugriff auf Bitrix Cloud Services";
$MESS["MAIN_SC_EXTERNAL_APPS_TEST"] = "Anwendungen (MS Office, Outlook, Exchange) �ber sichere Verbindung";
$MESS["MAIN_SC_TEST_IS_INCORRECT"] = "Korrekte Ergebnisse sind nicht m�glich, weil der Test fehlgeschlagen ist.";
$MESS["MAIN_SC_SOME_WARNING"] = "Warnung";
$MESS["MAIN_SC_MCRYPT"] = "Verschl�sselungsfunktionen";
$MESS["MAIN_SC_ALL_MODULES"] = "Alle erforderlichen Module sind installiert.";
$MESS["MAIN_SC_ERROR_PRECISION"] = "Der Wert des Parameters \"precision\" ist nicht g�ltig.";
$MESS["MAIN_SC_CANT_CHANGE"] = "Es ist nicht m�glich, den Wert von pcre.backtrack_limit �ber ini_set zu �ndern.";
$MESS["MAIN_SC_CORRECT_SETTINGS"] = "Einstellungen sind korrekt";
$MESS["MAIN_IS_CORRECT"] = "Korrekt";
$MESS["MAIN_SC_NO_ACCESS"] = "Der Server von Bitrix, Inc. ist nicht verf�gbar. Aktualisierungen und Bitrix Cloud Services sind nicht verf�gbar.";
$MESS["MAIN_SC_ABS"] = "Keine";
$MESS["MAIN_SC_CORRECT"] = "Korrekt";
$MESS["MAIN_SC_NO_IM"] = "Das Modul Web Messenger ist nicht installiert.";
$MESS["MAIN_SC_AVAIL"] = "Verf�gbar";
$MESS["MAIN_SC_NOT_AVAIL"] = "Nicht verf�gbar";
$MESS["MAIN_SC_NOT_SUPPORTED"] = "Der Server unterst�tzt nicht diese Funktion.";
$MESS["MAIN_SC_NO_CONFLICT"] = "Es wurden keine Konflikte festgestellt.";
$MESS["MAIN_SC_ABSENT_ALL"] = "Keine";
$MESS["MAIN_SC_REQUIRED_MODS_DESC"] = "Stellt sicher, dass alle erforderlichen Module installiert und alle wichtigen Einstellungen korrekt sind. Im anderen Fall kann eine reibungslose Funktionsf�higkeit des Intranets nicht gew�hrleistet werden.";
$MESS["MAIN_SC_CORRECT_DESC"] = "Intranet erfordert eine spezielle Konfiguration der Server-Umgebung. Die <a href=\"http://www.bitrix.de/products/virtual_appliance/\" target=\"_blank\">Bitrix Virtual Appliance</a> ist standardm��ig vorkonfiguriert. Einige Funktionen k�nnen nicht verf�gbar sein, wenn erforderliche Parameter nicht konfiguriert sind.";
$MESS["MAIN_SC_GOT_ERRORS"] = "Die Website enth�lt Fehler. <a href=\"#LINK#\">Pr�fen und beheben</a>";
$MESS["MAIN_SC_SITE_GOT_ERRORS"] = "Die Website hat Fehler. <a href=\"#LINK#\">Pr�fen und beheben.</a>";
$MESS["MAIN_SC_FULL_TEST_DESC"] = "Starten Sie die komplette System�berpr�fung, um Engp�sse festzustellen und Fehler zu beheben oder Probleme in Zukunft zu vermeiden. Kurzbeschreibungen f�r jeden Test werden Ihnen helfen, etwaige Probleme schnell zu finden und zu beseitigen.";
$MESS["MAIN_SC_SYSTEST_LOG"] = "Protokoll der System�berpr�fung";
$MESS["MAIN_SC_TEST_RESULT"] = "Testergebnisse:";
$MESS["MAIN_SC_ALL_FUNCS_TESTED"] = "Alle Intranet-Funktionen wurden �berpr�ft und sind in Ordnung.";
$MESS["MAIN_SC_FUNC_WORKS_FINE"] = "Die Funktion ist in Ordnung.";
$MESS["MAIN_SC_FUNC_WORKS_PARTIAL"] = "Diese Funktion kann Probleme haben, Sie sollten diese finden und beheben.";
$MESS["MAIN_SC_FUNC_WORKS_WRONG"] = "Die Funktion ist nicht in Ordnung, beheben Sie Fehler.";
$MESS["MAIN_SC_TEST_CHAT"] = "Instant Messenger in Echtzeit";
$MESS["MAIN_SC_TEST_COMMENTS"] = "Kommentare live";
$MESS["MAIN_SC_TEST_VIDEO"] = "Video-Anrufe";
$MESS["MAIN_SC_TEST_MOBILE"] = "Bitrix24 Mobile App";
$MESS["MAIN_SC_TEST_MAIL_PUSH"] = "E-Mail-Nachrichten an den Activity Stream weiterleiten";
$MESS["MAIN_SC_TEST_PUSH"] = "Benachrichtigungen auf mobile Ger�te (Push-Benachrichtigungen)";
$MESS["MAIN_SC_TEST_DOCS"] = "Dokumente in Google Docs und Microsoft Office Online bearbeiten";
$MESS["MAIN_SC_TEST_FAST_FILES"] = "Bitrix24.Drive. Schnelle Dateiverwaltung";
$MESS["MAIN_SC_TEST_SEARCH_CONTENTS"] = "Dokumentinhalte suchen";
$MESS["MAIN_SC_TEST_MAIL_INTEGRATION"] = "Mail-Integration innerhalb des Unternehmens";
$MESS["MAIN_SC_TEST_SOCNET_INTEGRATION"] = "Integration mit sozialen Services";
$MESS["MAIN_SC_TEST_REST"] = "Nutzung von REST API";
$MESS["MAIN_SC_EXTRANET_ACCESS"] = "Externer Zugriff auf Extranet";
$MESS["MAIN_SC_WINDOWS_ENV"] = "Integration mit Windows-Environment";
$MESS["MAIN_SC_DOCS_EDIT_MS_OFFICE"] = "Dokumente in Microsoft Office bearbeiten";
$MESS["MAIN_SC_TEST_LDAP"] = "Integration mit Active Directory";
$MESS["MAIN_SC_TEST_NTLM"] = "Windows NTLM-Authentifizierung";
$MESS["MAIN_SC_PERFORM"] = "Performance";
$MESS["MAIN_SC_MAIL_IS_NOT_INSTALLED"] = "Das Modul E-Mail ist nicht installiert.";
$MESS["MAIN_SC_MAIL_INTEGRATION"] = "Integration mit den externen E-Mail-Account ist n Ordnung, aber kein Nutzer hat die Integrationseinstellungen vorgenommen.";
$MESS["MAIN_SC_NO_SOCIAL_MODULE"] = "Das Modul Soziale Netzwerke ist nicht installiert.";
$MESS["MAIN_SC_NO_REST_MODULE"] = "Das Modul Rest ist nicht installiert.";
$MESS["MAIN_SC_NO_SOCIAL_SERVICES"] = "In den Einstellungen des Moduls Soziale Netzwerke sind keine sozialen Services konfiguriert.";
$MESS["MAIN_SC_NO_SOCIAL_SERVICES_24NET"] = "Die Integration mit Bitrix24.net ist in den Einstellungen des Moduls Soziale Services nicht konfiguriert.";
$MESS["MAIN_SC_NO_LDAP_MODULE"] = "Das Modul LDAP ist nicht installiert.";
$MESS["MAIN_SC_NO_LDAP_INTEGRATION"] = "Die Integration mit dem AD-Server ist nicht eingestellt.";
$MESS["MAIN_SC_OPTION_SWITCHED_OFF"] = "De NTLM-Authentifizierung ist in den Einstellungen des LDAP-Moduls aktiviert.";
$MESS["MAIN_SC_NTLM_SUCCESS"] = "Die NTLM-Authentifizierung ist in Ordnung, aktueller Nutzer: ";
$MESS["MAIN_SC_NO_NTLM"] = "Aktuelle Verbindung verwendet nicht die NTLM-Authentifizierung";
$MESS["MAIN_SC_NO_PUSH_STREAM_CONNECTION"] = "Die Verbindung mit dem Modul nginx-push-stream kann nicht hergestellt werden, um Sofortnachrichten zu senden";
$MESS["MAIN_SC_NO_SUB_CONNECTION"] = "Die Verbindung mit dem Modul nginx-push-stream kann nicht hergestellt werden, um Sofortnachrichten zu lesen";
$MESS["MAIN_SC_PUSH_INCORRECT"] = "Das Modul nginx-push-stream funktioniert nicht korrekt.";
$MESS["MAIN_SC_NO_PUSH_STREAM"] = "Das Modul nginx-push-stream wird aufgefordert, Kommentare in Echtzeit anzuzeigen, ist aber nicht korrekt konfiguriert.";
$MESS["MAIN_SC_NO_PUSH_STREAM_VIDEO"] = "Das Modul nginx-push-stream wird aufgefordert, Videoanrufe durchzuf�hren, ist aber nicht korrekt konfiguriert.";
$MESS["MAIN_SC_NO_EXTERNAL_ACCESS_MOB"] = "Diese Funktion ist nicht verf�gbar, weil das Intranet von au�en via mobile Anwendung nicht erreichbar ist.";
$MESS["MAIN_SC_NO_EXTERNAL_ACCESS_"] = "Diese Funktion ist nicht verf�gbar, weil das Intranet von au�en nicht erreichbar ist.";
$MESS["MAIN_SC_NO_EXTRANET_CONNECT"] = "Das Extranet funktioniert nicht korrekt, weil das Intranet von au�en via Internet nicht erreichbar ist.";
$MESS["MAIN_SC_NO_WEBDAV_MODULE"] = "Das Modul der Dokumentenbibliothek ist nicht installiert.";
$MESS["MAIN_SC_METHOD_NOT_SUP"] = "Der Server unterst�tzt nicht die Methode #METHOD#.";
$MESS["MAIN_SC_SEARCH_INCORRECT"] = "Die Indexierung der Dokumentinhalte funktioniert nicht korrekt.";
$MESS["MAIN_SC_NO_CONNECTTO"] = "Verbindung zu #HOST# kann nicht hergestellt werden";
$MESS["MAIN_SC_UNKNOWN_ANSWER"] = "Unbekannte Antwort von #HOST#";
$MESS["MAIN_SC_WARN_EXPAND_SESSION"] = "Wenn das Modul Instant Messenger installiert ist, muss die Option der Sitzungsverl�ngerung, wenn der Nutzer im Browser aktiv ist, in den <a href='/bitrix/admin/settings.php?mid=main' target=_blank>Einstellungen des Hauptmoduls</a> deaktiviert werden, um die Serverbelastung zu reduzieren.";
$MESS["MAIN_SC_NO_EXTERNAL_CONNECT_WARN"] = "Die Verbindung mit dem Intranet von au�en ist nicht m�glich. Die mobile Anwendung wird nicht funktionieren.";
$MESS["MAIN_SC_EXTERNAL_ANSWER_INCORRECT"] = "Die Verbindung mit dem Intranet von au�en war erfolgreich, aber der Server gibt einen nicht korrekten Status an.";
$MESS["MAIN_SC_NO_PULL_EXTERNAL"] = "Die Verbindung mit dem Intranet von au�en war erfolgreich, aber der Port von nginx-push-stream zum Lesen von Nachrichten ist nicht verf�gbar. Mobile Sofortnachrichten werden nicht verf�gbar sein.";
$MESS["MAIN_CATDOC_WARN"] = "Schlechte Version von catdoc: #VERSION#<br>
Details: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=679877<br>
Installieren Sie eine �ltere catdoc Version, oder eine neuere mit Fehlerbehebungen.";
$MESS["MAIN_SC_MBSTRING_SETTIGNS_DIFFER"] = "Die Einstellungen von mbstring in <i>/bitrix/.settings.php</i> (utf_mode) und <i>/bitrix/php_interface/dbconn.php</i> (BX_UTF) unterscheiden sich.";
$MESS["SC_ERR_NO_SETTINGS"] = "Die Konfigurationsdatei /bitrix/.settings.php wurde nicht gefunden";
$MESS["SC_FIX_MBSTRING"] = "Konfiguration korrigieren";
$MESS["SC_FIX_MBSTRING_CONFIRM"] = "Achtung!

Die Konfigurationsdateien werden ge�ndert. Wird diese Operation fehlschlagen, werden Sie Ihre Website nur �ber das Control Panel des Webhostings wiederherstellen k�nnen.

Fortfahren?
";
?>