<?
$MESS["GEOIP_LIST_TITLE"] = "Handler der Geolokalisierung";
$MESS["GEOIP_LIST_DELETE_CONFIRM"] = "M�chten Sie die Handler-Einstellungen wirklich l�schen?";
$MESS["GEOIP_LIST_ADD_HANDLER"] = "Hinzuf�gen";
$MESS["GEOIP_LIST_ADD_HANDLER_T"] = "Handler-Einstellungen hinzuf�gen";
$MESS["GEOIP_LIST_F_ID"] = "ID";
$MESS["GEOIP_LIST_F_TITLE"] = "Name";
$MESS["GEOIP_LIST_F_DESCRIPTION"] = "Beschreibung";
$MESS["GEOIP_LIST_F_ACTIVE"] = "Aktiv";
$MESS["GEOIP_LIST_F_IS_INSTALLED"] = "Definiert";
$MESS["GEOIP_LIST_F_SORT"] = "Sortierung";
$MESS["GEOIP_LIST_F_LANG"] = "Sprachen";
$MESS["GEOIP_LIST_F_CLASS"] = "Klasse";
$MESS["GEOIP_LIST_Y"] = "Ja";
$MESS["GEOIP_LIST_N"] = "Nein";
$MESS["GEOIP_LIST_DELETE"] = "L�schen";
$MESS["GEOIP_LIST_EDIT"] = "Bearbeiten";
?>