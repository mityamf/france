<?
$MESS["SMILE_TITLE"] = "Galerien mit Emoticons";
$MESS["SMILE_DEL_CONF"] = "M�chten Sie diese Galerie wirklich l�schen? Damit werden alle Pakete mit Emoticons gel�scht, welche in dieser Galerie enthalten sind.";
$MESS["SMILE_NAV"] = "Galerien";
$MESS["SMILE_ID"] = "ID";
$MESS["SMILE_SORT"] = "Sortierung";
$MESS["SMILE_NAME"] = "Name";
$MESS["SMILE_GALLERY_NAME"] = "Galerie #ID#";
$MESS["SMILE_STRING_ID"] = "Galerie-Code";
$MESS["SMILE_SMILE_COUNT"] = "Emoticons";
$MESS["SMILE_DELETE_DESCR"] = "L�schen";
$MESS["SMILE_EDIT"] = "Bearbeiten";
$MESS["SMILE_EDIT_DESCR"] = "Bearbeiten";
$MESS["ERROR_DEL_SMILE"] = "Fehler beim L�schen der Galerie.";
$MESS["SMILE_BTN_ADD_NEW"] = "Neue Galerie";
$MESS["SMILE_BTN_ADD_NEW_ALT"] = "Hier klicken, um eine neue Galerie zu erstellen";
?>