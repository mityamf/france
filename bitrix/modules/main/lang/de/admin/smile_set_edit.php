<?
$MESS["SMILE_EDIT_RECORD"] = "Satz bearbeiten";
$MESS["SMILE_NEW_RECORD"] = "Satz hinzuf�gen";
$MESS["ERROR_EDIT_SMILE"] = "Fehler beim Bearbeiten vom Satz";
$MESS["ERROR_ADD_SMILE"] = "Fehler beim Hinzuf�gen vom Satz";
$MESS["ERROR_BAD_SESSID"] = "Ihre Sitzung ist abgelaufen. Versuchen Sie bitte erneut.";
$MESS["SMILE_SORT"] = "Sortierung";
$MESS["SMILE_STRING_ID"] = "Satz-ID";
$MESS["SMILE_SMILE_EXAMPLE"] = "Smileys im Satz";
$MESS["SMILE_SMILE_EXAMPLE_LINK"] = "Smileys anzeigen";
$MESS["SMILE_IMAGE_NAME"] = "Satz-Name";
$MESS["SMILE_IMAGE_NAME_EN"] = "Englisch";
$MESS["SMILE_IMAGE_NAME_DE"] = "Deutsch";
$MESS["SMILE_IMAGE_NAME_RU"] = "Russisch";
$MESS["SMILE_BTN_BACK"] = "Smiley-S�tze";
$MESS["SMILE_BTN_NEW"] = "Einen neuen hinzuf�gen";
$MESS["SMILE_BTN_DELETE"] = "L�schen";
$MESS["SMILE_BTN_DELETE_CONFIRM"] = "M�chten Sie diesen Satz wirklich l�schen? Dadurch werden auch alle Smileys gel�scht, die in diesem Satz enthalten sind.";
$MESS["SMILE_TAB_SMILE"] = "Parameter";
$MESS["SMILE_TAB_SMILE_DESCR"] = "Satz-Parameter";
$MESS["SMILE_IMPORT"] = "Smileys in diesen Satz importieren, sobald er erstellt wird";
$MESS["SMILE_IMAGE_PARAMS"] = "Zus�tzliche Parameter";
?>