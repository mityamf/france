<?
$MESS["SMILE_EDIT_RECORD"] = "Galerie bearbeiten";
$MESS["SMILE_NEW_RECORD"] = "Galerie hinzuf�gen";
$MESS["ERROR_EDIT_SMILE"] = "Fehler bei Aktualisierung der Galerie";
$MESS["ERROR_ADD_SMILE"] = "Fehler beim Hinzuf�gen einer neuen Galerie";
$MESS["ERROR_BAD_SESSID"] = "Ihre Sitzung ist abgelaufen. Versuchen Sie bitte erneut.";
$MESS["SMILE_SORT"] = "Sortierung";
$MESS["SMILE_STRING_ID"] = "Galerie-Code";
$MESS["SMILE_SMILE_EXAMPLE"] = "Emoticons in Paketen";
$MESS["SMILE_IMAGE_NAME"] = "Galeriename";
$MESS["SMILE_IMAGE_NAME_EN"] = "Englisch";
$MESS["SMILE_IMAGE_NAME_DE"] = "Deutsch";
$MESS["SMILE_IMAGE_NAME_RU"] = "Russisch";
$MESS["SMILE_IMAGE_PARAMS"] = "Zus�tzliche Parameter";
$MESS["SMILE_BTN_BACK"] = "Galerien mit Emoticons";
$MESS["SMILE_BTN_NEW"] = "Neue hinzuf�gen";
$MESS["SMILE_BTN_DELETE"] = "L�schen";
$MESS["SMILE_BTN_DELETE_CONFIRM"] = "M�chten Sie diese Galerie wirklich l�schen? Damit werden alle Pakete mit Emoticons gel�scht, welche in dieser Galerie enthalten sind.";
$MESS["SMILE_TAB_SMILE"] = "Parameter";
$MESS["SMILE_TAB_SMILE_DESCR"] = "Galerie-Parameter";
$MESS["SMILE_IMPORT"] = "Nach der Erstellung zum Import wechseln";
?>