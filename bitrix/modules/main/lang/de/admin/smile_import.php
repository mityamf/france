<?
$MESS["SMILE_IMPORT_TITLE"] = "Smileys importieren";
$MESS["ERROR_IMPORT_SMILE"] = "Fehler beim Import von Smileys";
$MESS["ERROR_BAD_SESSID"] = "Ihre Sitzung ist abgelaufen. Versuchen Sie bitte erneut.";
$MESS["ERROR_COPY_FILE"] = "Fehler beim Bildhochladen (evtl. unzureichende Zugriffsrechte f�rs Schreiben).";
$MESS["ERROR_EXISTS_FILE"] = "Fehler beim Bildhochladen. Versuchen Sie bitte erneut.";
$MESS["SMILE_SET_ID"] = "Set";
$MESS["SMILE_FILE"] = "Archiv";
$MESS["SMILE_FILE_NOTE"] = "Archiv-Format: Zip, ohne Kompression";
$MESS["SMILE_TAB_SMILE"] = "Parameter";
$MESS["SMILE_TAB_SMILE_DESCR"] = "Import-Einstellungen";
$MESS["IM_IMPORT_COMPLETE"] = "Smileys wurden importiert.";
$MESS["IM_IMPORT_TOTAL"] = "Erfolgreich importiert: #COUNT#";
$MESS["IM_IMPORT_HELP_1"] = "Eine Archiv-Datei kann Testbeschreibungen oder Nachrichtenlokalisierungen enthalten. Ein Beispiel des Archivs ist verf�gbar #LINK_START#hier#LINK_END#.";
$MESS["IM_IMPORT_HELP_2"] = "Wenn das Archiv keine Service Dateien enth�lt, wird das Import-Verfahren die Smileys entsprechend ihren Dateinamen zuweisen (Pr�fixe: \"smile_\" - ein Smiley, \"icon_\" - ein Icon; Suffix: \"_hr\" � hohe Aufl�sung).";
$MESS["IM_IMPORT_HELP_3"] = "Zum Beispiel: Wenn der Import in die Kategorie unter dem Namen \"test\" erfolgt, wird die Datei <b>smile_green_hr.png</b> als ein Smiley mit hoher Aufl�sung interpretiert und bekommt einen Code <b>:test/green:</b> zugewiesen.";
$MESS["IM_IMPORT_HELP_4"] = "<b>Achtung: Das ist ein Standardpaket. Eine neue Aktualisierung kann alle �nderung, welche Sie in diesem Paket vornehmen k�nnen, zur�cksetzen.<br>Es wird empfohlen, ein benutzerdefiniertes Paket zu #LINK_START#erstellen#LINK_END# und Emoticons dorthin hinzuzuf�gen.</b>";
?>