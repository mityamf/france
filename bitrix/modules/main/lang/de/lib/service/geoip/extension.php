<?
$MESS["MAIN_SRV_GEOIP_EXT_TITLE"] = "Erweiterung GeoIP";
$MESS["MAIN_SRV_GEOIP_EXT_DESCRIPTION"] = "The GeoIP extension allows you to find the location of an IP address. City, state, country, longitude, latitude and other information like ISP and connection type can be obtained with the help of GeoIP. <br>
You will find details on installation and configuration here: <a href='http://php.net/manual/en/book.geoip.php'>http://php.net/manual/en/book.geoip.php</a>";
$MESS["MAIN_SRV_GEOIP_EXT_DB_COUNTRY"] = "L�nder";
$MESS["MAIN_SRV_GEOIP_EXT_DB_CITY"] = "St�dte";
$MESS["MAIN_SRV_GEOIP_EXT_DB_ORG"] = "Organisationen";
$MESS["MAIN_SRV_GEOIP_EXT_DB_ISP"] = "Intranet-Anbieter";
$MESS["MAIN_SRV_GEOIP_EXT_DB_ASN"] = "ASN's";
$MESS["MAIN_SRV_GEOIP_EXT_DB_AVIALABLE"] = "Verf�gbare Datenbanken";
$MESS["MAIN_SRV_GEOIP_EXT_NOT_REQ"] = "Keine zus�tzlichen Einstellungen erforderlich";
?>