<?
$MESS["culture_err_del_lang"] = "Regionale Einstellungen k�nnen nicht gel�scht werden, weil sie von der Sprache #LID# genutzt werden.";
$MESS["culture_err_del_site"] = "Regionale Einstellungen k�nnen nicht gel�scht werden, weil sie von der Website #LID# genutzt werden.";
$MESS["culture_entity_name"] = "Name";
$MESS["culture_entity_date_format"] = "Datumformat";
$MESS["culture_entity_datetime_format"] = "Datum- und Uhrzeitformat";
$MESS["culture_entity_name_format"] = "Namenformat";
$MESS["culture_entity_charset"] = "Codierung";
?>