<?
$MESS["BXU_FileIsBlockedByOtherProcess"] = "Die Datei ist durch einen anderen Prozess gesperrt.";
$MESS["BXU_RequiredParamCIDIsNotEntered"] = "Der Parameter CID ist erforderlich.";
$MESS["BXU_RequiredParamPackageIndexIsNotEntered"] = "Der packageIndex ist erforderlich.";
$MESS["BXU_EmptyData"] = "Leeres Datum";
$MESS["BXU_SessionIsExpired"] = "Ihre Sitzung ist abgelaufen. Versuchen Sie bitte erneut.";
$MESS["BXU_AccessDenied"] = "Sie haben nicht gen�gend Rechte f�r den Upload-Ordner. Wenden Sie sich bitte an Ihren Administrator.";
$MESS["BXU_TemporaryDirectoryIsNotCreated"] = "Tempor�res Upload-Verzeichnis wurde nicht erstellt. Wenden Sie sich bitte an Ihren Administrator.";
$MESS["BXU_FileIsNotUploaded"] = "Datei wurde nicht hochgeladen.";
$MESS["BXU_FileIsFailed"] = "Datei�berpr�fung ist fehlgeschlagen.";
$MESS["BXU_FileIsNotFullyUploaded"] = "Die Datei wurde nur teilweise hochgeladen.";
$MESS["BXU_FileIsLost"] = "Datei wurde nicht gefunden.";
$MESS["BXU_TemporaryFileIsNotCreated"] = "Temporary upload file was not created.";
$MESS["BXU_FilePartCanNotBeRead"] = "Ein Dateiteil kann nicht gelesen werden.";
$MESS["BXU_FilePartCanNotBeOpened"] = "Ein Dateiteil kann nicht ge�ffnet werden.";
$MESS["BXU_FilesIsNotGlued"] = "Fehler bei Vereinigung.";
$MESS["BXU_UserHandlerError"] = "Fehler im Nutzer-Handler.";
$MESS["BXU_UPLOAD_ERR_INI_SIZE"] = "Die Gr��e der hochgeladenen Datei �berschreitet das Limit.";
$MESS["BXU_UPLOAD_ERR_FORM_SIZE"] = "Die Gr��e der hochgeladenen Datei �berschreitet das Limit von MAX_FILE_SIZE, was im HTML-Formular angegeben ist.";
$MESS["BXU_UPLOAD_ERR_PARTIAL"] = "Die Datei wurde nur teilweise hochgeladen.";
$MESS["BXU_UPLOAD_ERR_NO_FILE"] = "Die Datei wurde nicht hochgeladen.";
$MESS["BXU_UPLOAD_ERR_NO_TMP_DIR"] = "Tempor�res Verzeichnis fehlt.";
$MESS["BXU_UPLOAD_ERR_CANT_WRITE"] = "Datei kann auf den Drive nicht geschrieben werden.";
$MESS["BXU_UPLOAD_ERR_EXTENSION"] = "Eine PHP Erweiterung hat den Datei-Upload gestoppt.";
$MESS["BXU_FileIsNotRestored"] = "Die Datei wurde nicht wiederhergestellt.";
?>