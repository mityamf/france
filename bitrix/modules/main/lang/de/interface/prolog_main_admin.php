<?
$MESS["MAIN_PROLOG_ADMIN_LOGOUT"] = "Logout";
$MESS["MAIN_PROLOG_ADMIN_TITLE"] = "Administrativer Bereich";
$MESS["TRIAL_ATTENTION"] = "Achtung! Bitte benutzen Sie <a href=\"/bitrix/admin/sysupdate.php\">SiteUpdate</a>, um die letzten Updates zu installieren.<br>";
$MESS["TRIAL_ATTENTION_TEXT2"] = "Der Testzeitraum endet in";
$MESS["TRIAL_ATTENTION_TEXT3"] = "Tag(en)";
$MESS["main_prolog_help"] = "Hilfe";
$MESS["prolog_main_show_menu"] = "Men� anzeigen";
$MESS["prolog_main_m_e_n_u"] = "M<br>e<br>n<br>�<br>";
$MESS["prolog_main_less_buttons"] = "Men� minimieren";
$MESS["prolog_main_hide_menu"] = "Men� verbergen";
$MESS["MAIN_PR_ADMIN_CUR_LINK"] = "Link zur aktuellen Seite";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix"] = "Dies ist eine Testversion des Bitrix Site Managers.";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix"] = "Die Testzeit des Bitrix Site Managers ist abgelaufen. Diese Website wird in zwei Wochen nicht mehr funktionieren.";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix"] = "Die Vollversion des Bitrix Site Managers kann auf der Seite <a href=\"http://www.bitrixsoft.com/buy/\">http://www.bitrixsoft.com/buy/</a> bestellt werden.";
$MESS["TRIAL_ATTENTION_TEXT1_bitrix"] = "Dies ist eine Testversion des Bitrix Site Managers.";
$MESS["TRIAL_ATTENTION_TEXT4_bitrix"] = "Die Testzeit des Bitrix Site Managers ist abgelaufen. Diese Website wird in zwei Wochen nicht mehr funktionieren.";
$MESS["TRIAL_ATTENTION_TEXT5_bitrix"] = "Die Vollversion des Bitrix Site Managers kann auf der Seite <a href=\"http://www.bitrixsoft.com/buy/\">http://www.bitrixsoft.com/buy/</a> bestellt werden.";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix_portal"] = "Dies ist eine Testversion von Bitrix24.";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix_portal"] = "Die Testzeit von Bitrix24 ist abgelaufen. Diese Seiten werden in zwei Wochen nicht mehr funktionieren.";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix_portal"] = "Die Vollversion von Bitrix24 kann auf der Seite <a href=\"http://www.bitrixsoft.com/buy/\">http://www.bitrixsoft.com/buy/</a> bestellt werden.";
$MESS["TRIAL_ATTENTION_TEXT1_bitrix_portal"] = "Dies ist eine Testversion von Bitrix24.";
$MESS["TRIAL_ATTENTION_TEXT4_bitrix_portal"] = "Die Testzeit von Bitrix24 ist abgelaufen. Diese Seiten werden in zwei Wochen nicht mehr funktionieren.";
$MESS["TRIAL_ATTENTION_TEXT5_bitrix_portal"] = "Die Vollversion von Bitrix24 kann auf der Seite <a href=\"https://www.bitrix24.de/prices/self-hosted.php\">https://www.bitrix24.de/prices/self-hosted.php</a> bestellt werden.";
$MESS["prolog_main_more_buttons"] = "Men� maximieren";
$MESS["prolog_main_support1"] = "<span class=\"required\">Warnung!</span> Der Zeitraum f�r den technischen Support und die automatischen Updates ist am #FINISH_DATE# <b>vor #DAYS_AGO#&nbsp;Tagen</b> abgelaufen. Sie k�nnen <a href=\"http://www.bitrix.de/support/key_info.php?license_key=#LICENSE_KEY#\" target=\"_blank\">eine Servicepaket-Verl�ngerung</a> kaufen.";
$MESS["prolog_main_support_days"] = "in <b>#N_DAYS_AGO#&nbsp;Tagen</b>";
$MESS["prolog_main_support2"] = "<span class=\"required\">Warnung!</span> Der Zeitraum f�r den technischen Support und die automatischen Updates ist am #FINISH_DATE# <b>vor #DAYS_AGO#&nbsp;Tagen</b> abgelaufen. Sie k�nnen <a href=\"http://www.bitrix.de/support/key_info.php?license_key=#LICENSE_KEY#\" target=\"_blank\">eine Servicepaket-Verl�ngerung</a> kaufen.";
$MESS["prolog_main_support3"] = "<span class=\"required\">Warnung!</span> Der Zeitraum f�r den technischen Support und die automatischen Updates ist am #FINISH_DATE# <b>vor #DAYS_AGO#&nbsp;Tagen</b> abgelaufen. Sie k�nnen <a href=\"http://www.bitrix.de/support/key_info.php?license_key=#LICENSE_KEY#\" target=\"_blank\">eine Servicepaket-Verl�ngerung</a> kaufen.";
$MESS["prolog_main_today"] = "<b>Heute</b>";
$MESS["prolog_admin_headers_sent"] = "Achtung! Ung�ltige Zeichen wurden in der Systemdatei entdeckt: #FILE#, line #LINE#.";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix_eduportal"] = "Testversion von Bitrix Education Portal.";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix_eduportal"] = "Die Testzeit von Bitrix Education Portal ist abgelaufen. Dieser Website auf in zwei Wochen nicht mehr funktionieren.";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix_eduportal"] = "Die Vollversion von Bitrix Education Portal kann auf der Seite <a href=\"http://www.bitrixsoft.com/buy/\">http://www.bitrixsoft.com/buy/</a> bestellt werden.";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix_gosportal"] = "Testversion des Bitrix Government Portals.";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix_gosportal"] = "Die Testzeit von Bitrix Government Portal ist abgelaufen. Dieser Website auf in zwei Wochen nicht mehr funktionieren.";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix_gosportal"] = "Die Vollversion von Bitrix Government Portal kann auf der Seite <a href=\"http://www.bitrixsoft.com/buy/\">http://www.bitrixsoft.com/buy/</a> bestellt werden.";
$MESS["TRIAL_ATTENTION_TEXT1_1c_bitrix_gossite"] = "Testversion von Bitrix Government Site.";
$MESS["TRIAL_ATTENTION_TEXT4_1c_bitrix_gossite"] = "Die Testzeit von Bitrix Government Site ist abgelaufen. Dieser Website auf in zwei Wochen nicht mehr funktionieren.";
$MESS["TRIAL_ATTENTION_TEXT5_1c_bitrix_gossite"] = "Die Vollversion von Bitrix Government Site kann auf der Seite <a href=\"http://www.bitrixsoft.com/buy/\">http://www.bitrixsoft.com/buy/</a> bestellt werden.";
$MESS["MAIN_PR_ADMIN_FAV_ADD"] = "Zu Lesezeichen hinzuf�gen";
$MESS["MAIN_PR_ADMIN_FAV_DEL"] = "Aus Lesezeichen entfernen";
$MESS["admin_panel_browser"] = "Der administrative Bereich unterst�tzt nicht Internet Explorer unter Version 8. Installieren Sie einen modernen Browser <a href=\"http://www.firefox.com\">Firefox</a>, <a href=\"http://www.google.com/chrome/\">Chrome</a>, <a href=\"http://www.opera.com\">Opera</a> oder <a href=\"http://www.microsoft.com/windows/internet-explorer/\">Internet Explorer 9</a>.";
$MESS["MAIN_PR_ADMIN_FAV"] = "Lesezeichen";
$MESS["prolog_main_support_wit"] = "Was ist das?";
$MESS["prolog_main_support_wit_descr1"] = "Was passiert, wenn mein Abonnement abl�uft?";
$MESS["prolog_main_support_wit_descr2"] = "Nachdem Ihr Abonnement f�r Updates und den technischen Support abl�uft, werden Sie keinen Zugriff mehr auf den Marketplace haben. Das bedeutet, Sie k�nnen die Plattform-Updates nicht mehr installieren und k�nnen auch keine L�sungen von Marketplace kaufen, installieren oder aktualisieren. <br/><br/>Ihre Priorit�t beim Technischen Support wird auf den Level von nicht kommerziellen Kunden downgraded, sodass Sie mit einer Wartezeit bis zu 24 Stunden rechnen sollten. Sie werden aber das Produkt weiterhin benutzen k�nnen, und zwar solange Sie wollen. Um Ihr Abonnement zu verl�ngern und so von allen Services inkl. Updates und Marketplace profitieren zu k�nnen, m�ssen Sie die Verl�ngerung bezahlen.<br/><br/>
Wenn Sie die Verl�ngerung innerhalb von 30 Tagen nach Ablauf des vorherigen Abonnements bezahlen, kostet sie nur 22% vom Preis Ihrer Edition (Fr�hverl�ngerung).<br/><br/>
Nach dem Ablauf von diesen 30 Tagen kostet die Verl�ngerung 60% vom Preis Ihrer Edition (Sp�tverl�ngerung).
";
$MESS["prolog_main_support_wit_descr2_cp"] = "Nachdem Ihr Abonnement f�r den technischen Support und Produkt-Updates abgelaufen ist, k�nnen keine Updates f�r Ihre Produktkopie installiert werden. Sie werden nicht mehr imstande sein, neue Produktversionen zu bekommen, die L�sungen aus Market Place zu installieren oder zu aktualisieren, den Service der Web-Telefonie zu nutzen oder von der kostenlosen \"Cloud Backup\"-Funktion Gebrauch zu machen. Au�erdem werden Ihre Tickets, welche Sie an unseren Technischen Support gesendet haben, eine niedrigere Priorit�t bekommen (sodass die Wartezeit f�r eine Antwort bis zu 48 Stunden dauern kann).<br /><br />
Obwohl die Verl�ngerung des Abonnements f�r den technischen Support und die Produkt-Updates nicht erforderlich ist, empfehlen wir Ihnen, Ihr Abonnement f�r das n�chste Jahr zu verl�ngern. Produkt-Updates enthalten oft Fehlerbehebungen, L�sungen f�r kritische Probleme und neue Funktionalit�ten. Wenn Sie Ihr Abonnement verl�ngern, k�nnen Sie neue Module installieren, sowie neue Funktionalit�ten und Website-Vorlagen benutzen, welche mit jedem neuen Release verf�gbar werden. So k�nnen Sie Ihre Produktkopie immer auf die aktuellste Version upgraden.
<br /><br />
Sie k�nnen Ihr Abonnement f�r den technischen Support und die Produkt-Updates f�r das n�chste Jahr lediglich f�r 22% des aktuellen Produktpreises verl�ngern. Beachten Sie bitte, dass diese Option der Fr�hverl�ngerung nur innerhalb von 30 Tagen nach dem Ablauf Ihres aktuellen Abonnements g�ltig ist. Nachdem auch diese 30 Tage abgelaufen sind, steht Ihnen jederzeit die Standardverl�ngerung zur Verf�gung (60% des aktuellen Produktpreises).
<br /><br />
Weitere Informationen finden Sie auf der Seite <a href=\"https://store.bitrix24.de/help/licensing-policy.php\">Lizenzierung</a>.
";
$MESS["prolog_main_support_button_prolong"] = "Abonnement verl�ngern";
$MESS["prolog_main_support_button_no_prolong"] = "Nein, danke";
$MESS["prolog_main_support11"] = "<span class=\"required\">Wichtig!</span> Ihr Abonnement f�r Updates und technischen Support <b>l�uft ab</b> am #FINISH_DATE#, #DAYS_AGO#.#WHAT_IS_IT#<br /> Die Fr�hverl�ngerung ist f�r Sie verf�gbar bis #SUP_FINISH_DATE#.";
$MESS["prolog_main_support21"] = "<span class=\"required\">Wichtig!</span> Ihr Abonnement f�r Updates und technischen Support ist am #FINISH_DATE#, vor <b>#DAYS_AGO#</b> Tagen abgelaufen. #WHAT_IS_IT#<br />Die Fr�hverl�ngerung ist f�r Sie verf�gbar bis #SUP_FINISH_DATE#.";
$MESS["prolog_main_support31"] = "<span class=\"required\"><span class=\"required\">Wichtig!</span> Ihr Abonnement f�r Updates und technischen Support ist am #FINISH_DATE#.#WHAT_IS_IT# abgelaufen.<br />Sie k�nnen Ihr Abonnement verl�ngern.";
$MESS["prolog_main_support_button_no_prolong2"] = "Erinnern Sie mich sp�ter";
$MESS["prolog_main_support_menu1"] = "in:";
$MESS["prolog_main_support_menu2"] = "einer Woche";
$MESS["prolog_main_support_menu3"] = "zwei Wochen";
$MESS["prolog_main_support_menu4"] = "einem<span style=\"color:red;\">Monat</span>";
$MESS["DEVSERVER_ADMIN_MESSAGE"] = "Diese Installation existiert in Form einer Bitrix Site Manager Entwicklungs-Workbench. Sie darf nicht als eine �ffentlich verf�gbare Website benutzt werden.";
?>