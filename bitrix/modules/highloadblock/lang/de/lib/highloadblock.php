<?
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_NAME_FIELD_INVALID"] = "Name der Einheit muss mit einem Gro�buchstaben anfangen und kann nur Buchstaben und Zahlzeichen enthalten.";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_TABLE_NAME_FIELD"] = "Tabellenname in der Datenbank";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_NAME_FIELD"] = "Elementname";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_NAME_FIELD_REGEXP_INVALID"] = "Elementname muss mit dem Gro�buchstaben anfangen und darf nur lateinische Buchstaben sowie Zahlen enthalten";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_TABLE_NAME_FIELD_REGEXP_INVALID"] = "Tabellenname darf nur lateinische Kleinbuchstaben sowie Zahlen und einen Unterstrich enthalten";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_NAME_FIELD_LENGTH_INVALID"] = "Name des Elements darf nicht �ber 100 Zeichen enthalten.";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_TABLE_NAME_FIELD_LENGTH_INVALID"] = "Name der Tabelle darf nicht �ber 64 Zeichen enthalten.";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_NAME_FIELD_TABLE_POSTFIX_INVALID"] = "Der Name der Einheit kann nicht mit \"Table\" enden. API-Aufrufe k�nnen aber dieses Suffix automatisch hinzuf�gen.";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_TABLE_NAME_ALREADY_EXISTS"] = "Eine Tabelle \"#TABLE_NAME#\" existiert bereits in der Datenbank.";
$MESS["HIGHLOADBLOCK_HIGHLOAD_BLOCK_ENTITY_FIELD_NAME_REF_RESERVED"] = "Namen mit Endung _REF sind f�r Felder ReferenceField reserviert.";
?>