<?
$MESS["BCL_MON_MOB_MENU_TITLE"] = "Bitrix Cloud Service";
$MESS["BCL_MON_MOB_MENU_IPAGE"] = "Cloud-Inspektor";
$MESS["BCL_MON_MOB_TEST_LICENSE"] = "Bitrix-Lizenz l�uft ab am";
$MESS["BCL_MON_MOB_IS_HTTPS"] = "Benutzen Sie HTTPS, um die Domain zu pr�fen";
$MESS["BCL_MON_MOB_TEST_SSL_CERT_VALIDITY"] = "SSL-Zertifikat l�uft ab am";
$MESS["BCL_MON_MOB_TEST_DOMAIN_REGISTRATION"] = "Domain l�uft ab am";
$MESS["BCL_MON_MOB_TEST_HTTP_RESPONSE_TIME"] = "Antwort der Website";
$MESS["BCL_MON_MOB_INSPECTOR"] = "Website-Inspektor";
$MESS["BCL_MON_MOB_SUBSCRIBE"] = "Benachrichtigungen empfangen";
$MESS["BCL_MON_MOB_MENU_PUSH"] = "PUSH-Benachrichtigungen konfigurieren";
?>