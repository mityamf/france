<?
$MESS["SEO_CAMPAIGN_ERROR_WRONG_ENGINE"] = "Eintrag eines anderen Anbieters kann nicht aktualisiert werden";
$MESS["SEO_CAMPAIGN_ERROR_WRONG_OWNER"] = "Eintrag eines anderen Nutzers kann nicht aktualisiert werden";
$MESS["SEO_CAMPAIGN_ERROR_NO_NAME"] = "Name der Werbekampagne ist nicht angegeben";
$MESS["SEO_CAMPAIGN_ERROR_NO_FIO"] = "Name des Besitzers der Werbekampagne ist nicht angegeben";
$MESS["SEO_CAMPAIGN_ERROR_NO_START_DATE"] = "Startdatum der Werbekampagne ist nicht angegeben";
$MESS["SEO_CAMPAIGN_ERROR_NO_STRATEGY"] = "Strategie der Werbekampagne ist nicht angegeben";
$MESS["SEO_CAMPAIGN_ERROR_STRATEGY_PARAM_NOT_SUPPORTED"] = "Strategie #STRATEGY# unterst�tzt nicht den Parameter #PARAM#.";
$MESS["SEO_CAMPAIGN_ERROR_STRATEGY_PARAM_MANDATORY"] = "Parameter #PARAM# ist erforderlich f�r Strategie #STRATEGY#.";
$MESS["SEO_CAMPAIGN_ERROR_STRATEGY_NOT_SUPPORTED"] = "Strategie #STRATEGY# wird nicht unterst�tzt";
$MESS["SEO_CAMPAIGN_ERROR_WRONG_EMAIL"] = "E-Mail f�r Benachrichtigungen ist nicht korrekt";
$MESS["SEO_CAMPAIGN_ERROR_WRONG_INTERVAL"] = "H�ufigkeit der Positionspr�fung muss Folgendes enthalten: #VALUES#";
$MESS["SEO_CAMPAIGN_ERROR_WRONG_WARNING"] = "Guthaben muss von #MIN# bis #MAX# sein";
$MESS["SEO_CAMPAIGN_ERROR_CAMPAIGN_NOT_FOUND"] = "Werbekampagne ##ID# wurde in Yandex.Direct nicht gefunden.";
?>