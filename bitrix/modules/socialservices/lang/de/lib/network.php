<?
$MESS["B24NET_SEARCH_STRING_TO_SHORT"] = "Suchzeile ist zu kurz.";
$MESS["B24NET_SOCSERV_NOT_INSTALLED"] = "Das Modul \"Soziale Services\" ist nicht installiert.";
$MESS["B24NET_SOCSERV_TRANSPORT_ERROR"] = "Authentifizierung in Bitrix24.Network ist fehlgeschlagen. Loggen Sie sich bitte erneut ein.";
$MESS["B24NET_SEARCH_USER_NOT_FOUND"] = "Nutzer wurde nicht gefunden.";
$MESS["B24NET_NETWORK_IN_NOT_ENABLED"] = "Bitrix24.Network 2.0 ist auf dem Portal nicht aktiviert.";
$MESS["B24NET_ERROR_INCORRECT_PARAMS"] = "Es wurden inkorrekte Parameter �bermittelt.";
$MESS["B24NET_POPUP_TITLE"] = "Bitrix24 - Single Sign-on";
$MESS["B24NET_POPUP_CONNECT"] = "Verbinden";
$MESS["B24NET_POPUP_TEXT"] = "<b>Verbinden Sie Ihr Bitrix24</b>, benutzen Sie einen Login und ein Passwort f�r alle Ihren Websites.</b><br /><br />All die anderen Passw�rter k�nnen Sie ruhig vergessen, Bitrix24 wird Ihnen den Weg zeigen.";
$MESS["B24NET_POPUP_DONTSHOW"] = "Nicht mehr anzeigen";
?>