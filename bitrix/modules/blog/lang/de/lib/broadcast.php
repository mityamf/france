<?
$MESS["BLOG_BROADCAST_REQUEST_IM_MESSAGE_ON"] = "Ihre Nutzung des Activity Streams wurde automatisch analysiert: Jeder Nutzer bekommt Benachrichtigungen über Ihre Nachrichten, die an alle Nutzer gesendet wurden, sowie über Kommentare zu diesen Nachrichten.";
$MESS["BLOG_BROADCAST_REQUEST_IM_MESSAGE_OFF"] = "Ihre Nutzung des Activity Streams wurde automatisch analysiert: es scheint, dass es zu viele Nachricht gibt, die an alle Nutzer gerichtet sind.
Es wird empfohlen, Benachrichtigungen für Nachrichten an alle Nutzer, sowie für Kommentare zu diesen Nachrichten zu deaktivieren.
";
$MESS["BLOG_BROADCAST_REQUEST_IM_BUTTON_ON_Y"] = "Benachrichtigungen aktivieren";
$MESS["BLOG_BROADCAST_REQUEST_IM_BUTTON_ON_N"] = "Nein, danke";
$MESS["BLOG_BROADCAST_REQUEST_IM_BUTTON_OFF_Y"] = "Benachrichtigungen deaktivieren";
$MESS["BLOG_BROADCAST_REQUEST_IM_BUTTON_OFF_N"] = "Nein, danke";
$MESS["BLOG_BROADCAST_PUSH_POST"] = "#author# hat eine neue Nachricht \"#title#\" veröffentlicht";
$MESS["BLOG_BROADCAST_PUSH_POST_M"] = "#author# hat eine neue Nachricht \"#title#\" veröffentlicht";
$MESS["BLOG_BROADCAST_PUSH_POST_F"] = "#author# hat eine neue Nachricht \"#title#\" veröffentlicht";
$MESS["BLOG_BROADCAST_PUSH_POSTA"] = "#author# hat eine neue Nachricht veröffentlicht";
$MESS["BLOG_BROADCAST_PUSH_POSTA_M"] = "#author# hat eine neue Nachricht veröffentlicht";
$MESS["BLOG_BROADCAST_PUSH_POSTA_F"] = "#author# hat eine neue Nachricht veröffentlicht";
?>