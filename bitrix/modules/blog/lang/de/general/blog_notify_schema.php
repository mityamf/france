<?
$MESS["BLG_NS"] = "Nachrichten (Beiträge im Activity Stream)";
$MESS["BLG_NS_POST"] = "Nachricht an Sie gesendet";
$MESS["BLG_NS_COMMENT"] = "Kommentar zu Ihrem Beitrag gemacht";
$MESS["BLG_NS_MENTION"] = "Sie wurden erwähnt";
$MESS["BLG_NS_SHARE"] = "Zu Ihrem Beitrag wurden neue Empfänger hinzugefügt";
$MESS["BLG_NS_SHARE2USERS"] = "Sie wurden zu den Empfängern dieses Beitrags hinzugefügt";
$MESS["BLG_NS_MENTION_COMMENT"] = "Sie wurden im Kommentar erwähnt";
$MESS["BLG_NS_POST_MAIL"] = "Ihre Nachricht wurde im Activity Stream veröffentlicht.";
$MESS["BLG_NS_BROADCAST_POST"] = "Neue Nachricht an alle Nutzer senden";
$MESS["BLG_NS_BROADCAST_COMMENT"] = "Einen Kommentar zur Nachricht hinzufügen, die an alle Nutzer gesendet wurde";
$MESS["BLG_NS_MODERATE_POST"] = "Neuer nicht moderierter Beitrag";
$MESS["BLG_NS_MODERATE_COMMENT"] = "Neuer nicht moderierter Kommentar";
$MESS["BLG_NS_PUBLISHED_POST"] = "Ihr Beitrag wurde vom Moderator veröffentlicht";
$MESS["BLG_NS_PUBLISHED_COMMENT"] = "Ihr Kommentar wurde vom Moderator veröffentlicht";
$MESS["BLG_NS_IM_ANSWER_SUCCESS"] = "Kommentar wurde veröffentlicht";
$MESS["BLG_NS_IM_ANSWER_ERROR"] = "Fehler bei Veröffentlichung des Kommentars";
?>