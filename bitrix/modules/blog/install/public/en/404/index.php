<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Blogs");
?><?
	$APPLICATION->IncludeFile(
		"blog/menu.php",
		array(
			"MENU_TYPE" => "MAIN",
			"BLOG_URL" => ""
		)
	);
	?> 	<?$APPLICATION->IncludeFile("blog/main_page/search.php", Array("SEARCH_PAGE"=>"search.php"));?> 	
	<?
	if(CModule::IncludeModule('blog') && CBlog::CanUserCreateBlog($USER->GetID()) && !CBlog::GetByOwnerID($USER->GetID())):?>
		<h2><a href="blog_edit.php"><img src="/bitrix/templates/.default/blog/images/add_blog.gif" width="42" height="42" border="0" title="Create your own blog" align="absmiddle"></a>&nbsp;<a href="blog_edit.php">Create your own blog</a></h2> 
			<br />
	<?endif;?> 	 
<table width="100%" cellspacing="0" cellpadding="5" border="0"> 	 
  <tbody> 
    <tr> 		<td valign="top" width="55%" style="padding-right:20px;"><font class="maininctitle"><b>Last posts</b></font> 
        <div class="mainincline" style="width: 50%;"><img width="1" height="1" alt="" src="/bitrix/images/1.gif" /></div>
       			<?$APPLICATION->IncludeFile("blog/main_page/messages.php", Array(
	'MESSAGES_COUNT'	=>	6,	
	'SORT_BY1'	=>	'DATE_PUBLISH',	
	'SORT_ORDER1'	=>	'DESC',	
	'SORT_BY2'	=>	'ID',	
	'SORT_ORDER2'	=>	'DESC',	
	'CACHE_TIME'	=>	0,	
	));?> 		</td> 		<td valign="top" width="45%"><font class="maininctitle"><b>New blogs</b></font> 
        <div class="mainincline" style="width: 70%;"><img width="1" height="1" alt="" src="/bitrix/images/1.gif" /></div>
       			<?$APPLICATION->IncludeFile("blog/main_page/new_blogs.php", Array(
	'BLOGS_COUNT'	=>	6,	
	'SHOW_DESCRIPTION'	=>	'Y',	
	'SORT_BY1'	=>	'DATE_PUBLISH',	
	'SORT_ORDER1'	=>	'DESC',	
	'SORT_BY2'	=>	'ID',	
	'SORT_ORDER2'	=>	'DESC',	
	'CACHE_TIME'	=>	0,	
	));?> 		</td> 	</tr>
   	 
    <tr> 		<td colspan="2"> 
        <br />
       
        <br />
       <font class="maininctitle"><b>Blog groups</b></font> 
        <div class="mainincline" style="width: 75%;"><img width="1" height="1" alt="" src="/bitrix/images/1.gif" /></div>
       			<?$APPLICATION->IncludeFile("blog/main_page/groups.php", Array(
	'COLS_COUNT'	=>	3,	
	'BLOGS_COUNT'	=>	0,
	'SORT_BY1'	=>	'NAME',	
	'SORT_ORDER1'	=>	'ASC',	
	'SORT_BY2'	=>	'ID',	
	'SORT_ORDER2'	=>	'DESC',	
	'CACHE_TIME'	=>	0,	
	));?> 
        <p></p>
       		</td> 	</tr>
   	</tbody>
 </table>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>