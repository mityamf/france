<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Blogs");
?>
	<?
	$APPLICATION->IncludeFile(
		"blog/menu.php",
		array(
			"MENU_TYPE" => "MAIN",
			"BLOG_URL" => ""
		)
	);
	?>
	<?$APPLICATION->IncludeFile("blog/main_page/search.php")?>
	<?if(CModule::IncludeModule('blog') && !CBlog::GetByOwnerID($USER->GetID())):?>
		<font class="maininctitle"><a href="blog_edit.php">Create your own blog</a><br><br></font>
	<?endif;?>
	<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td valign="top"><font class="maininctitle"><b>Last messages</b></font><div class="mainincline" style="WIDTH: 50%"><img height="1" alt="" src="/bitrix/images/1.gif" width="1" /></div>
			<?$APPLICATION->IncludeFile("blog/main_page/messages.php", 
				Array(
					"MESSAGES_COUNT"=>6,
					"SORT_BY1"=>"DATE_PUBLISH",
					"SORT_ORDER1"=>"DESC",
					"SORT_BY2"=>"ID",
					"SORT_ORDER2"=>"DESC",
					"CACHE_TIME"=>0,
					)
				)?>
		</td>
		<td valign="top"><font class="maininctitle"><b>New blogs</b></font><div class="mainincline" style="WIDTH: 70%"><img height="1" alt="" src="/bitrix/images/1.gif" width="1" /></div>
			<?$APPLICATION->IncludeFile("blog/main_page/new_blogs.php", 
				Array(
					"BLOGS_COUNT"=>6,
					"SHOW_DESCRIPTION"=>"Y",
					"SORT_BY1"=>"DATE_PUBLISH",
					"SORT_ORDER1"=>"DESC",
					"SORT_BY2"=>"ID",
					"SORT_ORDER2"=>"DESC",
					"CACHE_TIME"=>0,
					)
				)?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><br><br><font class="maininctitle"><b>Groups of blogs</b></font><div class="mainincline" style="WIDTH: 60%"><img height="1" alt="" src="/bitrix/images/1.gif" width="1" /></div>
			<?$APPLICATION->IncludeFile("blog/main_page/groups.php", 
				Array(
					"COLS_COUNT"=>3,
					"BLOGS_COUNT"=>0,
					"SORT_BY1"=>"NAME",
					"SORT_ORDER1"=>"ASC",
					"SORT_BY2"=>"ID",
					"SORT_ORDER2"=>"DESC",
					"CACHE_TIME"=>0,
					)
			)?></p>
		</td>
	</tr>
	</table>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
