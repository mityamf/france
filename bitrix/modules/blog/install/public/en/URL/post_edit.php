<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Creating post");
	$APPLICATION->IncludeFile(
		"blog/menu.php",
		array(
			"MENU_TYPE" => "POST_FORM",
			"BLOG_URL" => $blog,
			"ID" => $post_id,
			"is404" => "N",
			"EditPage" => "Y"
		)
	);

	$APPLICATION->IncludeFile(
		"blog/blog/post_edit.php", 
		Array(
			"OWNER" => $blog,
			"ID" => $post_id,
			"is404" => "N"
		)
	);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
