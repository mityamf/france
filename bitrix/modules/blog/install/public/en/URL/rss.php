<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("RSS");
	$APPLICATION->IncludeFile(
		"blog/rss/rss_out.php",
		array(
			"BLOG_URL" => $blog,
			"NUM_POSTS" => "10",
			"TYPE" => $type,
			"CACHE_TIME" => "0",
			"is404" => "N"
		)
	);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>