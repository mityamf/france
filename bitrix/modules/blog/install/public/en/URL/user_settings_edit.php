<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Users");
	$APPLICATION->IncludeFile(
		"blog/menu.php",
		array(
			"MENU_TYPE" => "BLOG",
			"BLOG_URL" => $blog,
			"is404" => "N"
		)
	);

	$APPLICATION->IncludeFile(
		"blog/blog/user_settings_edit.php", 
		Array(
			"BLOG_URL" => $blog,
			"USER_ID" => IntVal($user_id),
			"is404" => "N"
		)
	);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
