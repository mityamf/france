<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Draft posts");
	$APPLICATION->IncludeFile(
		"blog/menu.php",
		array(
			"MENU_TYPE" => "BLOG",
			"BLOG_URL" => $blog,
			"is404" => "N"
		)
	);

	$APPLICATION->IncludeFile(
		"blog/blog/draft.php", 
		Array(
			"BLOG_URL" => $blog,
			"is404" => "N"
		)
	);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>