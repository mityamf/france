<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Creating blog");
if ($USER->IsAuthorized())
{
	if(CModule::IncludeModule("blog"))
	{
		if(strlen($blog)<=0)
		{
			$arBlog = CBlog::GetByOwnerID($USER->GetID());
			$blog=$arBlog["URL"];
		}

		$APPLICATION->IncludeFile(
			"blog/menu.php",
			array(
				"MENU_TYPE" => "POST_FORM",
				"BLOG_URL" => $blog,
				"ID" => "",
				"is404" => "N"
			)
		);
	}
}

$APPLICATION->IncludeFile("blog/blog/blog_edit.php", array(
	"OWNER" => $blog,
	"BLOG_ID" => "",
	"is404" => "N"
));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
