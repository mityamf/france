<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if (CModule::IncludeModule("blog"))
{
	$arVars = explode("/", $_SERVER["REQUEST_URI"]);
	$i = count($arVars) - 1;
	while ($i > 0 && strlen($arVars[$i]) <= 0)
		$i--;

	$postID = $arVars[$i];
	$blogUrl = $arVars[$i - 1];

	if (strtoupper($_SERVER["REQUEST_METHOD"]) != "POST")
	{
		LocalRedirect(CBlogPost::PreparePath($blogUrl, $postID));
		die();
	}

	$arParams = array();
	if (isset($_REQUEST))
	{
		if (isset($_REQUEST["title"]))
			$arParams["title"] = $_REQUEST["title"];
		if (isset($_REQUEST["url"]))
			$arParams["url"] = $_REQUEST["url"];
		if (isset($_REQUEST["excerpt"]))
			$arParams["excerpt"] = $_REQUEST["excerpt"];
		if (isset($_REQUEST["blog_name"]))
			$arParams["blog_name"] = $_REQUEST["blog_name"];
	}

	CBlogTrackback::GetPing($blogUrl, $postID, $arParams);
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>