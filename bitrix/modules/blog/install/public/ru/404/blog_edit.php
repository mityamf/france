<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("�������� �����");
if ($USER->IsAuthorized())
{
	CModule::IncludeModule("blog");
	$arBlog = CBlog::GetByOwnerID($USER->GetID());

	$APPLICATION->IncludeFile(
		"blog/menu.php",
		array(
			"MENU_TYPE" => "BLOG",
			"BLOG_URL" => $arBlog['URL']
		)
	);
}

$APPLICATION->IncludeFile("blog/blog/blog_edit.php", array(
	"BLOG_ID" => $_POST['BLOG_ID'],
));
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
