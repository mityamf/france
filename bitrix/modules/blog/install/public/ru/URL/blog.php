<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("����");
	$APPLICATION->IncludeFile(
		"blog/menu.php",
		array(
			"MENU_TYPE" => "BLOG",
			"BLOG_URL" => $blog,
			"is404" => "N"
		)
	);
	?>
	<table cellspacing="2" cellpadding="0" border="0" width="100%">
	<tr>
		<td width="85%" valign="top">
			<?
				$APPLICATION->IncludeFile("blog/blog/blog.php", 
					Array(
						"BLOG_URL"=>$blog,
						"MESSAGE_COUNT"=>10,
						"SORT_BY1"=>"DATE_PUBLISH",
						"SORT_ORDER1"=>"DESC",
						"SORT_BY2"=>"ID",
						"SORT_ORDER2"=>"DESC",
						"EDIT_PAGE" => "post_edit.php",
						"MONTH" => $MONTH,
						"YEAR" => $YEAR,
						"DAY" => $DAY,
						"CATEGORY" => $category,
						"CACHE_TIME_LONG"=>0,
						"CACHE_TIME_SHORT"=>0,
						"is404" => "N"
					)
				);
			?></td>
		<td valign="top" width="15%" align="center" style="padding-left:4px">
			<table class="blogtableborder" cellspacing="1" cellpadding="0" width="100%" border="0">
			<tr>
				<td>
					<table border="0" width="100%" cellpadding="3" cellspacing="0" class="blogtablebody">
					<tr>
						<td>
							<?
							$APPLICATION->IncludeFile("blog/blog/bloginfo.php", 
								Array(
									"URL"=>$blog,
									"CACHE_TIME"=>0,
									"CATEGORY" => $category,
									"is404" => "N"
								)
							);
							$APPLICATION->IncludeFile("blog/blog/calendar.php", 
								Array(
									"BLOG_URL" => $blog,
									"MONTH" => $MONTH,
									"YEAR" => $YEAR,
									"DAY" => $DAY,
									"CACHE_TIME" => 0,
									"is404" => "N"
								)
							);
							$APPLICATION->IncludeFile("blog/blog/rss.php", 
								Array(
									"BLOG_URL" => $blog,
									"VERTICAL" => "Y",
									"RSS1" => "Y",
									"RSS2" => "Y",
									"ATOM" => "Y",
									"is404" => "N"
								)
							);
							?>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>