<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Изменение категорий");
	$APPLICATION->IncludeFile(
		"blog/menu.php",
		array(
			"MENU_TYPE" => "POST_FORM",
			"BLOG_URL" => $blog,
			"is404" => "N",
			"EditPage" => "Y"
		)
	);

	$APPLICATION->IncludeFile(
		"blog/blog/category_edit.php", 
		Array(
			"OWNER" => $blog,
			"BLOG_ID" => "",
			"is404" => "N"
		)
	);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
