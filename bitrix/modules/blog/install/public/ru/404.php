<?
define("AUTH_404", "Y");
header("HTTP/1.1 200 OK");

$params = "";
if (($p = strpos($_SERVER["REQUEST_URI"], "?")) !== false)
	$params = substr($_SERVER["REQUEST_URI"], $p + 1);
parse_str($params, $_GET);
$HTTP_GET_VARS = $_GET;
extract($_GET, EXTR_SKIP);

$request_path = $_SERVER["REQUEST_URI"];
if (($p = strpos($request_path, "?")) !== false)
	$request_path = substr($request_path, 0, $p);

$realPath = str_replace("\\", "/", __FILE__);
$realPath = substr($realPath, 0, strlen($realPath) - strlen("/".basename(__FILE__)));

$documentRoot = str_replace("\\", "/", $_SERVER["DOCUMENT_ROOT"]);
while (substr($documentRoot, -1, 1) == "/")
	$documentRoot = substr($documentRoot, 0, -1);

$realPath = substr($realPath, strlen($documentRoot));

$request_path = substr($request_path, strlen($realPath));
$arFolders = explode("/", trim($request_path, "/"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$bReal404 = True;
$isBlog = false;
if (count($arFolders) > 0)
{
	if ($arFolders[0] == "users")
	{
		if ($arFolders[1] == "friends")
		{
			$bReal404 = False;

			$APPLICATION->IncludeFile(
				"blog/menu.php",
				array(
					"MENU_TYPE" => "USER",
					"BLOG_URL" => ""
				)
			);

			$APPLICATION->IncludeFile(
				"blog/blog/friends.php", 
				array(
					"ID" => IntVal($arFolders[2]),
				)
			);
		}
		elseif (IntVal($arFolders[1]) > 0)
		{
			$bReal404 = False;

			$APPLICATION->IncludeFile(
				"blog/menu.php",
				array(
					"MENU_TYPE" => "USER",
					"BLOG_URL" => ""
				)
			);

			$APPLICATION->IncludeFile(
				"blog/blog/user.php", 
				array(
					"ID" => IntVal($arFolders[1])
				)
			);
		}
	}
	elseif ($arFolders[0] == "group")
	{
		$bReal404 = False;

		$APPLICATION->IncludeFile(
			"blog/menu.php",
			array(
				"MENU_TYPE" => "GROUP",
				"BLOG_URL" => ""
			)
		);

		$APPLICATION->IncludeFile(
			"blog/blog/group.php", 
			array(
				"ID"=>IntVal($arFolders[1]),
				"BLOGS_COUNT"=>10,
				"SORT_BY1"=>"LAST_POST_DATE",
				"SORT_ORDER1"=>"DESC",
				"SORT_BY2"=>"NAME",
				"SORT_ORDER2"=>"ASC",
				"CACHE_TIME"=>0,
			)
		);
	}
	else
	{
		if (count($arFolders) > 1)
		{
			if ($arFolders[1] == "rss")
			{
				$bReal404 = False;
				$APPLICATION->IncludeFile(
					"blog/rss/rss_out.php",
					array(
						"BLOG_URL" => $arFolders[0],
						"NUM_POSTS" => "10",
						"TYPE" => $arFolders[2],
						"CACHE_TIME" => "0"
					)
				);
			}
			elseif ($arFolders[1] == "blog_edit.php")
			{
				$bReal404 = False;

				$APPLICATION->IncludeFile(
					"blog/menu.php",
					array(
						"MENU_TYPE" => "POST_FORM",
						"BLOG_URL" => $arFolders[0],
						"ID" => $ID
					)
				);

				$APPLICATION->IncludeFile(
					"blog/blog/blog_edit.php", 
					Array(
						"OWNER" => $arFolders[0],
						"BLOG_ID" => $BLOG_ID,
					)
				);
			}
			elseif ($arFolders[1] == "group_edit.php")
			{
				$bReal404 = False;

				$APPLICATION->IncludeFile(
					"blog/menu.php",
					array(
						"MENU_TYPE" => "POST_FORM",
						"BLOG_URL" => $arFolders[0],
						"ID" => $ID
					)
				);

				$APPLICATION->IncludeFile(
					"blog/blog/group_edit.php", 
					Array(
						"OWNER" => $arFolders[0],
						"BLOG_ID" => $BLOG_ID,
					)
				);
			}
			elseif ($arFolders[1] == "post_edit.php")
			{
				$bReal404 = False;

				$APPLICATION->IncludeFile(
					"blog/menu.php",
					array(
						"MENU_TYPE" => "POST_FORM",
						"BLOG_URL" => $arFolders[0],
						"ID" => $ID
					)
				);

				$APPLICATION->IncludeFile(
					"blog/blog/post_edit.php", 
					Array(
						"OWNER" => $arFolders[0],
						"ID" => $ID,
					)
				);
			}
			elseif ($arFolders[1] == "draft.php")
			{
				$bReal404 = False;

				$APPLICATION->IncludeFile(
					"blog/menu.php",
					array(
						"MENU_TYPE" => "BLOG",
						"BLOG_URL" => $arFolders[0]
					)
				);

				$APPLICATION->IncludeFile(
					"blog/blog/draft.php", 
					Array(
						"BLOG_URL" => $arFolders[0]
					)
				);
			}
			elseif ($arFolders[1] == "user_settings.php")
			{
				$bReal404 = False;

				$APPLICATION->IncludeFile(
					"blog/menu.php",
					array(
						"MENU_TYPE" => "BLOG",
						"BLOG_URL" => $arFolders[0]
					)
				);

				$APPLICATION->IncludeFile(
					"blog/blog/user_settings.php", 
					Array(
						"BLOG_URL" => $arFolders[0]
					)
				);
			}
			elseif ($arFolders[1] == "user_settings_edit.php")
			{
				$bReal404 = False;

				$APPLICATION->IncludeFile(
					"blog/menu.php",
					array(
						"MENU_TYPE" => "BLOG",
						"BLOG_URL" => $arFolders[0]
					)
				);

				$APPLICATION->IncludeFile(
					"blog/blog/user_settings_edit.php", 
					Array(
						"BLOG_URL" => $arFolders[0],
						"USER_ID" => IntVal($user_id)
					)
				);
			}
			elseif (IntVal($arFolders[1]) > 0)
			{
				$bReal404 = False;

				$APPLICATION->IncludeFile(
					"blog/menu.php",
					array(
						"MENU_TYPE" => "POST",
						"BLOG_URL" => $arFolders[0],
						"ID" => IntVal($arFolders[1])
					)
				);

				$APPLICATION->IncludeFile(
					"blog/blog/message.php", 
					Array(
						"ID"=>IntVal($arFolders[1]),
						"TBLenght" => 0,
						"CACHE_TIME"=>0,
					)
				);

				$APPLICATION->IncludeFile("blog/blog/rss.php", 
					Array(
						"BLOG_URL" => $arFolders[0],
						"VERTICAL" => "N",
						"RSS1" => "Y",
						"RSS2" => "Y",
						"ATOM" => "Y"
					)
				);

			}
			else
			{
				$bReal404 = False;
				$isBlog = true;
			}
		}
		else
		{
			$bReal404 = False;
			$isBlog = true;
		}
	}
}
if($isBlog)
{
	?>
		<?
		$APPLICATION->IncludeFile(
			"blog/menu.php",
			array(
				"MENU_TYPE" => "BLOG",
				"BLOG_URL" => $arFolders[0]
			)
		);
		?>
	<table cellspacing="2" cellpadding="0" border="0" width="100%">
	<tr>
		<td width="85%" valign="top">
			<?
				$APPLICATION->IncludeFile("blog/blog/blog.php", 
					Array(
						"BLOG_URL"=>$arFolders[0],
						"MESSAGE_COUNT"=>10,
						"SORT_BY1"=>"LAST_POST_DATE",
						"SORT_ORDER1"=>"DESC",
						"SORT_BY2"=>"ID",
						"SORT_ORDER2"=>"DESC",
						"EDIT_PAGE" => "post_edit.php",
						"MONTH" => $MONTH,
						"YEAR" => $YEAR,
						"DAY" => $DAY,
						"CATEGORY" => $category,
						"CACHE_TIME_LONG"=>0,
						"CACHE_TIME_SHORT"=>0,
					)
				);
			?></td>
		<td valign="top" width="15%" align="center">
			<?
				$APPLICATION->IncludeFile("blog/blog/bloginfo.php", 
					Array(
						"URL"=>$arFolders[0],
						"CACHE_TIME"=>0,
					)
				);
				$APPLICATION->IncludeFile("blog/blog/calendar.php", 
					Array(
						"BLOG_URL" => $arFolders[0],
						"MONTH" => $MONTH,
						"YEAR" => $YEAR,
						"DAY" => $DAY,
						"CACHE_TIME" => 0
					)
				);
				$APPLICATION->IncludeFile("blog/blog/rss.php", 
					Array(
						"BLOG_URL" => $arFolders[0],
						"VERTICAL" => "Y",
						"RSS1" => "Y",
						"RSS2" => "Y",
						"ATOM" => "Y"
					)
				);
				?>
		</td>
	</tr>
	</table>
	<?
}
if ($bReal404)
{
	$APPLICATION->SetTitle("404 - HTTP not found");
	$APPLICATION->IncludeFile("main/map/default.php");
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
