<?
$MESS["IBLOCK_ELSEARCH_TITLE"] = "Elementsuche";
$MESS["IBLOCK_ELSEARCH_LOCK_BY"] = "Blockiert von";
$MESS["IBLOCK_ELSEARCH_CHOOSE_IBLOCK"] = "W�hlen Sie einen Informationsblock";
$MESS["IBLOCK_ELSEARCH_USERINFO"] = "Userparameter anzeigen";
$MESS["IBLOCK_ELSEARCH_SECTION_EDIT"] = "Bereich bearbeiten";
$MESS["IBLOCK_ELSEARCH_ELEMENT_EDIT"] = "Element bearbeiten";
$MESS["IBLOCK_ELSEARCH_SELECT"] = "w�hlen";
$MESS["IBLOCK_ELSEARCH_NOT_SET"] = "(nicht festgelegt)";
$MESS["IBLOCK_ELSEARCH_F_DATE"] = "Datum";
$MESS["IBLOCK_ELSEARCH_F_CHANGED"] = "Ge�ndert von";
$MESS["IBLOCK_ELSEARCH_F_STATUS"] = "Status";
$MESS["IBLOCK_ELSEARCH_F_SECTION"] = "Bereich";
$MESS["IBLOCK_ELSEARCH_F_ACTIVE"] = "Aktiv";
$MESS["IBLOCK_ELSEARCH_F_TITLE"] = "�berschrift";
$MESS["IBLOCK_ELSEARCH_F_DSC"] = "Beschreibung";
$MESS["IBLOCK_ELSEARCH_IBLOCK"] = "Informationsblock:";
$MESS["IBLOCK_ELSEARCH_DESC"] = "Beschreibung:";
$MESS["IBLOCK_ELSEARCH_INCLUDING_SUBSECTIONS"] = "Unterbereiche einf�gen";
$MESS["IBLOCK_ELSEARCH_FROMTO_ID"] = "ID (erster und letzter Wert):";
$MESS["IBLOCK_ELSEARCH_ELEMENTS"] = "Elemente";
$MESS["IBLOCK_FIELD_EXTERNAL_ID"] = "Externe ID";
$MESS["IBLOCK_FIELD_CODE"] = "Symbolischer Code";
?>