<?
$MESS["IB_SE_GENERAL_SKU_ERROR"] = "Das Modul Kommerzieller Katalog fehlt oder der Informationsblock ist nicht im Modus des Kommerziellen Katalogs oder der produktvariante.";
$MESS["IB_SE_SET_PRODUCT_TYPE_GROUP_ADD"] = "Set hinzuf�gen";
$MESS["IB_SE_SET_PRODUCT_TYPE_GROUP_DELETE"] = "Set l�schen";
$MESS["IB_SE_SET_PRODUCT_TYPE_GROUP_DELETE_CONFIRM"] = "Sind Sie sicher, dass Sie dieses Set f�r diese Produktvariante";
$MESS["IB_SE_FIELD_HINT_XML_ID"] = "Erforderlich f�r bereits existierende Elemente; kann leer bleiben, wenn ein neues Element erstellt wird, in diesem Fall wird die externe ID zur Element-ID.";
$MESS["IBLOCK_SUB_FIELD_CREATED"] = "Erstellt am";
$MESS["IBLOCK_SUB_FIELD_LAST_UPDATED"] = "Ge�ndert am";
?>