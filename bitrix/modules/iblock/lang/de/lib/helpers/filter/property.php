<?
$MESS["CRM_ENTITY_SEL_BTN"] = "Ausw�hlen";
$MESS["CRM_ENTITY_SEL_SEARCH"] = "Suchen";
$MESS["CRM_SEL_SEARCH_NO_RESULT"] = "Es wurden keine Eintr�ge gefunden.";
$MESS["CRM_ENTITY_SEL_LAST"] = "Letzte";
$MESS["CRM_CONTACT_SELECTOR_TITLE"] = "W�hlen Sie Kontakte aus";
$MESS["CRM_COMPANY_SELECTOR_TITLE"] = "W�hlen Sie ein Unternehmen aus";
$MESS["CRM_DEAL_SELECTOR_TITLE"] = "W�hlen Sie Auftr�ge aus";
$MESS["CRM_LEAD_SELECTOR_TITLE"] = "W�hlen Sie Leads aus";
$MESS["FIP_CRM_FF_LEAD"] = "Leads";
$MESS["FIP_CRM_FF_CONTACT"] = "Kontakte";
$MESS["FIP_CRM_FF_COMPANY"] = "Unternehmen";
$MESS["FIP_CRM_FF_DEAL"] = "Auftr�ge";
$MESS["FIP_CRM_FF_QUOTE"] = "Angebote";
?>