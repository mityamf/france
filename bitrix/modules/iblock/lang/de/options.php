<?
$MESS["IBLOCK_COMBINED_LIST_MODE"] = "Vorschau von Bereichen und Elementen kombinieren";
$MESS["IBLOCK_MENU_MAX_SECTIONS"] = "Maximale Anzahl der Bereiche im Men�:";
$MESS["IBLOCK_PATH2RSS"] = "Pfad zu den exportierten RSS-Dateien:";
$MESS["IBLOCK_SHOW_LOADING_CODE"] = "Code aus externen Quellen anzeigen";
$MESS["IBLOCK_USE_HTMLEDIT"] = "Visuellen Editor f�r die HTML �nderungen benutzen";
$MESS["IBLOCK_EVENT_LOG"] = "Aktionen mit Informationsbl�cken protokollieren";
$MESS["IBLOCK_LIST_IMAGE_SIZE"] = "Gr��e des Vorschaubildes in Listen:";
$MESS["IBLOCK_DETAIL_IMAGE_SIZE"] = "Gr��e des Vorschaubildes im Bearbeitungsformular:";
$MESS["IBLOCK_NUM_CATALOG_LEVELS"] = "Maximale Anzahl der verschachtelten Unterbereiche f�r CSV-Export und Import:";
$MESS["IBLOCK_OPTION_SECTION_SYSTEM"] = "Systemeinstellungen";
$MESS["IBLOCK_OPTION_SECTION_LIST_AND_FORM"] = "Listen und Formulare der Elementbearbeitung";
$MESS["IBLOCK_OPTION_SECTION_CUSTOM_FORM"] = "Benutzerdefinierte Formulare der Elementbearbeitung";
$MESS["IBLOCK_OPTION_SECTION_IMPORT_EXPORT"] = "Export und Import von Daten";
$MESS["IBLOCK_CUSTOM_FORM_USE_PROPERTY_ID"] = "Eigenschafts-ID's als Schl�ssel im Bereich der Eigenschaftswerte nutzen, die ans Formular �bermittelt werden";
$MESS["IBLOCK_LIST_FULL_DATE_EDIT"] = "Auswahl der Zeit im Kalender beim Schnellbearbeiten von Listenelementen aktivieren:";
?>