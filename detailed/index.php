<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Detailed Schedule");
?> 
<h1>Accueil</h1>
<p>Workshop France</p>
 
<p><strong>Date:</strong> le 3 octobre 2018<br />
 <strong>Lieu:</strong> hotel St Regis, salle Kandinsky<br>
 <strong>Format:</strong> rendez-vous preprogrammes lors d'un workshop</p>
 
<p><strong>Programme</strong> </p>
 
<p><strong style="color: rgb(204, 0, 51);">10h30</strong> Cafe d'accueil</p>

<p><strong style="color: rgb(204, 0, 51);">11h00 &ndash; 14h00</strong> Premiere partie du workshop</p>

<p><strong style="color: rgb(204, 0, 51);">14h00 &ndash; 15h00</strong> dejeuner</p>

<p><strong style="color: rgb(204, 0, 51);">15h00 &ndash; 17h00</strong> Deuxieme partie du workshop</p>


 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>