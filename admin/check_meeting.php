<?php
//#!/usr/local/bin/php
$_SERVER["DOCUMENT_ROOT"] = "/home/travelwork/france.travelworkshop.ru/docs";
//$_SERVER["DOCUMENT_ROOT"] = "G:/openserver/domains/france";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
set_time_limit(0);

// ������� ������ ����� �������� ������������
CModule::IncludeModule('iblock');
  $times = CTimesMatrix::getTimeslots();
  $fields = CTimesMatrix::getFields();

$arParams["GUEST"] = 6;
$arParams["PARTICIP"] = 4;
$arParams["APP_COUNT"] = CTimesMatrix::getTimeslotsCount();
$arParams["APP_ID"] = 3;

//������ ������
$filter = Array(
	"GROUPS_ID"  => Array($arParams["GUEST"])
);
$rsUsers = CUser::GetList(($by="ID"), ($order="asc"), $filter, array("SELECT"=>array("UF_*"))); // �������� �������������

$arGuest = array();
while($arUsersTemp=$rsUsers->Fetch()){
	$arGuest[$arUsersTemp["ID"]]["COMP"] = $arUsersTemp["WORK_COMPANY"];
	$arGuest[$arUsersTemp["ID"]]["REP"] = $arUsersTemp["NAME"]." ".$arUsersTemp["LAST_NAME"];
	$arGuest[$arUsersTemp["ID"]]["APP"] = array();
	for($i=1; $i<$arParams["APP_COUNT"]+1; $i++){
		$arGuest[$arUsersTemp["ID"]]["APP"][] = $arUsersTemp["UF_SHEDULE_".$i];
	}
}

//������ ����������
$filter = Array(
	"GROUPS_ID"  => Array($arParams["PARTICIP"])
);
$rsUsers = CUser::GetList(($by="ID"), ($order="asc"), $filter, array("SELECT"=>array("UF_*"))); // �������� �������������

$arParticip = array();
while($arUsersTemp=$rsUsers->Fetch()){
	$arParticip[$arUsersTemp["ID"]]["COMP"] = $arUsersTemp["WORK_COMPANY"];
	$arParticip[$arUsersTemp["ID"]]["REP"] = $arUsersTemp["NAME"]." ".$arUsersTemp["LAST_NAME"];
	$arParticip[$arUsersTemp["ID"]]["APP"] = array();
	for($i=1; $i<$arParams["APP_COUNT"]+1; $i++){
		$arParticip[$arUsersTemp["ID"]]["APP"][] = $arUsersTemp["UF_SHEDULE_".$i];
	}
}

//������ ������
$arFilterM = Array(
   "IBLOCK_ID" => $arParams["APP_ID"]
   );
$arSelect = Array("DATE_CREATE", "ID", "NAME", "ACTIVE", "PROPERTY_SENDER_ID", "PROPERTY_RECIVER_ID", "PROPERTY_STATUS", "PROPERTY_TIME");
$resMeet = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilterM, false, false, $arSelect);

$arApp = array();
while($ar_meet = $resMeet->GetNext()){
	$arApp[$ar_meet["ID"]] = array();
	$arApp[$ar_meet["ID"]]["SENDER"] = $ar_meet['PROPERTY_SENDER_ID_VALUE'];
	$arApp[$ar_meet["ID"]]["RECIVER"] = $ar_meet['PROPERTY_RECIVER_ID_VALUE'];
	$arApp[$ar_meet["ID"]]["TIME"] = $ar_meet['PROPERTY_STATUS_VALUE'];
	$arApp[$ar_meet["ID"]]["STATUS"] = $ar_meet['PROPERTY_TIME_VALUE'];
}

//print_r($arApp);

$errorStr = '';
/*---- ��������� ������ ----*/
foreach ($arGuest as $key => $value){
	for($i=0; $i<$arParams["APP_COUNT"]; $i++){
		if($value["APP"][$i] != ''){
			$errorFlag = 0;
			$appID = $value["APP"][$i];
			$particip = 0;
			if(isset($arApp[$appID])){
				if($arApp[$appID]["SENDER"] == $key){
					$particip = $arApp[$appID]["RECIVER"];
				}
				elseif($arApp[$appID]["RECIVER"] == $key){
					$particip = $arApp[$appID]["SENDER"];
				}
				else{
					$errorStr .= '� ����� ID='.$key.' �� ������� '.$fields[$i].' ('.$times[$i].', ID - '.$appID.') ������. ������ ����� ��� � ���� �������.<br />';
					$errorFlag = 1;
				}
				if($particip){
					if($arParticip[$particip]["APP"][$i] != $appID){
						$errorStr .= '� ����� ID='.$key.' �� ������� '.$fields[$i].' ('.$times[$i].', ID - '.$appID.') ������. � ���������������� ��������� ����� ������� ���.<br />';
						$errorFlag = 1;
					}
				}
			}
			else{
				$errorStr .= '� ����� ID='.$key.' �� ������� '.$fields[$i].' ('.$times[$i].', ID - '.$appID.') ������. ����� ������� ���.<br />';
				$errorFlag = 1;
			}
			if($errorFlag == 0){
				$arGuest[$key]["APP"][$i] = '';
				$arParticip[$particip]["APP"][$i] = '';
			}
			else{
				$arGuest[$key]["APP"][$i] = '';
			}
		}
	}    
}
echo "������ � ������: <br />";
print_r($errorStr);
$errorStr = '';

/*---- ��������� ���������� ----*/
foreach ($arParticip as $key => $value){
	for($i=0; $i<$arParams["APP_COUNT"]; $i++){
		if($value["APP"][$i] != ''){
			$errorFlag = 0;
			$appID = $value["APP"][$i];
			$guest = 0;
			if(isset($arApp[$appID])){
				if($arApp[$appID]["SENDER"] == $key){
					$guest = $arApp[$appID]["RECIVER"];
				}
				elseif($arApp[$appID]["RECIVER"] == $key){
					$guest = $arApp[$appID]["SENDER"];
				}
				else{
					$errorStr .= '� ��������� ID='.$key.' �� ������� '.$fields[$i].' ('.$times[$i].', ID - '.$appID.') ������. ������ ��������� ��� � ���� �������.<br />';
					$errorFlag = 1;
				}
				if($guest){
					if($arGuest[$guest]["APP"][$i] != $appID){
						$errorStr .= '� ��������� ID='.$key.' �� ������� '.$fields[$i].' ('.$times[$i].', ID - '.$appID.') ������. � ���������������� ����� ����� ������� ���.<br />';
						$errorFlag = 1;
					}
				}
			}
			else{
				$errorStr .= '� ��������� ID='.$key.' �� ������� '.$fields[$i].' ('.$times[$i].', ID - '.$appID.') ������. ����� ������� ���.<br />';
				$errorFlag = 1;
			}
			if($errorFlag == 0){
				$arParticip[$key]["APP"][$i] = '';
				$arGuest[$guest]["APP"][$i] = '';
			}
			else{
				$arParticip[$key]["APP"][$i] = '';
			}
		}
	}    
}
echo "������ � ����������: <br />";
print_r($errorStr);
$errorStr = '';

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>