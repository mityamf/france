<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
  CModule::IncludeModule('iblock');
  
  $times = CTimesMatrix::getTimeslots();
  
  /*---------------------------------------------------*/
  //           ��������� ����� ��� �������             //
  /*---------------------------------------------------*/
	  $rsUser = CUser::GetByID($_REQUEST["id"]);
	  $thisUser = $rsUser->Fetch();
	  $arResult["USER"]["NAME"] = $thisUser["NAME"]." ".$thisUser["LAST_NAME"];
	  $arResult["USER"]["COMPANY"] = $thisUser["WORK_COMPANY"];
	  $arAnswer = CFormResult::GetDataByID($thisUser["UF_ANKETA"], array(), $arTmpResult, $arAnswer2);
	  $arResult["USER"]["HALL"] = $arAnswer2["user_hall"]["399"]["MESSAGE"];
	  $arResult["USER"]["TABLE"] = $arAnswer2["user_table"]["394"]["VALUE"];
	  $arParams["APP_COUNT"] = CTimesMatrix::getTimeslotsCount();
	  $arParams["GROUP_RECIVER_ID"] = 6;
	  
	  //������ �������������
	  $thisUser["UF_WISH_OUT"] = trim(str_replace("  "," ",str_replace(",", "|", substr($thisUser["UF_WISH_OUT"],2))));
	  $thisUser["UF_WISH_IN"] = trim(str_replace("  "," ",str_replace(",", "|", substr($thisUser["UF_WISH_IN"],2))));
	  
	  $myWishIn = array();
	  $countIn = 0;
	  $myWishOut = array();
	  $countOut = 0;
	  if($thisUser["UF_WISH_OUT"] != ''){
		  $filter = Array(
			  "ID"  => $thisUser["UF_WISH_OUT"]
		  );
		  $rsUsers = CUser::GetList(($by="WORK_COMPANY"), ($order="asc"), $filter, array("SELECT"=>array("UF_*"))); // �������� �������������
		  while($arUsersTemp=$rsUsers->Fetch()){
			  $myWishOut[$countOut]["COMPANY"] = $arUsersTemp["WORK_COMPANY"];
			  $myWishOut[$countOut]["ID"] = $arUsersTemp["ID"];
			  $myWishOut[$countOut]["REP"] = $arUsersTemp["NAME"]." ".$arUsersTemp["LAST_NAME"];
			  $countOut++;
		  }
	  }
	  if($thisUser["UF_WISH_IN"] != ''){
		  $filter = Array(
			  "ID"  => $thisUser["UF_WISH_IN"]
		  );
		  $rsUsers = CUser::GetList(($by="WORK_COMPANY"), ($order="asc"), $filter, array("SELECT"=>array("UF_*"))); // �������� �������������
		  while($arUsersTemp=$rsUsers->Fetch()){
			  $myWishIn[$countIn]["COMPANY"] = $arUsersTemp["WORK_COMPANY"];
			  $myWishIn[$countIn]["ID"] = $arUsersTemp["ID"];
			  $myWishIn[$countIn]["REP"] = $arUsersTemp["NAME"]." ".$arUsersTemp["LAST_NAME"];
			  $countIn++;
		  }
	  }
	  $arResult["WISH_IN"] = $myWishIn;
	  $arResult["WISH_OUT"] = $myWishOut;
  
	require('pdf/tcpdf.php');
	$pdf = new TCPDF('P', 'mm', 'A4', false, 'UTF-8', false);
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);
	$pdf->AddFont('times','I','timesi.php');
	$pdf->AddPage();
	$pdf->ImageSVG($file='images/logo.svg', $x=30, $y=5, $w='150', $h='', $link='', $align='', $palign='', $border=0, $fitonpage=false);
	$pdf->setXY(15,30);
	$pdf->SetFont('times','B',17);
	$pdf->multiCell(180, 5, "Wish-List Workshop France 2018 for", 0, C);
	$pdf->SetFont('times','',15);
	$pdf->setXY(30,50);
	$pdf->multiCell(210, 5, $arResult["USER"]["COMPANY"], 0, L);
	$pdf->setXY(30,60);
	$pdf->multiCell(210, 5,$arResult["USER"]['NAME']." ".$arResult["USER"]["LAST_NAME"], 0, L);

	$pdf->SetFont('times','B',13);
	$pdf->SetX(50);

	/* ��������� ������� */
	$pdf->setXY(0,80);
	$pdf->multiCell(210, 5, "You requested an appointment with these companies,\nbut they declined your requests or their schedules were full:", 0, C);
	
	if($countOut){
		$pdf->setXY(20,$pdf->getY() + 5);
		$pdf->SetFont('times','',13);

		$tbl = '<table cellspacing="0" cellpadding="5" border="1">
			<tr>
				<td align="center" width="60">Number</td>
				<td align="center" width="250">Companies</td>
				<td align="center" width="200">Representative</td>
			</tr>';
		$counter = 1;
		for($i = 0; $i < $countOut; $i++){
		  $tbl .= '<tr>
				<td align="center">'.$counter.'</td>
				<td>'.$arResult["WISH_OUT"][$i]["COMPANY"].'</td>
				<td>'.$arResult["WISH_OUT"][$i]["REP"].'</td>
			</tr>';
			$counter++;
		}
	  $tbl .= '</table>';
	  $pdf->writeHTML($tbl, true, false, false, false, '');
	}
	else{
		$pdf->setXY(0,$pdf->getY() + 5);
		$pdf->SetFont('times','',13);
		$pdf->multiCell(210, 5, "You don't have any companies in this section", 0, C);
	}
	
	$pdf->SetFont('times','B',13);
	$pdf->setXY(0,$pdf->getY() + 20);
	$pdf->multiCell(210, 5, "These companies requested an appointment with you,\nbut you declined their requests or your schedule was full:", 0, C);
	if($countIn){

		$pdf->setXY(20,$pdf->getY() + 5);
		$pdf->SetFont('times','',13);

		$tbl = '<table cellspacing="0" cellpadding="5" border="1">
			<tr>
				<td align="center" width="60">Number</td>
				<td align="center" width="250">Companies</td>
				<td align="center" width="200">Representative</td>
			</tr>';
		$counter = 1;
		for($i = 0; $i < $countIn; $i++){
		  $tbl .= '<tr>
				<td align="center">'.$counter.'</td>
				<td>'.$arResult["WISH_IN"][$i]["COMPANY"].'</td>
				<td>'.$arResult["WISH_IN"][$i]["REP"].'</td>
			</tr>';
			$counter++;
		}
	  $tbl .= '</table>';
	  $pdf->writeHTML($tbl, true, false, false, false, '');
	}
	else{
		$pdf->setXY(0,$pdf->getY() + 5);
		$pdf->SetFont('times','',13);
		$pdf->multiCell(210, 5, "You don't have any companies in this section", 0, C);
	}
	$pdf->setXY(20,$pdf->getY() + 5);
	$y = $pdf->getY();
	$html = 'These companies were not included in your schedule because either your or their schedule was already full.';
	$pdf->writeHTMLCell('', '', 20, $y, $html, $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true);
/*	
$pdf->setXY(20,$pdf->getY() + 15);
$y = $pdf->getY();
$html = 'You can meet these companies at any time except for the morning session.<br /><br />Please message each guest individually, and make an appointment for any time that suits you � for example, at the evening session';
	$pdf->writeHTMLCell('', '', 20, $y, $html, $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true); */
	
	
	$pdf->Output("print_wish.pdf", I);
?>