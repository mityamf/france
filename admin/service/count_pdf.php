<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
		$arParams["FORM_ID"] = "1";
		$rsUser = CUser::GetByID($_REQUEST["id"]);
		$thisUser = $rsUser->Fetch();
		CForm::GetResultAnswerArray($arParams["FORM_ID"], $arrColumns, $arrAnswers, $arrAnswersVarname, array("RESULT_ID" => $thisUser["UF_ANKETA"]));
		$realUser = array();
		$realUser["ID"] = $_REQUEST["id"];
		$realUser["NAME"] = $arrAnswers[$thisUser["UF_ANKETA"]][1][1]["USER_TEXT"];
		$realUser["SURNAME"] = $arrAnswers[$thisUser["UF_ANKETA"]][2][2]["USER_TEXT"];
		$realUser["COMPANY"] = $arrAnswers[$thisUser["UF_ANKETA"]][6][10]["USER_TEXT"];
		$realUser["CITY"] = $arrAnswers[$thisUser["UF_ANKETA"]][8][17]["USER_TEXT"];
		$realUser["COUNTRY"] = $arrAnswers[$thisUser["UF_ANKETA"]][9][18]["USER_TEXT"];
		$realUser["PHONE"] = $arrAnswers[$thisUser["UF_ANKETA"]][12][21]["USER_TEXT"];
		$realUser["ADDRESS"] = $arrAnswers[$thisUser["UF_ANKETA"]][185][393]["USER_TEXT"];
		$realUser["DATE"] = date("d.m.Y");
		$realUser["PAY_COUNT"] = $thisUser["UF_PAY_COUNT"];
		
		$rsTempRekvizit = CUserFieldEnum::GetList(array(), array("USER_FIELD_ID" => "5"));
		$countRekv = 0;
		$choosenRekv = $thisUser["UF_REKVIZIT"];
		while($rsRekvizit = $rsTempRekvizit->GetNext()){
			$realUser["REKV"][$countRekv]["ID"] = $rsRekvizit["ID"];
			$realUser["REKV"][$countRekv]["VALUE"] = $rsRekvizit["VALUE"];
			if($rsRekvizit["ID"] == $thisUser["UF_REKVIZIT"]){
				$realUser["REKV"][$countRekv]["ACTIVE"] = 'Y';
			}
			else{
				$realUser["REKV"][$countRekv]["ACTIVE"] = 'N';
			}
			$countRekv++;
		}

		require('pdf/tcpdf.php');
		if($choosenRekv == 1){
			$string= "Ordering customer / ����������: \n".$realUser['COMPANY'].",\n ".$realUser['ADDRESS'].",\n ".$realUser['CITY'].", ".$realUser['COUNTRY'].",\n ".$realUser["PHONE"]."\n\n  Beneficiary / ����������: Polanskiy Artem Valentinovich,\nregistered as independent entrepreneur\nwith State Registration Number 309503525800010\nat Federal Tax Service Inspectorate in \nPavlosvkiy Posad, Moscow Region\n\n�������������� ��������������� \n��������� ���� ������������,\n������������������ ���������� ����������� ��������� ������ \n�� �. ���������� ����� ���������� �������, \n��������������� ��������������� ����� 309503525800010\n��� 503507510512\n\nMoscow, Russia / ������, ���������� ���������\n" . date("d.m.Y");
			$pdf = new TCPDF('P', 'mm', 'A4', false, 'UTF-8', false);
			$pdf->AddFont('times','I','timesi.php');
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false); 
			$pdf->AddPage();
			$pdf->Image('images/logo_ltm.jpg');
			$pdf->SetFont('times','',9);
			$pdf->setXY(35,12);
			$pdf->multiCell(150, 5, $string, 0, R);

$pdf->setXY(0,$pdf->getY() + 5);

			$pdf->SetFont('times','B',12);
			$pdf->multiCell(200, 5, "INVOICE N/ ���� � ".$realUser["ID"]."/Kiev\n\n", 0, C);
			$pdf->SetFont('times','B',12);
			$pdf->multiCell(190, 5, "Details of payment / ������� �����:", 0, C);
			$pdf->SetFont('times','',10);
			$pdf->multiCell(185, 0, "Participation in the Luxury Travel Mart exhibition on September 24, 2013 at the\n Intercontinental Hotel Kiev, Ukraine, organized by Artem Polanskiy.\nPayment made on non-contractual basis\n\n������� � �������� Luxury Travel Mart, ������������ �� ��������� ���� ������������\n 24 �������� 2013 ���� � ����� �����������������, ����, �������.\n ������� �� ���������� � �� ������������ ��������� �������.\n\n", 0, C); 
			$pdf->SetFont('times','B',14);
			$pdf->multiCell(0, 5, "Total amount of payment / ����� �������: ".$realUser["PAY_COUNT"]." Euro\n", 0, C); 
			$pdf->SetFont('times','B',12);
			$pdf->multiCell(0, 5, "Payment information / ������ �������:", 0, C); 
			$pdf->SetFont('times','',10);
			$pdf->multiCell(0, 5, "Please put the invoice number / ������� ����� �����\n\n", 0, C); 
			$pdf->SetFont('times','B',12);
			$pdf->multiCell(0, 5, "Bank details / ���������� ���������:", 0, C);
			$pdf->SetFont('times','',12); 
			$pdf->multiCell(0, 5, "Beneficiary's Bank:\nVTB 24 (JSC), Moscow, Russia\nSWIFT: CBGURUMM\nBeneficiary: Polanskiy Artem Valentinovich\nAccount: 40802978700001002738\n\n", 0, C);
			$pdf->SetFont('times','',10);
			$pdf->multiCell(0, 10, "This invoice is valid for payments until the 20th of March 2013\nBank charges at payer's expense\n������ ������������ �� 20 ����� 2013 ����\n���������� ����� � �������� �� ���� �����������\n\n", 0, C);
			$pdf->SetFont('times','',12);
			$pdf->multiCell(300, 5, "Artem V. Polanskiy /\n��������� ���� ������������\n\n", 0, L);
			$pdf->SetFont('times','',8);
			$pdf->multiCell(0, 5, "(Electronic copy, without signature and company stamp / ����������� �����, ��� ������� � ������)", 0, C);
		}
		elseif($choosenRekv == 2){
			$string= "Ordering customer / ����������: \n".$realUser['COMPANY'].",\n ".$realUser['ADDRESS'].",\n ".$realUser['CITY'].", ".$realUser['COUNTRY'].",\n ".$realUser["PHONE"]."\n\n  Beneficiary / ����������: Travel Media,\nregistered as Society with limited liability\nwith State Registration Number 1047796617472\nat Federal Tax Service Inspectorate No 46 in\nMoscow\n\n�������� � ������������ ���������������� ������� �����,\n������������������ ���������� ����������� ��������� ������ � 46 \n�� �. ������, \n��������������� ��������������� ����� 1047796617472\n��� 7707525284\n\nMoscow, Russia / ������, ���������� ���������\n" . date("d.m.Y");
			$pdf = new TCPDF('P', 'mm', 'A4', false, 'UTF-8', false);
			$pdf->AddFont('times','I','timesi.php');
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false); 
			$pdf->AddPage();
			$pdf->Image('images/logo_ltm.jpg');
			$pdf->SetFont('times','',9);
			$pdf->setXY(35,12);
			$pdf->multiCell(150, 5, $string, 0, R);

$pdf->setXY(0,$pdf->getY() + 5);

			$pdf->SetFont('times','B',12);
			$pdf->multiCell(200, 5, "INVOICE N/ ���� � ".$realUser["ID"]."/Kiev\n\n", 0, C);
			$pdf->SetFont('times','B',12);
			$pdf->multiCell(190, 5, "Details of payment / ������� �����:", 0, C);
			$pdf->SetFont('times','',10);
			$pdf->multiCell(185, 0, "Participation in the Luxury Travel Mart exhibition on September 24, 2013 at the\n Intercontinental Hotel Kiev, Ukraine, organized by Travel Media.\nPayment made on non-contractual basis\n\n������� � �������� Luxury Travel Mart, ������������ ��� ������� �����\n 24 �������� 2013 ���� � ����� �����������������, ����, �������.\n ������� �� ���������� � �� ������������ ��������� �������.\n\n", 0, C); 
			$pdf->SetFont('times','B',14);
			$pdf->multiCell(0, 5, "Total amount of payment / ����� �������: ".$realUser["PAY_COUNT"]." Euro\n", 0, C); 
			$pdf->SetFont('times','B',12);
			$pdf->multiCell(0, 5, "Payment information / ������ �������:", 0, C); 
			$pdf->SetFont('times','',10);
			$pdf->multiCell(0, 5, "Please put the invoice number / ������� ����� �����\n\n", 0, C); 
			$pdf->SetFont('times','B',12);
			$pdf->multiCell(0, 5, "Bank details / ���������� ���������:", 0, C);
			$pdf->SetFont('times','',12); 
			$pdf->multiCell(0, 5, "Beneficiary's Bank:\nVTB Bank (open joint-stock company), Moscow, Russia\nSWIFT: VTBRRUMM\nBeneficiary: Travel Media\nAccount: 40702978900140010240\n\n", 0, C);
			$pdf->SetFont('times','',10);
			$pdf->multiCell(0, 10, "This invoice is valid for payments until the 20th of March 2013\n Bank charges at payer's expense\n ������ ������������ �� 20 ����� 2013 ����\n ���������� ����� � �������� �� ���� �����������\n\n", 0, C);
			$pdf->SetFont('times','',12);
			$pdf->multiCell(300, 5, "Elena Vetrova /\n������� ����� ����������, ����������� ��������\n\n", 0, L);
			$pdf->SetFont('times','',8);
			$pdf->multiCell(0, 5, "(Electronic copy, without signature and company stamp / ����������� �����, ��� ������� � ������)", 0, C);
		}
		else{
			$string= "Ordering customer / ����������: \n".$realUser['COMPANY'].",\n ".$realUser['ADDRESS'].",\n ".$realUser['CITY'].", ".$realUser['COUNTRY'].",\n ".$realUser["PHONE"]."\n\n  Beneficiary / ����������: Supralux Transit LLP\nEnterprise House, 82 Whitchurch Road,\nCardiff, CF14 3LX, Wales, Great Britain\n\n\n Cardiff, United Kingdom\n\n" . date("d.m.Y");					
			$pdf = new TCPDF('P', 'mm', 'A4', false, 'UTF-8', false);
			$pdf->AddFont('times','I','timesi.php');
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false); 
			$pdf->AddPage();
			$pdf->Image('images/logo_ltm.jpg');
			$pdf->SetFont('times','',9);
			$pdf->setXY(35,12);
			$pdf->multiCell(150, 5, $string, 0, R);

$pdf->setXY(0,$pdf->getY() + 5);


			$pdf->SetFont('times','B',12);
			$pdf->multiCell(200, 5, "INVOICE N/ ���� � ".$realUser["ID"]."/Kiev\n\n", 0, C);
			$pdf->SetFont('times','B',12);
			$pdf->multiCell(190, 5, "Details of payment / ������� �����:", 0, C);
			$pdf->SetFont('times','',10);
			$pdf->multiCell(185, 0, "Participation in the Luxury Travel Mart exhibition on September 24, 2013 at the\n Intercontinental Hotel Kiev, Ukraine, organized by Supralux Transit.\nPayment made on non-contractual basis\n\n������� � �������� Luxury Travel Mart, ������������ Supralux Transit\n 24 �������� 2013 ���� � ����� �����������������, ����, �������.\n ������� �� ���������� � �� ������������ ��������� �������.\n\n", 0, C); 
			$pdf->SetFont('times','B',14);
			$pdf->multiCell(0, 5, "Total amount of payment / ����� �������: ".$realUser["PAY_COUNT"]." Euro\n", 0, C); 
			$pdf->SetFont('times','B',12);
			$pdf->multiCell(0, 5, "Payment information / ������ �������:", 0, C); 
			$pdf->SetFont('times','',10);
			$pdf->multiCell(0, 5, "Please put the invoice number / ������� ����� �����\n\n", 0, C); 
			$pdf->SetFont('times','B',12);
			$pdf->multiCell(0, 5, "Bank details / ���������� ���������:", 0, C);
			$pdf->SetFont('times','',12); 
			$pdf->multiCell(0, 5, "Beneficiary: SUPRALUX TRANSIT LLP\nBeneficiary account IBAN: LV86 KBRB 1111 2144 9100 1\nBank Of  Beneficiary: Trasta Komercbanka A/O, Miesnieku iela 9, Riga LV-1050, Latvija\nSWIFT: KBRBLV2X\n\n", 0, C);
			$pdf->SetFont('times','',10);
			$pdf->multiCell(0, 10, "This invoice is valid for payments until the 20th of March 2013\n Bank charges at payer's expense\n ������ ������������ �� 20 ����� 2013 ����\n ���������� ����� � �������� �� ���� �����������\n\n", 0, C);
			$pdf->SetFont('times','',12);
			$pdf->multiCell(300, 5, "Valerii Dzuba / ������� �����,\nDirector / ��������\n\n", 0, L);
			$pdf->SetFont('times','',8);
			$pdf->multiCell(0, 5, "(Electronic copy, without signature and company stamp / ����������� �����, ��� ������� � ������)", 0, C);
		}
		$pdf->Output("count.pdf", I);
?>